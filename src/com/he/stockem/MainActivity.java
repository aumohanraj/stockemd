package com.he.stockem;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.he.stockem.adapter.NavDrawerListAdapter;
import com.he.stockem.customers.CustomersFragment;
import com.he.stockem.db.model.AreaLine;
import com.he.stockem.lib.AsyncRequest;
import com.he.stockem.lib.AsyncRequest.OnAsyncRequestComplete;
import com.he.stockem.lib.AutoCompleteItem;
import com.he.stockem.lib.AutoCompleteView;
import com.he.stockem.lib.AutocompleteArrayAdapter;
import com.he.stockem.lib.DatabaseHelper;
import com.he.stockem.lib.UtilityFunctions;
import com.he.stockem.model.NavDrawerItem;
import com.he.stockem.orders.InvoicesFragment;
import com.he.stockem.orders.OrdersFragment;
import com.he.stockem.payments.PaymentsFragment;
import com.he.stockem.purchaseorder.PurchaseOrderFragment;
import com.ngx.BluetoothPrinter;

@SuppressLint("NewApi") public class MainActivity extends Activity implements OnAsyncRequestComplete {
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;

	// nav drawer title
	private CharSequence mDrawerTitle;

	// used to store app title
	private CharSequence mTitle;

	// slide menu items
	private String[] navMenuTitles, accessControlArray;
	private TypedArray navMenuIcons;

	private ArrayList<NavDrawerItem> navDrawerItems;
	private NavDrawerListAdapter adapter;
	
	//Progress Dialog for sync
	ProgressDialog prgDialog;
	DatabaseHelper controller ;
	String updatetime="0000-00-00 00:00:00";
	UtilityFunctions util ;
	int orderCount=0;
	String SyncOrderID="0";	
	ArrayAdapter<AutoCompleteItem> customerAdapter;
	int selectedCustomerID=0;
	String selectedCustomer="";
	
	public static BluetoothPrinter mBtp= null;
	public static final String title_connecting = "connecting...";
	public static final String title_connected_to = "connected: ";
	public static final String title_not_connected = "Device not connect to printer";
	
	private static final int RESULT_SETTINGS = 1;
	SharedPreferences sharedPrefs;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
			
		//Bug Fix for crash issue		
		controller = DatabaseHelper.getInstance(this);		
		UtilityFunctions util = new UtilityFunctions(this);
				
				
		sharedPrefs = PreferenceManager
	            .getDefaultSharedPreferences(this);
		
		mTitle = mDrawerTitle = getTitle();
		String role = controller.getConfig("role_name").getConfigValue().trim();
		// load slide menu items
		navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
		
		if(role.contentEquals("administrator")){
			accessControlArray= getResources().getStringArray(R.array.admin_modules);			
		}else if(role.contentEquals("salesman")){
			accessControlArray= getResources().getStringArray(R.array.salesman_modules);			
		}else if(role.contentEquals("spotsales")){
			accessControlArray= getResources().getStringArray(R.array.spotsales_modules);			
		} 
		
		

		// nav drawer icons from resources
		navMenuIcons = getResources()
				.obtainTypedArray(R.array.nav_drawer_icons);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.list_slidermenu);
		

		navDrawerItems = new ArrayList<NavDrawerItem>();

		// adding nav drawer items to array
		// Home
		
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
		if(controller.getConfigValue("online").contentEquals("true")){
			for(int i=1; i<=7; i++){
				if (Arrays.asList(accessControlArray).contains(navMenuTitles[i].toString().trim())) {
					navDrawerItems.add(new NavDrawerItem(navMenuTitles[i], navMenuIcons.getResourceId(i, -1)));
				}
			}					
		}else{
			if(sharedPrefs.getString("prefWorkingMode", "offline").toLowerCase().contentEquals("offline")){
				navDrawerItems.add(new NavDrawerItem("Payments", navMenuIcons.getResourceId(4, -1)));			}
				navDrawerItems.add(new NavDrawerItem("Exit", navMenuIcons.getResourceId(6, -1)));
		}

		// Recycle the typed array
		navMenuIcons.recycle();

		mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

		// setting the nav drawer list adapter
		adapter = new NavDrawerListAdapter(getApplicationContext(),
				navDrawerItems);
		mDrawerList.setAdapter(adapter);
		

		// enabling action bar app icon and behaving it as toggle button
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, //nav menu toggle icon
				R.string.app_name, // nav drawer open - description for accessibility
				R.string.app_name // nav drawer close - description for accessibility
		) {
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle);
				// calling onPrepareOptionsMenu() to show action bar icons
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(mDrawerTitle);
				// calling onPrepareOptionsMenu() to hide action bar icons
				invalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		if (savedInstanceState == null) {
			// on first time display view for first nav item
			displayView(0,"Orders");
		}
		
	}

	/**
	 * Slide menu item click listener
	 * */
	private class SlideMenuClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// display view for selected nav drawer item
			NavDrawerItem item = (NavDrawerItem) adapter.getItem(position);
			displayView(position, item.getTitle());
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		MenuItem item = menu.findItem(R.id.action_sync_route);
		item.setVisible(false);
		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// toggle nav drawer on selecting action bar app icon/title
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		
		
		// Handle action bar actions click
		switch (item.getItemId()) {
		case R.id.action_settings:
//			System.out.println(controller.getLastKnownLocation().getLatitude());
			//Toast.makeText(this, "Log out to change the settings", 10).show();
			Intent i = new Intent(this, SettingActivity.class);
            startActivityForResult(i, RESULT_SETTINGS);
			return true;
			
		case R.id.action_export_db:
//			System.out.println(controller.getLastKnownLocation().getLatitude());
			exportDB();
			return true;
		case R.id.action_sync_route:
			ShowRouteSyncDialog();
			return true;
		case R.id.action_connect_printer:
			if(mBtp == null){
				initBluetooth();
			}
//			Toast.makeText(this, "Connect to printer", 10).show();
//			Intent intent = new Intent(this, ConnectPrinter.class);	        
//	        this.startActivity(intent);
			try{
				if(mBtp.getState()==mBtp.STATE_CONNECTED){
					Toast.makeText(this, "Printer already connected to "+mConnectedDeviceName, 10).show();
				}else{					
					mBtp.showDeviceList();
				}				
			}catch(Exception e){
				Log.e("MainActivity", "Bluetooth Printer connect", e);
				Toast.makeText(this, "Something went wrong. Try Again!!!", 10).show();
			}
			
			return true;
		case R.id.action_disconnect_printer:
			if(mBtp == null){
				initBluetooth();
			}
//			Toast.makeText(this, "Connect to printer", 10).show();
//			Intent intent = new Intent(this, ConnectPrinter.class);	        
//	        this.startActivity(intent);
			try{
				mBtp.unPairAll();			
				mBtp.clearStoredPrinter();
				Toast.makeText(this, "Disconnected", 10).show();
			}catch(Exception e){
				Log.e("MainActivity", "Bluetooth Printer disconnect", e);
				Toast.makeText(this, "Something went wrong. Try Again!!!", 10).show();
			}
			
			
			
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/* *
	 * Called when invalidateOptionsMenu() is triggered
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	/**
	 * Diplaying fragment view for selected nav drawer list item
	 * @param title 
	 * */
	private void displayView(int position, String title) {
		// update the main content by replacing fragments
		Fragment fragment = null;
		
		switch (title) {
		case "Orders":
			fragment = new OrdersFragment();
			break;
		case "Invoices":
			fragment = new InvoicesFragment();
			break;
		
		case "Customers":
			fragment = new CustomersFragment();
			break;
			
		case "Purchase Order":
			fragment = new PurchaseOrderFragment();
			break;
			
		case "Inventory":
			Toast.makeText(this, "Inprogress", 10).show();
			//fragment = new InvoicesFragment();
			break;			
		case "Payments":
			ShowPaymentDialog();
			//fragment = new OfflinePaymentFragment();
			
						
			break;		
		case "Sync":	
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");			
			Date date = new Date();
			date=new Date(date.getTime() - 1* 24 * 3600 * 1000 );
			
			updatetime=dateFormat.format(date);
			syncSQLiteMySQLDB();
			
			
			break;
		case "Log out":
			logoff();
			break;
		case "Exit":
			MainActivity.this.finish();
			break;
		default:
			break;
		}

		if (fragment != null) {
			FragmentManager fragmentManager = getFragmentManager();
			
			
			for(int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {    
				fragmentManager.popBackStack();
			}
			
			android.app.FragmentTransaction ft = fragmentManager.beginTransaction();
			ft.add(R.id.frame_container, fragment);
			ft.addToBackStack("SideBar");
			ft.commit();
			
			// update selected item and title, then close the drawer
			mDrawerList.setItemChecked(position, true);
			mDrawerList.setSelection(position);
			setTitle(navMenuTitles[position]);
			mDrawerLayout.closeDrawer(mDrawerList);
			
		} else {
			// error in creating fragment
			mDrawerLayout.closeDrawer(mDrawerList);
			Log.e("MainActivity", "Error in creating fragment");
		}
	}

	public void ShowPaymentDialog() {
		// TODO Auto-generated method stub
		LayoutInflater layoutInflater = LayoutInflater.from(this);
		View promptView = layoutInflater.inflate(R.layout.select_customer_popup, null);
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder.setView(promptView);
		
		ArrayList<AutoCompleteItem> customerArray = new  ArrayList<AutoCompleteItem>();
        customerArray  = (ArrayList<AutoCompleteItem>) controller.getAllCustomersItems();
        
        customerAdapter = new AutocompleteArrayAdapter(this, R.layout.list_view_row_item, customerArray);
        
		final AutoCompleteView ACSelectCustomer = (AutoCompleteView) promptView.findViewById(R.id.ACSelectCustomer);
		ACSelectCustomer.setAdapter(customerAdapter);		
		ACSelectCustomer.setOnItemClickListener(new OnItemClickListener() {
		    @Override
		    public void onItemClick(AdapterView<?> parent, View arg1, int pos, long id) {
		    	AutoCompleteItem selectedItem = (AutoCompleteItem)(parent.getItemAtPosition(pos));
		    	ACSelectCustomer.setText(selectedItem.getName());
		    	selectedCustomerID=selectedItem.getID();
		    	selectedCustomer=selectedItem.getName();						
		    }
		});
		
		alertDialogBuilder
		.setCancelable(false)
		.setPositiveButton("View Invoices", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// get user input and set it to result									
						
						if(controller.checkIsCustomerValid(selectedCustomerID, ACSelectCustomer.getText().toString())){
							Bundle args = new Bundle();
							args.putString("paymentCustomerID", String.valueOf(selectedCustomerID));
							args.putString("paymentCustomerName", String.valueOf(selectedCustomer));
							
							FragmentManager fragmentManager = getFragmentManager();
							for(int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {    
								fragmentManager.popBackStack();
							}
							
							PaymentsFragment nextFrag= new PaymentsFragment();
							setTitle("Payments");
							nextFrag.setArguments(args);
						     MainActivity.this.getFragmentManager().beginTransaction()
						     .replace(R.id.frame_container, nextFrag,"test")											     
						     .addToBackStack(null)
						     .commit();
						     
						}else{
							Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_select_customer), 10).show();
						}
					}
				})
		.setNegativeButton(getResources().getString(R.string.cancel),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,	int id) {
						dialog.cancel();
					}
				});

// create an alert dialog
AlertDialog alertD = alertDialogBuilder.create();

alertD.show();
		
	}

	public void logoff() {
		// TODO Auto-generated method stub
		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("module", "api"));
		params.add(new BasicNameValuePair("view", "logout"));				
		
		String url=getResources().getString(R.string.server);
		AsyncRequest getPosts = new AsyncRequest(this, "GET", params,"logout","Logging out");
		getPosts.execute(url);
	}

	public void syncSQLiteMySQLDB() {
		// TODO Auto-generated method stub
		controller.deleteZeroProductsOrders();
		SyncOrders();
	}
	
	public void syncRoute(String routeID) {
		// TODO Auto-generated method stub		
		
		Calendar cal = Calendar.getInstance();
		
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		System.out.println(cal.getTime());
		// Output "Wed Sep 26 14:23:28 EST 2012"

		String today = format.format(cal.getTime());
		System.out.println(today);
		// Output "2012-09-26"
		
		cal.add(Calendar.DATE, -90);
		String start_date = format.format(cal.getTime());
		System.out.println(start_date);
		// Output "2012-09-26"

		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("module", "api/payments"));
		params.add(new BasicNameValuePair("view", "arealine_statement"));		
		params.add(new BasicNameValuePair("MANUAL_invoiceDate_FROMDATE", start_date));
		params.add(new BasicNameValuePair("MANUAL_invoiceDate_TODATE", today));
		params.add(new BasicNameValuePair("payment", "1"));
		params.add(new BasicNameValuePair("AreaLine", routeID));
		String url=getResources().getString(R.string.server);
		AsyncRequest getPosts = new AsyncRequest(this, "GET", params,"syncroute",getResources().getString(R.string.synchronizing_route));
		getPosts.execute(url);
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public void asyncResponse(String response, String label) {
		// TODO Auto-generated method stub
		if(label.contentEquals("syncroute")){
			
			try {
				JSONArray arr = new JSONArray(response);
				if(arr.length()==0){
					Toast.makeText(this, "Route Sync : Data is empty/Invalid", 10).show();
				}else{
					controller.truncateTable("si_invoice_statement");
					controller.truncateTable("si_payment_mobile");
					controller.SyncRoute(arr);
					Toast.makeText(this, "Route Sync : Completed", 10).show();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				Toast.makeText(this, "Unable to parse the data", 10).show();
				e.printStackTrace();
			}			
			refreshFragment();
			return;
		}
		if(label.contentEquals("syncremotedbtolocal")){
			
			if(response.contains("billers")){
				controller.SyncDB(response);
				controller.UpdatetLastupdatedTime(updatetime);
				controller.truncateTable("si_invoice_statement");				
				syncRoute("");
			}else{
				Toast.makeText(getApplicationContext(), "Unable to Sync", 10).show();
			}			
		}
		
		if(label.contentEquals("syncorders")){			
			if(!response.contentEquals("")){
				try{
					Integer.parseInt(response);
					controller.UpdatetOrderIndexID(String.valueOf(SyncOrderID), response);
					SyncOrders();
				}catch(Exception e){
					Log.e("MainActivity", "Order Sync"+SyncOrderID, e);
					Toast.makeText(this, "Unable to sync the order " +SyncOrderID, 10).show();
				}				
				
			}						
		}
		
		if(label.contentEquals("logout")){
			controller.insertUpdateConfig("cookie", "");
			controller.logout();
			Toast.makeText(getApplicationContext(), "Logged out successfully!!!", 10).show();
			finish();
		}
		
	}
	
	public void SyncOrders(){
		SyncOrderID=controller.getOrderIDforSync();
		if(SyncOrderID.contentEquals("0")){		
			ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
//			params.add(new BasicNameValuePair("module", "api/orders"));
//			params.add(new BasicNameValuePair("view", "itemised"));
//			params.add(new BasicNameValuePair("lastupdated", controller.getLastupdatedTime()));
			params.add(new BasicNameValuePair("payments", controller.getUnSyncPaymentJSON().toString()));
			String url=getResources().getString(R.string.server)+"?module=api/orders&view=itemised&lastupdated="+URLEncoder.encode(controller.getLastupdatedTime());
			AsyncRequest getPosts = new AsyncRequest(this, "POST", params,"syncremotedbtolocal",getResources().getString(R.string.synchronizing));
			getPosts.execute(url);			
		}else{
			ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();				
			String url=getResources().getString(R.string.server)+"?module=api/orders&view=save";
			AsyncRequest getPosts = new AsyncRequest(this, "POST", controller.prepareOrderSyncParameter(params, SyncOrderID),"syncorders",getResources().getString(R.string.synchronizing));
			getPosts.execute(url);			
		}		
	}
	
	@Override
	public void onBackPressed() {		
		if(this.getFragmentManager().getBackStackEntryCount() !=1){			
				this.getFragmentManager().popBackStack();			
		}else if(!mDrawerLayout.isDrawerOpen(Gravity.LEFT)){
			mDrawerLayout.openDrawer(Gravity.LEFT);					
		}else{
			controller.logout();
			MainActivity.this.finish();
		}
		
//		super.onBackPressed();
//		if(controller.getConfigValue("online").contentEquals("false")){
//											
//		}else{
//		
//	    new AlertDialog.Builder(this)
//	        .setTitle("Really Logoff?")
//	        .setMessage("Are you sure you want to logoff?")
//	        .setNegativeButton(android.R.string.no, null)
//	        .setPositiveButton(android.R.string.yes, new OnClickListener() {
//	            public void onClick(DialogInterface arg0, int arg1) {
//	            	controller.logout();
//	                MainActivity.this.finish();
//	            }
//	        }).create().show();
//	  }
	}
	private String mConnectedDeviceName = "";
	@SuppressLint("HandlerLeak")
	private final Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case BluetoothPrinter.MESSAGE_STATE_CHANGE:
				switch (msg.arg1) {
				case BluetoothPrinter.STATE_CONNECTED:
					Toast.makeText(MainActivity.this, title_connected_to+mConnectedDeviceName, 10).show();					
					break;
				case BluetoothPrinter.STATE_CONNECTING:
					Toast.makeText(MainActivity.this, title_connecting, 10).show();					
					break;
				case BluetoothPrinter.STATE_LISTEN:
				case BluetoothPrinter.STATE_NONE:
					Toast.makeText(MainActivity.this, title_not_connected, 10).show();					
					break;
				}
				break;
			case BluetoothPrinter.MESSAGE_DEVICE_NAME:
				// save the connected device's name
				mConnectedDeviceName = msg.getData().getString(
						BluetoothPrinter.DEVICE_NAME);
				break;
			case BluetoothPrinter.MESSAGE_STATUS:				
				Log.i("MainActivity", msg.getData().getString(
						BluetoothPrinter.STATUS_TEXT));								
				break;
			default:
				break;
			}
		}
	};
	
	public void refreshFragment(){
		Fragment frg = null;
		
		frg = this.getFragmentManager().findFragmentById(R.id.frame_container);
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		ft.detach(frg);
		ft.attach(frg);
		ft.commit();
	}
	
	public void initBluetooth(){
		mBtp = BluetoothPrinter.INSTANCE;
		mBtp.setDebugService(true);

		try {
			mBtp.initService(this, mHandler);
		} catch (Exception e) {
			Log.e("MainActivity", "Bluetooth Printer initialization", e);
			e.printStackTrace();
		}
	}
	
	public void exportDB()  {
        //Open your local db as the input stream
        String inFileName = "/data/data/com.he.stockem/databases/stockem.db";
        File dbFile = new File(inFileName);
        FileInputStream fis;
		try {
			fis = new FileInputStream(dbFile);
		
		Calendar calendar = Calendar.getInstance();	
        String outFileName = Environment.getExternalStorageDirectory()+"/Stockem/databasebkp/"+calendar.getTimeInMillis()+".db";
        
        File main = new File(Environment.getExternalStorageDirectory() + "/Stockem/");
        if (!main.exists()) {
            main.mkdir();
        }
        
        File folder = new File(Environment.getExternalStorageDirectory() + "/Stockem/databasebkp/");
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdir();
        }
        
        
        //Open the empty db as the output stream
        OutputStream output = new FileOutputStream(outFileName);
        //transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = fis.read(buffer))>0){
            output.write(buffer, 0, length);
        }
        //Close the streams
        output.flush();
        output.close();
        fis.close();
        Toast.makeText(MainActivity.this, "Export completed", 10).show();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Log.e("MainActivity", "DB Export", e);
			Toast.makeText(MainActivity.this, "Unable to export. ", 10).show();
			e.printStackTrace();
		}
    }
	
	public void ShowRouteSyncDialog() {
		// TODO Auto-generated method stub
		LayoutInflater layoutInflater = LayoutInflater.from(this);
		View promptView = layoutInflater.inflate(R.layout.select_route_popup, null);
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder.setView(promptView);
		
		final Spinner spinnerRoute = (Spinner)promptView.findViewById(R.id.spinnerRoute);
		List<String> routes = new ArrayList<String>();
		List<AreaLine> area_lines= controller.getAllAreaLines();
		routes.add("All");		
		for(AreaLine area_line : area_lines){		
			routes.add(area_line.getName());
		}
		
		ArrayAdapter<String> routeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, routes);    			    
		routeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); 
	    spinnerRoute.setAdapter(routeAdapter);
	    
	    
	    
		alertDialogBuilder
		.setCancelable(false)
		.setPositiveButton("Sync Route", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// get user input and set it to result	
						if(spinnerRoute.getSelectedItem().toString().contentEquals("All")){
							syncRoute("");
						}else{
							AreaLine route = controller.getRoute(spinnerRoute.getSelectedItem().toString());						
							syncRoute(route.getId());							
						}
						
					}
				})
		.setNegativeButton(getResources().getString(R.string.cancel),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,	int id) {
						dialog.cancel();
					}
				});

		// create an alert dialog
		AlertDialog alertD = alertDialogBuilder.create();
		
		alertD.show();		
	}
	
	
	
}
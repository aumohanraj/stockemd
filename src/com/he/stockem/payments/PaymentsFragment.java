package com.he.stockem.payments;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.he.stockem.R;
import com.he.stockem.db.model.InvoiceStatement;
import com.he.stockem.db.model.Payment;
import com.he.stockem.lib.AsyncRequest;
import com.he.stockem.lib.AsyncRequest.OnAsyncRequestComplete;
import com.he.stockem.lib.ConnectionDetector;
import com.he.stockem.lib.CustomListAdapter;
import com.he.stockem.lib.CustomListItem;
import com.he.stockem.lib.DatabaseHelper;
import com.he.stockem.lib.UtilityFunctions;

public class PaymentsFragment extends Fragment implements OnAsyncRequestComplete{
	
	public PaymentsFragment(){}
	String customerName,customerID;
	UtilityFunctions util;
	ProgressDialog prgDialog;
	DatabaseHelper controller = DatabaseHelper.getInstance(this.getActivity());
	String selectedInvoice="";
	String amount="";
	EditText ETPaymentAmt;	
	TextView txtPaymentCustomerName,txt_payment_message;
	SharedPreferences sharedPrefs;
	private List<CustomListItem> customList = new ArrayList<CustomListItem>();
	private ListView listView;
	private CustomListAdapter adapter;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_payments, container, false);
        
        sharedPrefs = PreferenceManager
	            .getDefaultSharedPreferences(this.getActivity());
        
        listView = (ListView) rootView.findViewById(R.id.listPayments);
		adapter = new CustomListAdapter(getActivity(), customList);
		listView.setAdapter(adapter);	
		
		listView.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				CustomListItem selectedItem = (CustomListItem)(parent.getItemAtPosition(position));
				selectedInvoice = selectedItem.getID();
				LayoutInflater layoutInflater = LayoutInflater.from(view.getContext());
				View promptView = layoutInflater.inflate(R.layout.payment_process_popup, null);
				ETPaymentAmt = (EditText)promptView.findViewById(R.id.ETPaymentAmt);
				//ETPaymentAmt.setText(util.StringroundTwoDecimals(selectedItem.getFooterLeft().replace("Due : ", "").replace(", Paid : ", "")));
				ETPaymentAmt.setText(util.StringroundTwoDecimals(selectedItem.getFooterLeft().split(", Paid : ")[0].replace("Due : ", "")));
				
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						view.getContext());
				alertDialogBuilder.setView(promptView);
				alertDialogBuilder.setTitle(getActivity().getResources().getString(R.string.payment));
				alertDialogBuilder
				.setMessage(selectedItem.getTitle()+", "+selectedItem.getDescription()+", "+selectedItem.getFooterLeft())
				.setCancelable(false)
				.setPositiveButton(view.getContext().getResources().getString(R.string.save),new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						String amount = ETPaymentAmt.getText().toString().trim();
						if(amount.contentEquals("")||amount.contentEquals("0")){
							Toast.makeText(getActivity(), "Amount cannot be zero or empty", 10).show();
							return;
						}
						
						Double temp = Double.parseDouble(amount);
						if(temp==0){
							Toast.makeText(getActivity(), "Amount cannot be zero", 10).show();
							return;					
						}
						if(sharedPrefs.getString("prefWorkingMode", "offline").toLowerCase().contentEquals("offline")){
							Payment payment = new Payment(getActivity());
							payment.setAc_amount(amount);
							payment.setAc_inv_id(selectedInvoice);
							DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
							Date date = new Date();				
							payment.setAc_date(dateFormat.format(date));
							payment.setAc_notes("Offline Mobile Payment");
							payment.save();
							Toast.makeText(getActivity(), "Payment Saved", 10).show();
							loadOfflineCustomerPendingInvoices(customerID);
							return;
						}
						
						ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
						params.add(new BasicNameValuePair("module", "api/payments"));
						params.add(new BasicNameValuePair("view", "save"));
						params.add(new BasicNameValuePair("invoice_id", selectedInvoice));
						params.add(new BasicNameValuePair("ac_amount", ETPaymentAmt.getText().toString()));
						DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
						Date date = new Date();				
						params.add(new BasicNameValuePair("ac_date", dateFormat.format(date)));
						
						String url=getResources().getString(R.string.server);
						AsyncRequest getPosts = new AsyncRequest(PaymentsFragment.this, "GET", params,"savepayment","Sending Details");
						getPosts.execute(url);
						dialog.cancel();					
						
					}
				  })
				.setNegativeButton(getActivity().getResources().getString(R.string.cancel),new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {	
						dialog.cancel();								
					}
				});
				
				AlertDialog alertDialog = alertDialogBuilder.create();
				alertDialog.show();
												
			}			
		});
        
        customerID = getArguments().getString("paymentCustomerID");
        customerName = getArguments().getString("paymentCustomerName");
        
        
		txtPaymentCustomerName =(TextView)rootView.findViewById(R.id.txtPaymentCustomerName);
		txtPaymentCustomerName.setText(customerName);
		
		txt_payment_message = (TextView)rootView.findViewById(R.id.txt_payment_message);
		util= new UtilityFunctions(getActivity());
		prgDialog = util.createProgressDialog(getResources().getString(R.string.please_wait)+getResources().getString(R.string.getting_orders));
		ConnectionDetector cd = new ConnectionDetector(getActivity());
		
		if(sharedPrefs.getString("prefWorkingMode", "offline").toLowerCase().contentEquals("offline")){
			loadOfflineCustomerPendingInvoices(customerID);
		}else{
			if(cd.isConnectingToInternet()){
				getPendingInvoices(customerID);
			}else{
				Toast.makeText(getActivity(), getResources().getString(R.string.no_internet_connection), 10).show();
			}				
		}
		
		
        
        return rootView;
    }
	
	public void getPendingInvoices(String customer_id) {
		// TODO Auto-generated method stub
		
		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("module", "api/payments"));
		params.add(new BasicNameValuePair("view", "process"));
		params.add(new BasicNameValuePair("customer_id", customer_id));
		
		String url=getResources().getString(R.string.server);
		AsyncRequest getPosts = new AsyncRequest(this, "GET", params,"getpendinginvoices",getResources().getString(R.string.getting_invoices));
		getPosts.execute(url);		
	}

	@Override
	public void asyncResponse(String response, String label) {
		// TODO Auto-generated method stub		
		if(label.contentEquals("getpendinginvoices")){
			customList.clear();
			List<String> list = new ArrayList<String>();
			JSONArray arr=null;
			try {
				 arr = new JSONArray(response);
				 int rows =arr.length();
				 if(rows ==0 ){
					 Toast.makeText(getActivity(), "No Pending invoices for this customer", 10).show();
					 txt_payment_message.setText("No pending invoices for this customer");
					 return;
				 }
				 
				 Double total=0.0;
				 for(int i=0; i<rows;i++){
						JSONObject obj = (JSONObject)arr.get(i);
						list.add("Inv. "+ obj.getString("index_id") +", Rs."+obj.getString("owing")+", "+obj.getString("date")+",#"+obj.getString("id"));
						CustomListItem listItem = new CustomListItem();
						listItem.setTitle("Invoice No : " +obj.getString("index_id"));
						listItem.setDescription("Invoice Amount : " +util.StringroundTwoDecimals(obj.getString("invoice_total")));
						listItem.setFooterLeft("Due : " +util.StringroundTwoDecimals(obj.getString("owing"))+ ", Paid : " +util.StringroundTwoDecimals(obj.getString("approval_pending_payments")));
						listItem.setFooterRight("Date : " +obj.getString("date"));
						listItem.setID(obj.getString("id"));
						try{
							total = total  + Double.parseDouble(obj.getString("owing"));
						}catch(Exception e){
							Log.e("PaymentsFragment","asyncResponse",e);
						}
						customList.add(listItem);
				}
				txt_payment_message.setText("Click on the invoices to process payment. Total : "+String.valueOf(util.roundTwoDecimals(total)));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				Log.e("PaymentsFragment","asyncResponse",e);
			}	
				
			adapter.notifyDataSetChanged();
		}
		if(label.contentEquals("savepayment")){
			Toast.makeText(getActivity(), response, 10).show();	
			getPendingInvoices(customerID);
		}
		
	}
	
	public void loadOfflineCustomerPendingInvoices(String customerID){
		List<InvoiceStatement> listInvoice = controller.getCustomerPendingPayments(customerID);
		
		Double total=0.0;
		customList.clear();
		 for(int i=0; i<listInvoice.size() ;i++){
			 	InvoiceStatement obj = listInvoice.get(i);
				
				CustomListItem listItem = new CustomListItem();
				listItem.setTitle("Invoice No : " +obj.getIndex_id());
				listItem.setDescription("Invoice Amount : " +obj.getTotal());
				listItem.setFooterLeft("Due : " +obj.getOwing() +", Paid : " +obj.getPaidAmount());
				listItem.setFooterRight("");
				listItem.setID(obj.getId());
				
				customList.add(listItem);
				
				try{
					total = total  + Double.parseDouble(obj.getOwing());
				}catch(Exception e){
					Log.e("PaymentsFragment","asyncResponse",e);
				}
				
		}
		if(listInvoice.size() ==0){
			 txt_payment_message.setText("Click on the invoices to process payment.");
		     Toast.makeText(this.getActivity(), "No Pending invoices for this customer", 10).show();
		}else{			
			txt_payment_message.setText("Click on the invoices to process payment. Total : "+String.valueOf(util.roundTwoDecimals(total)));
		}
		 
		adapter.notifyDataSetChanged();
		
	}
}

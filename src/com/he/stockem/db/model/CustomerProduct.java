package com.he.stockem.db.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.he.stockem.lib.DatabaseHelper;



public class CustomerProduct {
	
	
	Context context;
	String customer_id="";
	String product_id="";
	String unit_price="";
	String logtime="";
	String quantity="";
	String free="";
	String id="";
	
	public CustomerProduct(Context context) {
		this.context=context;			
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getProduct_id() {
		return product_id;
	}

	public void setProduct_id(String product_id) {
		this.product_id = product_id;
	}

	public String getUnit_price() {
		return unit_price;
	}

	public void setUnit_price(String unit_price) {
		this.unit_price = unit_price;
	}

	public String getLogtime() {
		return logtime;
	}

	public void setLogtime(String logtime) {
		this.logtime = logtime;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getFree() {
		return free;
	}

	public void setFree(String free) {
		this.free = free;
	}	
		
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void save(int i){
		DatabaseHelper controller = DatabaseHelper.getInstance(context);
		SQLiteDatabase db = controller.getDB();
		ContentValues values = new ContentValues();
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		
		
		values.put("unit_price", this.unit_price);
		values.put("free", this.free);
		values.put("quantity", this.quantity);
		values.put("logtime", this.logtime);
		values.put("id", this.id);
		
		if(i == 1){
			db.update("si_customer_products", values, "customer_id=? and product_id=?", new String[] {this.customer_id,this.product_id});						
		}else{
			values.put("customer_id", this.customer_id);
			values.put("product_id", this.product_id);
			db.insert("si_customer_products", null, values);												
		}
		
	}
}

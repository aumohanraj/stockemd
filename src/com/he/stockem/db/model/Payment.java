package com.he.stockem.db.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.he.stockem.lib.DatabaseHelper;


public class Payment {
	   
	String id="";
	String ac_inv_id="";
	String ac_amount="";
	String ac_notes="";
	String ac_date="";
	String ac_payment_type="";
	String domain_id="";
	String online_payment_id="";
	String cheque_no="";
	String cheque_bank="";
	String cheque_status="";
	String status="0";
	String user_id="";
	
	Context context;	
	
	public Payment(Context context) {
		this.context=context;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAc_inv_id() {
		return ac_inv_id;
	}

	public void setAc_inv_id(String ac_inv_id) {
		this.ac_inv_id = ac_inv_id;
	}

	public String getAc_amount() {
		return ac_amount;
	}

	public void setAc_amount(String ac_amount) {
		this.ac_amount = ac_amount;
	}

	public String getAc_notes() {
		return ac_notes;
	}

	public void setAc_notes(String ac_notes) {
		this.ac_notes = ac_notes;
	}

	public String getAc_date() {
		return ac_date;
	}

	public void setAc_date(String ac_date) {
		this.ac_date = ac_date;
	}

	public String getAc_payment_type() {
		return ac_payment_type;
	}

	public void setAc_payment_type(String ac_payment_type) {
		this.ac_payment_type = ac_payment_type;
	}

	public String getDomain_id() {
		return domain_id;
	}

	public void setDomain_id(String domain_id) {
		this.domain_id = domain_id;
	}

	public String getOnline_payment_id() {
		return online_payment_id;
	}

	public void setOnline_payment_id(String online_payment_id) {
		this.online_payment_id = online_payment_id;
	}

	public String getCheque_no() {
		return cheque_no;
	}

	public void setCheque_no(String cheque_no) {
		this.cheque_no = cheque_no;
	}

	public String getCheque_bank() {
		return cheque_bank;
	}

	public void setCheque_bank(String cheque_bank) {
		this.cheque_bank = cheque_bank;
	}

	public String getCheque_status() {
		return cheque_status;
	}

	public void setCheque_status(String cheque_status) {
		this.cheque_status = cheque_status;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	
	public String save(){
		DatabaseHelper controller = DatabaseHelper.getInstance(context);
		SQLiteDatabase db = controller.getDB();
		ContentValues values = new ContentValues();	
		
		
		values.put(DatabaseHelper.KEY_PAYMENTMOBILE_AC_AMOUNT, this.ac_amount);
		values.put(DatabaseHelper.KEY_PAYMENTMOBILE_AC_DATE, this.ac_date);
		values.put(DatabaseHelper.KEY_PAYMENTMOBILE_AC_INV_ID, this.ac_inv_id);
		values.put(DatabaseHelper.KEY_PAYMENTMOBILE_AC_NOTES, this.ac_notes);
		values.put(DatabaseHelper.KEY_PAYMENTMOBILE_AC_PAYMENT_TYPE, this.ac_payment_type);
		values.put(DatabaseHelper.KEY_PAYMENTMOBILE_CHEQUE_BANK, this.cheque_bank);
		values.put(DatabaseHelper.KEY_PAYMENTMOBILE_CHEQUE_NO, this.cheque_no);
		values.put(DatabaseHelper.KEY_PAYMENTMOBILE_CHEQUE_STATUS, this.cheque_status);
		values.put(DatabaseHelper.KEY_PAYMENTMOBILE_DOMAIN_ID, this.domain_id);
		values.put(DatabaseHelper.KEY_PAYMENTMOBILE_ONLINE_PAYMENT_ID, this.online_payment_id);
		values.put(DatabaseHelper.KEY_PAYMENTMOBILE_STATUS, this.status);
		values.put(DatabaseHelper.KEY_PAYMENTMOBILE_USER_ID, this.user_id);
		
		if(this.id.contentEquals("")){
			this.id=String.valueOf((int)db.insert(DatabaseHelper.TABLE_PAYMENT_MOBILE, null, values));			
			return this.id;			
		}else{
			db.update(DatabaseHelper.TABLE_PAYMENT_MOBILE, values, "id=?", new String[] {this.id});
			return this.id;
		}
	}
	
	public String updateStatus(){
		DatabaseHelper controller = DatabaseHelper.getInstance(context);
		SQLiteDatabase db = controller.getDB();
		ContentValues values = new ContentValues();	
		
		values.put(DatabaseHelper.KEY_PAYMENTMOBILE_STATUS, this.status);		
			
		db.update(DatabaseHelper.TABLE_PAYMENT_MOBILE, values, "id=?", new String[] {this.id});
		return this.id;
   }	
	
}


package com.he.stockem.db.model;

import java.util.ArrayList;
import java.util.List;

import com.he.stockem.lib.DatabaseHelper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;



public class AreaLine {
	
	String id="";
	String name="";
	String domain_id="";
	Context context;
	List<Customer> listCustomers = new ArrayList<Customer>();
	
	public AreaLine(Context context) {
		this.context=context;			
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDomain_id() {
		return domain_id;
	}
	public void setDomain_id(String domain_id) {
		this.domain_id = domain_id;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public List<Customer> getCustomers(){
		DatabaseHelper controller = DatabaseHelper.getInstance(this.context);
		SQLiteDatabase db = controller.getDB();
		Cursor cursor = db.query(DatabaseHelper.TABLE_AREA_LINE, new String[]{"id"}, "name=?", new String[] {this.name}, null, null, null);
		if(cursor.moveToFirst()){
			listCustomers.clear();
			listCustomers=controller.getAllCustomersByAreaLine(cursor.getInt(cursor.getColumnIndex("id")));			
		}
		DatabaseHelper.closeCursor(cursor);
		return this.listCustomers;
	}
}

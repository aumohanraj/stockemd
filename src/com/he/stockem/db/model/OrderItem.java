package com.he.stockem.db.model;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.he.stockem.lib.DatabaseHelper;
import com.he.stockem.lib.UtilityFunctions;


public class OrderItem {
	
	String id="";
	String order_id="";
	int quantity=0;		
	int product_id;
	Double unit_price=0.00;
	Double tax_amount=0.00;
	Double gross_amount=0.00;	
	String description="";	
	int free_id=0;
	int free_qty=0;
	String billed="off";
	List<OrderItemTax> ItemTax = new ArrayList<OrderItemTax>();
	Context context;
	UtilityFunctions util;
	public OrderItem(Context context) {
		this.context=context;
		util = new UtilityFunctions(context);
	}
	
	//Set Methods for all the attributes of the table
	public void setID(String id) {
		this.id = id;
	}		
	
	public void setOrderID(String OrderID){
		this.order_id=OrderID;		
	}
	public void setQuantity(int Quantity){
		this.quantity=Quantity;
		
	}
	public void setProductID(int ProductID){
		this.product_id=ProductID;
	}
	public void setUnitPrice(Double UnitPrice){
		this.unit_price=UnitPrice;
	}
	public void setTaxAmount(Double TaxAmount){
		this.tax_amount=TaxAmount;
	}	
	
	public void setGrossAmount(Double gross_amount){
		this.gross_amount=gross_amount;
	}	
	
	public Double getGrossAmount(){
		return this.gross_amount;
	}	
	
	public void setDescription(String Description){
		this.description=Description;
	}	
	public void setFreeID(int prodFreeInsertID){
		this.free_id=prodFreeInsertID;
	}
	public void setFreeQty(int FreeQty){
		this.free_qty=FreeQty;		
	}
	public void setBilled(String Billed){
		this.billed=Billed;
	}
	
	//Get Methods for all the attributes of the table
	public String getID() {
		return id;
	}
	public String getOrderID() {
		return order_id;
	}
	public int getQuantity(){
		return this.quantity;		
	}
	public int getProductID(){
		return this.product_id;
	}
	public Double getUnitPrice(){
		return this.unit_price;
	}
	public Double getTaxAmount(){
		this.tax_amount = 0.00;
		for (OrderItemTax tax : getItemTaxes()) {
		    tax_amount=tax_amount = tax.getTaxAmount();
		}
		return this.tax_amount;
	}
	public Double getGrossTotal(){
		return this.unit_price*this.quantity;
	}
	public String getDescription(){
		return this.description;
	}
	public Double getTotal(){		
		return this.gross_amount+this.tax_amount;		
	}
	public int getFreeID(){
		return this.free_id;
	}
	public int getFreeQty(){
		return this.free_qty;		
	}
	public String getBilled(){
		return this.billed;
	}
	
	public List<OrderItemTax> getItemTaxes(){
		DatabaseHelper controller = DatabaseHelper.getInstance(this.context);
		SQLiteDatabase db = controller.getDB();
		Cursor cursor = db.query(DatabaseHelper.TABLE_ORDER_ITEM_TAX, new String[]{"id"}, DatabaseHelper.KEY_ORDERITEM_TAX_ORDER_ITEM_ID+"=?", new String[] {this.id}, null, null, null);
		if(cursor.moveToFirst()){
			ItemTax.clear();
			do{
				ItemTax.add(controller.getOrderItemTax(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_ORDERITEM_TAX_ID))));
			}while(cursor.moveToNext());
			
		}
		DatabaseHelper.closeCursor(cursor);
		return this.ItemTax;
	}
	
	public String save(){
		DatabaseHelper controller = DatabaseHelper.getInstance(context);
		SQLiteDatabase db = controller.getDB();
		ContentValues values = new ContentValues();	
		values.put(DatabaseHelper.KEY_ORDERITEM_BILLED, getBilled());
		values.put(DatabaseHelper.KEY_ORDERITEM_DESCRIPTION, getDescription());
		values.put(DatabaseHelper.KEY_ORDERITEM_FREE_ID, getFreeID());
		values.put(DatabaseHelper.KEY_ORDERITEM_FREE_QTY, getFreeQty());
		values.put(DatabaseHelper.KEY_ORDERITEM_GROSS_TOTAL, getGrossTotal());		
		values.put(DatabaseHelper.KEY_ORDERITEM_ORDER_ID, getOrderID());
		values.put(DatabaseHelper.KEY_ORDERITEM_PRODUCT_ID, getProductID());
		values.put(DatabaseHelper.KEY_ORDERITEM_QUANTITY, getQuantity());
		values.put(DatabaseHelper.KEY_ORDERITEM_TAX_AMOUNT, getTaxAmount());
		values.put(DatabaseHelper.KEY_ORDERITEM_TOTAL, getTotal());
		values.put(DatabaseHelper.KEY_ORDERITEM_UNIT_PRICE, getUnitPrice());
		
		if(this.id.contentEquals("")){
			this.id=String.valueOf((int)db.insert(DatabaseHelper.TABLE_ORDER_ITEM, null, values));					
		}else{
			db.update(DatabaseHelper.TABLE_ORDER_ITEM, values, "id=?", new String[] {this.id});			
		}
		return this.id;
	}
	
	public int addTax(OrderItemTax taxItem){
		taxItem.setOrderItemID(Integer.parseInt(this.id));		
		taxItem.save();	
		ItemTax.add(taxItem);		
		return Integer.parseInt(taxItem.getID());
	}
}

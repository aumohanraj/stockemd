package com.he.stockem.db.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.he.stockem.lib.DatabaseHelper;


public class OrderItemTax {
	
	String id="";
	int order_item_id=0;
	int tax_id=0;
	String tax_type="";
	Double tax_rate=0.00;
	Double tax_amount=0.00;
	Context context;

	public OrderItemTax(Context context) {
		this.context=context;
	}
	
	//Set Methods for all the attributes of the table
	public void setID(String id) {
		this.id = id;
	}
	public void setOrderItemID(int prodFreeInsertID) {
		this.order_item_id = prodFreeInsertID;
	}
	public void setTaxID(int TaxID) {
		this.tax_id = TaxID;
	}
	public void setTaxType(String TaxType) {
		this.tax_type = TaxType;
	}
	public void setTaxRate(Double TaxRate) {
		this.tax_rate = TaxRate;
	}
	public void setTaxAmount(Double TaxAmount) {
		this.tax_amount = TaxAmount;
	}
	
	//Get Methods for all the attributes of the table
	public String getID() {
		return id;
	}	
	public int getOrderItemID() {
		return this.order_item_id ;
	}
	public int getTaxID() {
		return this.tax_id ;
	}
	public String getTaxType() {
		return this.tax_type ;
	}
	public Double getTaxRate() {
		return this.tax_rate ;
	}
	public Double getTaxAmount() {
		return this.tax_amount;
	}
	
	public String save(){
		DatabaseHelper controller = DatabaseHelper.getInstance(context);
		SQLiteDatabase db = controller.getDB();
		
		OrderItem item = controller.getOrderItem(String.valueOf(this.order_item_id));
		this.setTaxAmount(item.getGrossTotal()*this.tax_rate/100);
		
		ContentValues values = new ContentValues();	
		values.put(DatabaseHelper.KEY_ORDERITEM_TAX_ORDER_ITEM_ID ,order_item_id);
		values.put(DatabaseHelper.KEY_ORDERITEM_TAX_TAX_ID ,tax_id);
		values.put(DatabaseHelper.KEY_ORDERITEM_TAX_TAX_TYPE ,tax_type);
		values.put(DatabaseHelper.KEY_ORDERITEM_TAX_TAX_RATE ,tax_rate);
		values.put(DatabaseHelper.KEY_ORDERITEM_TAX_TAX_AMOUNT ,tax_amount);
		if(this.id.contentEquals("")){
			this.id=String.valueOf((int)db.insert(DatabaseHelper.TABLE_ORDER_ITEM_TAX, null, values));
		}else{
			db.update(DatabaseHelper.TABLE_ORDER_ITEM_TAX, values, "id=?", new String[] {this.id});
		}
		item.save();
		return this.id;
	}
	
}

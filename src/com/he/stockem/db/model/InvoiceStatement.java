package com.he.stockem.db.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.he.stockem.lib.DatabaseHelper;



public class InvoiceStatement {
	
	String id="";
	String index_id="";
	String customer_id="";
	String owing="";
	String paid="";
	String total="";
	Context context;
	
	
	public InvoiceStatement(Context context) {
		this.context=context;			
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIndex_id() {
		return index_id;
	}

	public void setIndex_id(String index_id) {
		this.index_id = index_id;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public String getOwing() {
		return owing;
	}

	public void setOwing(String owing) {
		this.owing = owing;
	}

	public String getPaid() {
		return paid;
	}

	public void setPaid(String paid) {
		this.paid = paid;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}
	
	public void save(Context context){
		SQLiteDatabase db = DatabaseHelper.getInstance(context).getDB();
		ContentValues values = new ContentValues();			
		values.put("id", this.id);
		values.put("customer_id", this.customer_id);
		values.put("index_id", this.index_id);
		values.put("owing", this.owing);
		values.put("paid", this.paid);
		values.put("total", this.total);
		db.insert("si_invoice_statement", null, values);			
	}
	
	public String getPaidAmount(){
		SQLiteDatabase db = DatabaseHelper.getInstance(context).getDB();
		Cursor c = db.rawQuery("select ifnull(sum(ac_amount),0) from si_payment_mobile where ac_inv_id=?", new String[]{this.id});		
		if(c.moveToFirst()){
			String paid=c.getString(0);
			DatabaseHelper.closeCursor(c);			
			return paid;
		}
		DatabaseHelper.closeCursor(c);
		return "0";		
	}
			
}

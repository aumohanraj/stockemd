package com.he.stockem.db.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;

import com.he.stockem.lib.DatabaseHelper;


public class Customer {
	
	 String id="";
	 String domain_id="1";
	 String attention="";
	 String name="";
	 String street_address="";
	 String street_address2="";
	 String city="";
	 String state="";
	 String zip_code="";
	 String country="";
	 String phone="";
	 String mobile_phone="";
	 String fax="";
	 String email="";
	 String credit_card_holder_name="";
	 String credit_card_number="";
	 String credit_card_expiry_month="";
	 String credit_card_expiry_year="";
	 String notes="";
	 String custom_field1="";
	 String custom_field2="";
	 String custom_field3="";
	 String custom_field4="";
	 String enabled="";
	 Double latitude=0.00;
	 Double longitude=0.00;
	 int area_line=0;
	 Context context;

	public Customer(Context context) {
		context=this.context;
	}
	
	//Set Methods for all the attributes of the table
	public void setID(String ID) {
		this.id = ID;
	}
	public void setDomainID(String DomainID){
		this.domain_id=DomainID;
	}
	public void setAttention(String Attention){
		this.attention=Attention;
	}
	public void setName(String Name){
		this.name=Name;
	}
	public void setStreetAddress(String StreetAddress){
		this.street_address=StreetAddress;
	}
	public void setStreetAddress2(String StreetAddress2){
		this.street_address2=StreetAddress2;
	}
	public void setCity(String City){
		this.city=City;
	}
	public void setState(String State){
		this.state=State;
	}
	public void setZipCode(String ZipCode){
		this.zip_code=ZipCode;
	}
	public void setCountry(String Country){
		this.country=Country;
	}
	public void setPhone(String Phone){
		this.phone=Phone;
	}
	public void setMobilePhone(String MobilePhone){
		this.mobile_phone=MobilePhone;
	}
	public void setFax(String Fax){
		this.fax=Fax;
	}
	public void setEmail(String Email){
		this.email=Email;
	}
	public void setCreditCardHolderName(String CreditCardHolderName){
		this.credit_card_holder_name=CreditCardHolderName;
	}
	public void setCreditCardNumber(String CreditCardNumber){
		this.credit_card_number=CreditCardNumber;
	}
	public void setCreditCardExpiryMonth(String CreditCardExpiryMonth){
		this.credit_card_expiry_month=CreditCardExpiryMonth;
	}
	public void setCreditCardExpiryYear(String CreditCardExpiryYear){
		this.credit_card_expiry_year=CreditCardExpiryYear;
	}
	public void setNotes(String Notes){
		this.notes=Notes;
	}
	public void setCustomField1(String CustomField1){
		this.custom_field1=CustomField1;
	}
	public void setCustomField2(String CustomField2){
		this.custom_field2=CustomField2;
	}
	public void setCustomField3(String CustomField3){
		this.custom_field3=CustomField3;
	}
	public void setCustomField4(String CustomField4){
		this.custom_field4=CustomField4;
	}
	public void setEnabled(String Enabled){
		this.enabled=Enabled;
	}
	
	
	
	//Get Methods for all the attributes of the table
	public String getID() {
		return this.id;
	}
	
	public String getDomainID(){
		return this.domain_id;
	}
	public String getAttention(){
		return this.attention;
	}
	public String getName(){
		return this.name;
	}
	public String getStreetAddress(){
		return this.street_address;
	}
	public String getStreetAddress2(){
		return this.street_address2;
	}
	public String getCity(){
		return this.city;
	}
	public String getState(){
		return this.state;
	}
	public String getZipCode(){
		return this.zip_code;
	}
	public String getCountry(){
		return this.country;
	}
	public String getPhone(){
		return this.phone;
	}
	public String getMobilePhone(){
		return this.mobile_phone;
	}
	public String getFax(){
		return this.fax;
	}
	public String getEmail(){
		return this.email;
	}
	public String getCreditCardHolderName(){
		return this.credit_card_holder_name;
	}
	public String getCreditCardNumber(){
		return this.credit_card_number;
	}
	public String getCreditCardExpiryMonth(){
		return this.credit_card_expiry_month;
	}
	public String getCreditCardExpiryYear(){
		return this.credit_card_expiry_year;
	}
	public String getNotes(){
		return this.notes;
	}
	public String getCustomField1(){
		return this.custom_field1;
	}
	public String getCustomField2(){
		return this.custom_field2;
	}
	public String getCustomField3(){
		return this.custom_field3;
	}
	public String getCustomField4(){
		return this.custom_field4;
	}
	public String getEnabled(){
		return this.enabled;
	}

	public int getAreaLine() {
		return area_line;
	}

	public void setAreaLine(int area_line) {
		this.area_line = area_line;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
	public String save(){
		
		DatabaseHelper controller = DatabaseHelper.getInstance(context);
		SQLiteDatabase db = controller.getDB();
		ContentValues values = new ContentValues();
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		
		values.put(DatabaseHelper.KEY_CUSTOMER_AREA_LINE, this.area_line);		
		values.put(DatabaseHelper.KEY_CUSTOMER_DOMAIN_ID,this.domain_id);
		values.put(DatabaseHelper.KEY_CUSTOMER_ATTENTION,this.attention);
		values.put(DatabaseHelper.KEY_CUSTOMER_NAME,this.name);
		values.put(DatabaseHelper.KEY_CUSTOMER_STREET_ADDRESS,this.street_address);
		values.put(DatabaseHelper.KEY_CUSTOMER_STREET_ADDRESS2,this.street_address2);
		values.put(DatabaseHelper.KEY_CUSTOMER_CITY,this.city);
		values.put(DatabaseHelper.KEY_CUSTOMER_STATE,this.state);
		values.put(DatabaseHelper.KEY_CUSTOMER_ZIP_CODE,this.zip_code);
		values.put(DatabaseHelper.KEY_CUSTOMER_COUNTRY,this.country);
		values.put(DatabaseHelper.KEY_CUSTOMER_PHONE,this.phone);
		values.put(DatabaseHelper.KEY_CUSTOMER_MOBILE_PHONE,this.mobile_phone);
		values.put(DatabaseHelper.KEY_CUSTOMER_FAX,this.fax);
		values.put(DatabaseHelper.KEY_CUSTOMER_EMAIL,this.email);
		values.put(DatabaseHelper.KEY_CUSTOMER_CREDIT_CARD_HOLDER_NAME,this.credit_card_holder_name);
		values.put(DatabaseHelper.KEY_CUSTOMER_CREDIT_CARD_NUMBER,this.credit_card_number);
		values.put(DatabaseHelper.KEY_CUSTOMER_CREDIT_CARD_EXPIRY_MONTH,this.credit_card_expiry_month);
		values.put(DatabaseHelper.KEY_CUSTOMER_CREDIT_CARD_EXPIRY_YEAR,this.credit_card_expiry_year);
		values.put(DatabaseHelper.KEY_CUSTOMER_NOTES,this.notes);
		values.put(DatabaseHelper.KEY_CUSTOMER_CUSTOM_FIELD1,this.custom_field1);
		values.put(DatabaseHelper.KEY_CUSTOMER_CUSTOM_FIELD2,this.custom_field2);
		values.put(DatabaseHelper.KEY_CUSTOMER_CUSTOM_FIELD3,this.custom_field3);
		values.put(DatabaseHelper.KEY_CUSTOMER_CUSTOM_FIELD4,this.custom_field4);
		values.put(DatabaseHelper.KEY_CUSTOMER_AREA_LINE,this.area_line);
		values.put(DatabaseHelper.KEY_CUSTOMER_LATITUDE,this.latitude);
		values.put(DatabaseHelper.KEY_CUSTOMER_LONGITUDE,this.longitude);
	
		if(this.id.contentEquals("")){
			this.id= String.valueOf(db.insert(DatabaseHelper.TABLE_CUSTOMER, null, values));
			return this.id;			
		}else{
			db.update(DatabaseHelper.TABLE_CUSTOMER, values, "id=?", new String[] {String.valueOf(this.id)});
			return this.id;
		}
	}
	
	
public String insert(){
		
		DatabaseHelper controller = DatabaseHelper.getInstance(context);
		SQLiteDatabase db = controller.getDB();
		ContentValues values = new ContentValues();
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		
		values.put(DatabaseHelper.KEY_CUSTOMER_ID, this.id);	
		values.put(DatabaseHelper.KEY_CUSTOMER_AREA_LINE, this.area_line);		
		values.put(DatabaseHelper.KEY_CUSTOMER_DOMAIN_ID,this.domain_id);
		values.put(DatabaseHelper.KEY_CUSTOMER_ATTENTION,this.attention);
		values.put(DatabaseHelper.KEY_CUSTOMER_NAME,this.name);
		values.put(DatabaseHelper.KEY_CUSTOMER_STREET_ADDRESS,this.street_address);
		values.put(DatabaseHelper.KEY_CUSTOMER_STREET_ADDRESS2,this.street_address2);
		values.put(DatabaseHelper.KEY_CUSTOMER_CITY,this.city);
		values.put(DatabaseHelper.KEY_CUSTOMER_STATE,this.state);
		values.put(DatabaseHelper.KEY_CUSTOMER_ZIP_CODE,this.zip_code);
		values.put(DatabaseHelper.KEY_CUSTOMER_COUNTRY,this.country);
		values.put(DatabaseHelper.KEY_CUSTOMER_PHONE,this.phone);
		values.put(DatabaseHelper.KEY_CUSTOMER_MOBILE_PHONE,this.mobile_phone);
		values.put(DatabaseHelper.KEY_CUSTOMER_FAX,this.fax);
		values.put(DatabaseHelper.KEY_CUSTOMER_EMAIL,this.email);
		values.put(DatabaseHelper.KEY_CUSTOMER_CREDIT_CARD_HOLDER_NAME,this.credit_card_holder_name);
		values.put(DatabaseHelper.KEY_CUSTOMER_CREDIT_CARD_NUMBER,this.credit_card_number);
		values.put(DatabaseHelper.KEY_CUSTOMER_CREDIT_CARD_EXPIRY_MONTH,this.credit_card_expiry_month);
		values.put(DatabaseHelper.KEY_CUSTOMER_CREDIT_CARD_EXPIRY_YEAR,this.credit_card_expiry_year);
		values.put(DatabaseHelper.KEY_CUSTOMER_NOTES,this.notes);
		values.put(DatabaseHelper.KEY_CUSTOMER_CUSTOM_FIELD1,this.custom_field1);
		values.put(DatabaseHelper.KEY_CUSTOMER_CUSTOM_FIELD2,this.custom_field2);
		values.put(DatabaseHelper.KEY_CUSTOMER_CUSTOM_FIELD3,this.custom_field3);
		values.put(DatabaseHelper.KEY_CUSTOMER_CUSTOM_FIELD4,this.custom_field4);
		values.put(DatabaseHelper.KEY_CUSTOMER_AREA_LINE,this.area_line);
		values.put(DatabaseHelper.KEY_CUSTOMER_LATITUDE,this.latitude);
		values.put(DatabaseHelper.KEY_CUSTOMER_LONGITUDE,this.longitude);
	
		this.id= String.valueOf(db.insert(DatabaseHelper.TABLE_CUSTOMER, null, values));
		return this.id;	
		
		
	}
	
	public int distance(Location loc){
		Location customerLoc = new Location("");
		customerLoc.setLatitude(this.getLatitude());
		customerLoc.setLongitude(this.getLongitude());				
		return (int) customerLoc.distanceTo(loc);
	}
		
}


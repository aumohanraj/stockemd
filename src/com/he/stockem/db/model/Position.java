package com.he.stockem.db.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.he.stockem.lib.DatabaseHelper;


public class Position {
	
	int id=0;
	String deviceid="";
	String devicetime="";
	String fixtime="";
	String valid="valid";
	Double latitude=0.00;
	Double longitude=0.00;
	Double altitude=0.00;
	Float speed=0.0f;
	Float course=0.0f;
	String attributes="attributes";
	Float accuracy=0.0f;
	
	
	Context context;

	public Position(Context context) {
		this.context=context;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDeviceid() {
		return deviceid;
	}

	public void setDeviceid(String deviceid) {
		this.deviceid = deviceid;
	}

	public String getDevicetime() {
		return devicetime;
	}

	public void setDevicetime(String devicetime) {
		this.devicetime = devicetime;
	}

	public String getFixtime() {
		return fixtime;
	}

	public void setFixtime(String fixtime) {
		this.fixtime = fixtime;
	}

	public String getValid() {
		return valid;
	}

	public void setValid(String valid) {
		this.valid = valid;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getAltitude() {
		return altitude;
	}

	public void setAltitude(Double altitude) {
		this.altitude = altitude;
	}

	public Float getSpeed() {
		return speed;
	}

	public void setSpeed(Float speed) {
		this.speed = speed;
	}

	public Float getCourse() {
		return course;
	}

	public void setCourse(Float course) {
		this.course = course;
	}

	public String getAttributes() {
		return attributes;
	}

	public void setAttributes(String attributes) {
		this.attributes = attributes;
	}

	public Float getAccuracy() {
		return accuracy;
	}

	public void setAccuracy(Float accuracy) {
		this.accuracy = accuracy;
	}

	public Context getContext() {
		return context;
	}

	public int getAge(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date=null;
		long diff=-1;
		try {
			 date = dateFormat.parse(this.getDevicetime());
		
		java.util.Date current_date= new java.util.Date();
		
		diff = current_date.getTime() - date.getTime();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (int) (diff/(1000*60));
	}
	
	public int save(){
		DatabaseHelper controller = DatabaseHelper.getInstance(context);
		SQLiteDatabase db = controller.getDB();
		ContentValues values = new ContentValues();
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		
		values.put(DatabaseHelper.KEY_LOCATION_ACCURACY, this.accuracy);
		values.put(DatabaseHelper.KEY_LOCATION_ALTITUDE, this.altitude);
		values.put(DatabaseHelper.KEY_LOCATION_ATTRIBUTES, this.attributes);
		values.put(DatabaseHelper.KEY_LOCATION_COURSE, this.course);
		values.put(DatabaseHelper.KEY_LOCATION_DEVICEID, this.deviceid);
		values.put(DatabaseHelper.KEY_LOCATION_DEVICETIME, this.devicetime);
		values.put(DatabaseHelper.KEY_LOCATION_FIXTIME, this.fixtime);
		values.put(DatabaseHelper.KEY_LOCATION_LATITUDE, this.latitude);
		values.put(DatabaseHelper.KEY_LOCATION_LONGITUDE, this.longitude);
		values.put(DatabaseHelper.KEY_LOCATION_SPEED, this.speed);
		values.put(DatabaseHelper.KEY_LOCATION_VALID, this.valid);
		if(this.id==0){
			this.id=(int) db.insert(DatabaseHelper.TABLE_POSITION, null, values);
			return this.id;			
		}else{
			db.update(DatabaseHelper.TABLE_POSITION, values, "id=?", new String[] {String.valueOf(this.id)});
			return this.id;
		}
	}
}
package com.he.stockem.orders;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.he.stockem.R;
import com.he.stockem.UpdateCustomerLocation;
import com.he.stockem.db.model.Customer;
import com.he.stockem.db.model.Order;
import com.he.stockem.db.model.Position;
import com.he.stockem.db.model.Product;
import com.he.stockem.lib.AsyncRequest;
import com.he.stockem.lib.AsyncRequest.OnAsyncRequestComplete;
import com.he.stockem.lib.AutoCompleteItem;
import com.he.stockem.lib.AutocompleteArrayAdapter;
import com.he.stockem.lib.DatabaseHelper;
import com.he.stockem.lib.FileDownloader;
import com.he.stockem.lib.UtilityFunctions;
import com.he.stockem.payments.PaymentsFragment;
import com.ngx.BluetoothPrinter;

public class UpdateOrderFragment extends Fragment implements OnAsyncRequestComplete, View.OnClickListener {
	
	public UpdateOrderFragment(){}

	Button btnNewOrderAddProd, btnProductDelete, btnRecentProd,btnOrderPrint,btnOrderDownload,btnOrderUpdateCustomerLoc, btnOrderPayment,btnOrderSync, btnOrderNotes;
	DatabaseHelper controller;
	TextView txtUpdateOrderDate, txtUpdateOrderCustID,txtAddProdMRP,txtUpdateOrderID,txtUpdateOrderStatus,txtUpdateOrderTotal;
	EditText ETAddProdQuantity, ETAddProdUnitPrice, ETEditProdQuantity, ETAddProdQuantityFree;
	Order order;
	TableLayout tblNewOrderlayout;
	JSONObject obj;
	String unit_price="-1";
	UtilityFunctions util =new UtilityFunctions();
	ArrayAdapter<AutoCompleteItem> productAdapter;
	int selectedProductID=0;
	String OrderItemID="0";
	
	Cursor OrderProdCursor;
	
	List<String> sIds = new ArrayList<String>();
	List<String> names = new ArrayList<String>();
	SharedPreferences sharedPrefs;		
	public BluetoothPrinter mBtp= BluetoothPrinter.INSTANCE;
	
	Context context;
    int scrWidth, scrWidth80by3, scrWidth10;
    Button btnMenu[];
    LinearLayout llRoot, llHsMain, llMenuGroup1, llMenuGroup2, llMenuGroup3;
    HorizontalScrollView hScrollView;
    
    
		
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		controller = DatabaseHelper.getInstance(getActivity());
        View rootView = inflater.inflate(R.layout.fragment_update_order, container, false);
        sharedPrefs = PreferenceManager
	            .getDefaultSharedPreferences(this.getActivity());
        order = controller.getOrder(getArguments().getString("localOrderID"));
        if(order.getID().isEmpty()){        
        	getActivity().onBackPressed();
        	return rootView;
        }
        
        final SharedPreferences sharedPrefs = PreferenceManager
        .getDefaultSharedPreferences(this.getActivity());
               
        
        llRoot = (LinearLayout) rootView.findViewById(R.id.llroot);
        
		
		DisplayMetrics dm = new DisplayMetrics();
        this.getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        scrWidth = dm.widthPixels;
        int scrWidth1 = dm.widthPixels;
        
        
        scrWidth = (int) (0.74 * scrWidth); // Calculating 80 % Width of Screen for Horizontal Scroll View
         // Calulation 1/3 of Width of  Horizontal Scroll View for 3  menu items
        if(scrWidth1 <500){
        	scrWidth80by3 = (int) (scrWidth / 1.5);
        }else if(scrWidth1 >=500 && scrWidth1 <=800){
        	scrWidth80by3 = (int) (scrWidth / 2);
        }else{
        	scrWidth80by3 = 300;
        }
        
        scrWidth10 = (int) (0.13 * dm.widthPixels);// Calulation 10% Width of Screen for Previous And Next Button

        context = this.getActivity();

        btnMenu = new Button[9];

        llMenuGroup1 = new LinearLayout(context);
        llMenuGroup2 = new LinearLayout(context);
        llMenuGroup3 = new LinearLayout(context);

        llMenuGroup1.setLayoutParams(new LayoutParams(scrWidth,
                LayoutParams.WRAP_CONTENT));
        llMenuGroup2.setLayoutParams(new LayoutParams(scrWidth,
                LayoutParams.WRAP_CONTENT));
        llMenuGroup3.setLayoutParams(new LayoutParams(scrWidth,
                LayoutParams.WRAP_CONTENT));
        llHsMain = new LinearLayout(context);
        llHsMain.setLayoutParams(new LayoutParams(scrWidth,
                LayoutParams.WRAP_CONTENT));
        
        btnNewOrderAddProd = (Button)this.getActivity().getLayoutInflater().inflate(R.layout.top_bar_button, null);
        btnNewOrderAddProd.setBackgroundResource(R.drawable.top_bar_button);
        btnNewOrderAddProd.setId(1);        
        btnNewOrderAddProd.setLayoutParams(new LayoutParams(scrWidth80by3,
              LayoutParams.MATCH_PARENT));
        Drawable img = this.getActivity().getResources().getDrawable( R.drawable.ic_add );
        img.setBounds( 0, 0, 60, 60 );
        btnNewOrderAddProd.setCompoundDrawables( img, null, null, null );
        
        btnNewOrderAddProd.setText("Add Product");
        
        llHsMain.addView(btnNewOrderAddProd);
        
        
        btnRecentProd = (Button)this.getActivity().getLayoutInflater().inflate(R.layout.top_bar_button, null);
        btnRecentProd.setBackgroundResource(R.drawable.top_bar_button);
        btnRecentProd.setId(2);        
        btnRecentProd.setLayoutParams(new LayoutParams(scrWidth80by3,
              LayoutParams.MATCH_PARENT));
            
        img = this.getActivity().getResources().getDrawable( R.drawable.ic_recent );
        img.setBounds( 0, 0, 60, 60 );
        btnRecentProd.setCompoundDrawables( img, null, null, null );
        btnRecentProd.setText("Recent Products");        
        llHsMain.addView(btnRecentProd);
        
        btnOrderPayment = (Button)this.getActivity().getLayoutInflater().inflate(R.layout.top_bar_button, null);
        btnOrderPayment.setId(3);      
        btnOrderPayment.setBackgroundResource(R.drawable.top_bar_button);
        btnOrderPayment.setLayoutParams(new LayoutParams(scrWidth80by3,
              LayoutParams.MATCH_PARENT));
        
        img = this.getActivity().getResources().getDrawable( R.drawable.ic_payments );
        img.setBounds( 0, 0, 60, 60 );
        btnOrderPayment.setCompoundDrawables( img, null, null, null );
        btnOrderPayment.setText("Payment");        
        llHsMain.addView(btnOrderPayment);
        
        
        btnOrderUpdateCustomerLoc = (Button)this.getActivity().getLayoutInflater().inflate(R.layout.top_bar_button, null);
        btnOrderUpdateCustomerLoc.setId(4); 
        btnOrderUpdateCustomerLoc.setBackgroundResource(R.drawable.top_bar_button);
        btnOrderUpdateCustomerLoc.setLayoutParams(new LayoutParams(scrWidth80by3,
              LayoutParams.MATCH_PARENT));        
        img = this.getActivity().getResources().getDrawable( R.drawable.ic_location );
        img.setBounds( 0, 0, 60, 60 );
        btnOrderUpdateCustomerLoc.setCompoundDrawables( img, null, null, null );
        btnOrderUpdateCustomerLoc.setText("Update Location");        
        llHsMain.addView(btnOrderUpdateCustomerLoc);
        
        
        
        btnOrderDownload = (Button)this.getActivity().getLayoutInflater().inflate(R.layout.top_bar_button, null);
        btnOrderDownload.setId(5);     
        btnOrderDownload.setBackgroundResource(R.drawable.top_bar_button);
        btnOrderDownload.setLayoutParams(new LayoutParams(scrWidth80by3,
              LayoutParams.MATCH_PARENT));           
        btnOrderDownload.setText("Download");
        img = this.getActivity().getResources().getDrawable( R.drawable.ic_download );
        img.setBounds( 0, 0, 60, 60 );
        btnOrderDownload.setCompoundDrawables( img, null, null, null );
        llHsMain.addView(btnOrderDownload);
        
        btnOrderPrint = (Button)this.getActivity().getLayoutInflater().inflate(R.layout.top_bar_button, null);
        btnOrderPrint.setId(6);        
        btnOrderPrint.setBackgroundResource(R.drawable.top_bar_button);
        btnOrderPrint.setLayoutParams(new LayoutParams(scrWidth80by3,
              LayoutParams.MATCH_PARENT));        
        img = this.getActivity().getResources().getDrawable( R.drawable.ic_print );
        img.setBounds( 0, 0, 60, 60 );
        btnOrderPrint.setCompoundDrawables( img, null, null, null );
        btnOrderPrint.setText("Print");  
        llHsMain.addView(btnOrderPrint);
        
        
        
        btnOrderSync = (Button)this.getActivity().getLayoutInflater().inflate(R.layout.top_bar_button, null);
        btnOrderSync.setId(7);        
        btnOrderSync.setBackgroundResource(R.drawable.top_bar_button);
        btnOrderSync.setLayoutParams(new LayoutParams(scrWidth80by3,
              LayoutParams.MATCH_PARENT));        
        img = this.getActivity().getResources().getDrawable( R.drawable.ic_sync );
        img.setBounds( 0, 0, 60, 60 );
        btnOrderSync.setCompoundDrawables( img, null, null, null );
        btnOrderSync.setText("Sync");  
        llHsMain.addView(btnOrderSync);
        
        btnOrderNotes = (Button)this.getActivity().getLayoutInflater().inflate(R.layout.top_bar_button, null);
        btnOrderNotes.setId(8);        
        btnOrderNotes.setBackgroundResource(R.drawable.top_bar_button);
        btnOrderNotes.setLayoutParams(new LayoutParams(scrWidth80by3,
              LayoutParams.MATCH_PARENT));        
        img = this.getActivity().getResources().getDrawable( R.drawable.notes );
        img.setBounds( 0, 0, 60, 60 );
        btnOrderNotes.setCompoundDrawables( img, null, null, null );
        btnOrderNotes.setText("Notes");  
        llHsMain.addView(btnOrderNotes);
//        for (int i = 1; i < btnMenu.length; i++) {
//            // Starting from int i=1  to Set Menu Item Counting from 1 like Menu 1 instead of Menu 0
//            btnMenu[i] = new Button(context);
//            btnMenu[i].setId(i);
//            btnMenu[i].setOnClickListener(this);
//            btnMenu[i].setLayoutParams(new LayoutParams(scrWidth80by3,
//                    LayoutParams.WRAP_CONTENT));
//            btnMenu[i].setBackgroundColor(Color.TRANSPARENT);
//            Drawable img = this.getActivity().getResources().getDrawable( R.drawable.ic_orders );
//            btnMenu[i].setCompoundDrawablesWithIntrinsicBounds( img, null, null, null);
//            btnMenu[i].setText("Menu" + i);
//            llHsMain.addView(btnMenu[i]);
//            if (i <= 3) {
//                llMenuGroup1.addView(btnMenu[i]);
//            } else if(i>3 && i<=6){
//                llMenuGroup2.addView(btnMenu[i]);
//            }else{
//            	llMenuGroup3.addView(btnMenu[i]);
//            }
//        }

        ImageButton btnNext = new ImageButton(context);
        btnNext.setOnClickListener(this);
        btnNext.setImageResource(R.drawable.ic_forward);
        btnNext.setId(0);
        ImageButton btnPre = new ImageButton(context);
        btnPre.setOnClickListener(this);
        btnPre.setImageResource(R.drawable.ic_back);
        btnPre.setPadding(3, 3, 3, 3);
        btnPre.setId(-1);
        

        btnNext.setLayoutParams(new LayoutParams(scrWidth10,
                LayoutParams.MATCH_PARENT));
        
        
        
        btnPre.setLayoutParams(new LayoutParams(scrWidth10,
                LayoutParams.MATCH_PARENT));
        
        
        

        //  hScrollView its HorizontalScrollView ! 
        hScrollView = new HorizontalScrollView(context);
        hScrollView.setLayoutParams(new LayoutParams(scrWidth,
                LayoutParams.WRAP_CONTENT));

        // As HorizontalScrollView can Have one Direct Child ! so llHsMain is one Direct Child of hScrollView!
        

        //Adding Menu Group1 And Menu Group 2 to  llHsMain as its Direct Child of hScrollView
//        llHsMain.addView(llMenuGroup1);
//        llHsMain.addView(llMenuGroup2);
//        llHsMain.addView(llMenuGroup3);

        //Adding Direct Child llHsMain to hScrollView
        hScrollView.addView(llHsMain);

        //  Adding views to llRoot (its Main Root Layout) ! 
        llRoot.addView(btnPre);
        llRoot.addView(hScrollView);
        llRoot.addView(btnNext);

		
        
        ArrayList<AutoCompleteItem> productArray = new  ArrayList<AutoCompleteItem>();
		productArray  = (ArrayList<AutoCompleteItem>) controller.getAllProductsItems();
		productAdapter = new AutocompleteArrayAdapter(getActivity(), R.layout.list_view_row_item, productArray);
		
		tblNewOrderlayout = (TableLayout)rootView.findViewById(R.id.tblNewOrderProducts);
		txtUpdateOrderDate = (TextView)rootView.findViewById(R.id.txtUpdateOrderDate);
		txtUpdateOrderCustID = (TextView)rootView.findViewById(R.id.txtUpdateOrderCustID);
		txtUpdateOrderID= (TextView)rootView.findViewById(R.id.txtUpdateOrderID);
		txtUpdateOrderStatus= (TextView)rootView.findViewById(R.id.txtUpdateOrderStatus);
		txtUpdateOrderTotal = (TextView)rootView.findViewById(R.id.txtUpdateOrderTotal);		
		
		
		
		if(order==null){
			Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.unable_to_get_order), 10).show();			
		}else{
			txtUpdateOrderCustID.setText("Customer Name : "+order.getCustomer().getName());
			txtUpdateOrderDate.setText("Date : "+order.getDate());
			if(order.getIndexID().contentEquals("")||order.getIndexID().contentEquals("0")){
				txtUpdateOrderID.setText("ID: " );								
			}else{				
				txtUpdateOrderID.setText("ID: " + order.getIndexID());
			}	 
			
			txtUpdateOrderStatus.setText("Status : Synced");
			
			if(order.getIndexID().contentEquals("0")){
				txtUpdateOrderStatus.setText("Status: Not Synced");	
				
			}else{
				
			}
		}
		BuildOrderItemsTable();
		
		if(order.getTypeID().contentEquals(Order.KEY_TYPE_INVOICE)){
			btnOrderDownload.setVisibility(View.VISIBLE);
		}else{
			btnOrderDownload.setVisibility(View.GONE);
		}
		btnOrderDownload.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent i = new Intent(Intent.ACTION_VIEW);		    
			    i.setData(Uri.parse( sharedPrefs.getString("prefURL", "")+"?module=api&view=invoice_api&id=5&format=print&index_id="+order.getIndexID() ));
		        try{
		        	startActivity(i);	        	
		        }catch(ActivityNotFoundException e){
		        	Log.e("UpdaetOrderFragment","OrderDownload",e);
		            Toast.makeText(getActivity(), "No Application available to view HTML", Toast.LENGTH_SHORT).show();
		        }

//				ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
//				params.add(new BasicNameValuePair("module", "export"));
//				params.add(new BasicNameValuePair("view", "invoice"));
//				params.add(new BasicNameValuePair("id", order.getIndexID()));
//				params.add(new BasicNameValuePair("format", "print"));	
//				params.add(new BasicNameValuePair("index_id", order.getIndexID()));	
//				String url=getResources().getString(R.string.server);
//				AsyncRequest getPosts = new AsyncRequest(UpdateOrderFragment.this, "GET", params,"getpdf",getResources().getString(R.string.getting_price));
//				getPosts.execute(url);
				//new DownloadFile().execute("http://maven.apache.org/maven-1.x/maven.pdf", "maven.pdf"); 								
				}

		});
		
		btnOrderSync.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Cursor tmpOrderProdCursor=controller.getOrderProducts(getArguments().getString("localOrderID"));
				
				if(tmpOrderProdCursor==null || !tmpOrderProdCursor.moveToFirst()){			
					Toast.makeText(v.getContext(), "No Products", 10).show();
					DatabaseHelper.closeCursor(tmpOrderProdCursor);
					return;
				}	
				DatabaseHelper.closeCursor(tmpOrderProdCursor);
				ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();				
				String url=getResources().getString(R.string.server)+"?module=api/orders&view=save";
				AsyncRequest getPosts = new AsyncRequest(UpdateOrderFragment.this, "POST", controller.prepareOrderSyncParameter(params, order.getID()),"syncorder",getResources().getString(R.string.synchronizing));
				getPosts.execute(url);	
				
			}
		});
		
		btnOrderNotes.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder alert = new AlertDialog.Builder(UpdateOrderFragment.this.getActivity());
				alert.setTitle("Notes");				
				// Create TextView
				final EditText input = new EditText (UpdateOrderFragment.this.getActivity());
				input.setHint("Your Notes goes here");
				input.setText(controller.getOrder(getArguments().getString("localOrderID")).getNote());
				alert.setView(input);
				
				alert.setPositiveButton("Save", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					Order order = controller.getOrder(getArguments().getString("localOrderID"));
					order.setNote(input.getText().toString());
					order.save();						
				  }
				});

				  alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				  public void onClick(DialogInterface dialog, int whichButton) {
				      // Canceled.
				  }
				});
				  AlertDialog dialog = alert.create();
				  dialog.show();
				  
				  if(!controller.getOrder(getArguments().getString("localOrderID")).getIndexID().contentEquals("0")){
					  dialog.getButton(AlertDialog.BUTTON1).setEnabled(false);
				  }
				  
				//alert.show();
			}
		});
		
		btnOrderUpdateCustomerLoc.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Bundle args = new Bundle();
				args.putString("localOrderID", getArguments().getString("localOrderID"));				
				UpdateCustomerLocation nextFrag= new UpdateCustomerLocation();
				nextFrag.setArguments(args);
			     getActivity().getFragmentManager().beginTransaction()
			     .replace(R.id.frame_container, nextFrag)											     
			     .addToBackStack("Update Customer Location")
			     .commit();
				
				/*
				Position position = controller.getLastKnownLocation();
				if(position == null){
					Toast.makeText(getActivity(), "Location not available", 10).show();
					return;
				}
				if(position.getAge()>=3 || position.getAge()<0 || position == null){
					Toast.makeText(getActivity(), "Location not available", 10).show();
					return;
				}
				
				if(position.getAccuracy() >=30){
					Toast.makeText(getActivity(), "Location not available", 10).show();
					return;
				}

				Customer customer = order.getCustomer();
				customer.setLatitude(position.getLatitude());
				customer.setLongitude(position.getLongitude());
				customer.save();
				
				ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair("module", "api"));
				params.add(new BasicNameValuePair("view", "updatecustomerloc"));
				params.add(new BasicNameValuePair("customerid", order.getCustomerID()));
				params.add(new BasicNameValuePair("latitude", String.valueOf(position.getLatitude())));
				params.add(new BasicNameValuePair("longitude", String.valueOf(position.getLongitude())));
				params.add(new BasicNameValuePair("accuracy", String.valueOf(position.getAccuracy())));
				String url=getResources().getString(R.string.server);
				AsyncRequest getPosts = new AsyncRequest(UpdateOrderFragment.this, "GET", params,"updatecustomerlocation","Updating location.. Please wait");
				getPosts.execute(url);
				*/
			}
		});
		
		
		btnOrderPrint.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				OrderProdCursor=controller.getOrderProducts(getArguments().getString("localOrderID"));
				if(OrderProdCursor==null || !OrderProdCursor.moveToFirst()){			
					txtUpdateOrderTotal.setText(String.valueOf("No products in the order to print"));
					DatabaseHelper.closeCursor(OrderProdCursor);
					return;
				}
				
				
				mBtp.printTextLine("Hari Enteprises");
				mBtp.printTextLine("TIN : 33246335660");
				mBtp.printTextLine("P-36, Kakkan Colony Extn,");
				mBtp.printTextLine("K.K.Nagar, Trichy -21");
				mBtp.printTextLine("Ph : 9092747505, 9092553335");
				
				mBtp.printLineFeed();				
				mBtp.printTextLine("Invoice No : " + order.getIndexID());
				mBtp.printTextLine("Invoice Date : " + order.getDate());
				mBtp.printLineFeed();		
				
				mBtp.printTextLine("-----------Customer Summary-------------");
				mBtp.printTextLine(order.getCustomer().getName());
//				mBtp.printTextLine(order.getCustomer().getCustomField1());
//				
//				if(!order.getCustomer().getCustomField2().isEmpty()){
//					mBtp.printTextLine(order.getCustomer().getCustomField2());					
//				}				
//				mBtp.printTextLine(order.getCustomer().getStreetAddress());
				mBtp.printLineFeed();
				
					
					
					String[] keys={"description","OrderUnitPrice","quantity","total"};
					String[] col_names={"Product","Landing Price","Quantity","Total"};
					
					mBtp.printTextLine("-----------------Items------------------");	
					do {
						String key = (String)keys[0];				
						mBtp.printTextLine(OrderProdCursor.getString(OrderProdCursor.getColumnIndex(key))+","+OrderProdCursor.getString(OrderProdCursor.getColumnIndex("custom_field1")));
						String line2 ="";
						line2 = "Qty:" +OrderProdCursor.getString(OrderProdCursor.getColumnIndex(keys[2]));
						line2 = String.format("%-10s", line2);
						line2 = line2+"Cost:"+OrderProdCursor.getString(OrderProdCursor.getColumnIndex(keys[1]));
						line2 = String.format("%-25s", line2);
						String price = OrderProdCursor.getString(OrderProdCursor.getColumnIndex(keys[3]));
						int priceLength = price.length();
						line2 = String.format("%-"+String.valueOf(25+10-priceLength)+"s", line2);
						line2 = line2+OrderProdCursor.getString(OrderProdCursor.getColumnIndex(keys[3]));
						
						mBtp.printTextLine(line2);
						mBtp.printTextLine("----------------------------------------");
					}while (OrderProdCursor.moveToNext());
					mBtp.printText(String.format("%-"+String.valueOf(25+12-txtUpdateOrderTotal.getText().toString().replace("Total : ", "").length())+"s", ""));
					mBtp.printTextLine(txtUpdateOrderTotal.getText().toString().replace("Total : ", ""));
					mBtp.printLineFeed();
					mBtp.printLineFeed();
					mBtp.printLineFeed();
					mBtp.printLineFeed();
					mBtp.printText("For Hari Enterprises");
					mBtp.printLineFeed();
					mBtp.printLineFeed();
					mBtp.printLineFeed();
					mBtp.printLineFeed();
					mBtp.printTextLine("Goods received");
					mBtp.printLineFeed();
					mBtp.printLineFeed();
					DatabaseHelper.closeCursor(OrderProdCursor);
				
			}
		});
		
		btnRecentProd.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {				
				// TODO Auto-generated method stub
				Bundle args = new Bundle();
				args.putString("localOrderID", getArguments().getString("localOrderID"));
				RecentProductsFragment nextFrag= new RecentProductsFragment();
				nextFrag.setArguments(args);
			     getActivity().getFragmentManager().beginTransaction()
			     .replace(R.id.frame_container, nextFrag)											     
			     .addToBackStack("Recent Product")
			     .commit();
				
				
			}
		});
		
		if(sharedPrefs.getString("prefAddProduct", "popup").toLowerCase().contentEquals("list")){
			btnNewOrderAddProd.setOnClickListener(new View.OnClickListener() {				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Bundle args = new Bundle();
					args.putString("localOrderID", getArguments().getString("localOrderID"));
					AddProductFragment nextFrag= new AddProductFragment();
					nextFrag.setArguments(args);
				     getActivity().getFragmentManager().beginTransaction()
				     .replace(R.id.frame_container, nextFrag)											     
				     .addToBackStack("Add Product Fragment")
				     .commit();				
				}
			});
		
		}else{
		btnNewOrderAddProd.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				LayoutInflater layoutInflater = LayoutInflater.from(v.getContext());
				View promptView = layoutInflater.inflate(R.layout.add_product_popup, null);
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(v.getContext());
				alertDialogBuilder.setView(promptView);
				
				final AutoCompleteTextView SelectProduct = (AutoCompleteTextView) promptView.findViewById(R.id.ACSelectProduct);
				final Spinner prodSpinCategory = (Spinner) promptView.findViewById(R.id.spinnerProductCategory);
				final Spinner productsSpin = (Spinner) promptView.findViewById(R.id.spinnerProducts);
				final ArrayAdapter<String> productsSpinnerAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, names);
				if(sharedPrefs.getString("prefAddProductCategory", "supplier").toLowerCase().contentEquals("supplier")){
					ArrayAdapter<String> categoryAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, controller.getAllProductSupplier());    			    
				    categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); 
				    prodSpinCategory.setAdapter(categoryAdapter);
				    
				        			    
				    productsSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				    productsSpin.setAdapter(productsSpinnerAdapter);
				    
				}else{
					ArrayAdapter<String> categoryAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, controller.getAllProductManufacturer());    			    
				    categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); 
				    prodSpinCategory.setAdapter(categoryAdapter);
				    
				        			    
				    productsSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				    productsSpin.setAdapter(productsSpinnerAdapter);
				}
				
			    
			    
			    
			    prodSpinCategory.setOnItemSelectedListener(new OnItemSelectedListener(){

					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						// TODO Auto-generated method stub
						String item = parent.getItemAtPosition(position).toString();
						names.clear();
						sIds.clear();
						sIds.add("0");
						names.add("Please Select");
						if(sharedPrefs.getString("prefAddProductCategory", "supplier").toLowerCase().contentEquals("supplier")){
							for(Product product : controller.getAllProductsBySupplier(item,getArguments().getString("localOrderID"))){
								names.add(product.getDisplayName());
								sIds.add(product.getId());
							}							
						}else{
							for(Product product : controller.getAllProductsByManufacturer(item,getArguments().getString("localOrderID"))){
								names.add(product.getDisplayName());
								sIds.add(product.getId());
							}
						}
						
						productsSpinnerAdapter.notifyDataSetChanged();
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {
						// TODO Auto-generated method stub
						
					}
			    	
			    });
			    
			    productsSpin.setOnItemSelectedListener(new OnItemSelectedListener(){

					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						// TODO Auto-generated method stub
						selectedProductID= Integer.parseInt(sIds.get(position));
						if(selectedProductID != 0){
							getPrice(order.getCustomerID(), String.valueOf(selectedProductID));
						}else{
							ETAddProdUnitPrice.setText("");
						}
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {
						// TODO Auto-generated method stub
						
					}
			    	
			    });
				SelectProduct.setAdapter(productAdapter);	
				txtAddProdMRP = (TextView)promptView.findViewById(R.id.txtAddProdMRP);
				ETAddProdQuantity = (EditText)promptView.findViewById(R.id.ETAddProdQuantity);
				ETAddProdQuantityFree = (EditText)promptView.findViewById(R.id.ETAddProdQuantityFree);
				ETAddProdUnitPrice= (EditText)promptView.findViewById(R.id.ETAddProdUnitPrice);
				//ETAddProdUnitPrice.setEnabled(false);
				SelectProduct.setOnItemClickListener(new OnItemClickListener() {
			        @Override
			        public void onItemClick(AdapterView<?> parent, View arg1, int pos,
			                long id) {
			        	AutoCompleteItem selectedItem = (AutoCompleteItem)(parent.getItemAtPosition(pos));
			        	SelectProduct.setText(selectedItem.getName());
			        	selectedProductID=selectedItem.getID();
			        	getPrice(order.getCustomerID(), String.valueOf(selectedItem.getID()));
			        	
			        }
			    });
				
				alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton("Add Product", new DialogInterface.OnClickListener() {
							@SuppressLint("NewApi") public void onClick(DialogInterface dialog, int id) {
								// get user input and set it to result					
								if(ETAddProdQuantity.getText().toString().trim().contentEquals("")||ETAddProdQuantity.getText().toString().trim().contentEquals("0")){
									Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.quantity_required), 10).show();
									return;
								}										
								
								if(selectedProductID==0){
									Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.product_required), 10).show();
									return;
								}
								String orderID=getArguments().getString("localOrderID");
								Cursor prodCursor=controller.getProduct(String.valueOf(selectedProductID));
								String tax_id=prodCursor.getString(prodCursor.getColumnIndex("tax_id"));
								String tax_percentage = prodCursor.getString(prodCursor.getColumnIndex("tax_percentage"));
								String price = ETAddProdUnitPrice.getText().toString();
								String quantity = ETAddProdQuantity.getText().toString();
								controller.insertProduct(orderID, quantity, String.valueOf(selectedProductID), price, tax_percentage, ETAddProdQuantityFree.getText().toString(), tax_id);
								
								BuildOrderItemsTable();							
							
							}
						})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,	int id) {
								dialog.cancel();
							}
						});

		// create an alert dialog
		AlertDialog alertD = alertDialogBuilder.create();

		alertD.show();				
	
				
			}
		});
		
		}
		
		
		
		btnOrderPayment.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Bundle args = new Bundle();
				args.putString("paymentCustomerID", String.valueOf(order.getCustomerID()));
				args.putString("paymentCustomerName", String.valueOf(order.getCustomer().getName()));
				PaymentsFragment nextFrag= new PaymentsFragment();	
				UpdateOrderFragment.this.getActivity().getActionBar().setTitle("Payments");
				nextFrag.setArguments(args);
			    UpdateOrderFragment.this.getActivity().getFragmentManager().beginTransaction()			    
			     .replace(R.id.frame_container, nextFrag)											     
			     .addToBackStack("Payment Fragment")
			     .commit();				
			}
		});
		
		if(order.getIndexID().contentEquals("0")){			
			btnRecentProd.setVisibility(View.VISIBLE);
			btnNewOrderAddProd.setVisibility(View.VISIBLE);
			btnOrderSync.setVisibility(View.VISIBLE);			
		}else{
			btnRecentProd.setVisibility(View.GONE);
			btnNewOrderAddProd.setVisibility(View.GONE);
			btnOrderSync.setVisibility(View.GONE);			
		}
		
		Customer customer = order.getCustomer();
		Position position = controller.getLastKnownLocation();
		if(position == null ||position.getAge()>=3 || position.getAge()<0 || position == null || position.getAccuracy() >=30){			
			System.out.println("Location not available");
		}else if(customer.getLatitude() == 0){
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this.getActivity());
			alertDialogBuilder.setTitle("Update");
			alertDialogBuilder.setMessage("Do you want to update the current location as store location ");
			alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {						
					btnOrderUpdateCustomerLoc.performClick();
				}
			});
			
			alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {						
					dialog.dismiss();
				}
			});
			
			alertDialogBuilder.show();
			System.out.println("Customer doesnot have locations");
			
		}
		
		if(controller.getConfigValue("online").contentEquals("true")){
        	
        }else{
        	
        	if(!sharedPrefs.getString("prefWorkingMode", "offline").toLowerCase().contentEquals("offline")){
        		btnRecentProd.setVisibility(View.GONE);        		
			}
        	btnOrderDownload.setVisibility(View.GONE);
        	btnOrderUpdateCustomerLoc.setVisibility(View.GONE);        	
        	btnOrderSync.setVisibility(View.GONE);
        }

        return rootView;
    }
	
	public void getPrice(String customerID, String ProductID){		
		if(sharedPrefs.getString("prefWorkingMode", "online").toLowerCase().contentEquals("online")){
			
			
			ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("module", "invoices"));
			params.add(new BasicNameValuePair("view", "product_ajax"));
			params.add(new BasicNameValuePair("id", ProductID));
			params.add(new BasicNameValuePair("customer_id", customerID));		
			String url=getResources().getString(R.string.server);
			AsyncRequest getPosts = new AsyncRequest((Fragment)this, "GET", params,"getprice",getResources().getString(R.string.getting_price));
			getPosts.execute(url);
		}else{
			
			
			asyncResponse(  controller.getCustomerProductPrice(ProductID,customerID),"getprice");
		}
	}

	
	public void asyncResponse(String response, String label) {
		// TODO Auto-generated method stub
		
		response = response.trim();
		if(label.contentEquals("syncorder")){
			if(!response.contentEquals("")){
				try{
					Integer.parseInt(response);
					controller.UpdatetOrderIndexID(String.valueOf(order.getID()), response);
					txtUpdateOrderID.setText("ID: "+response);
					txtUpdateOrderStatus.setText("Status : Synced");
					btnOrderSync.setVisibility(View.GONE);
					btnRecentProd.setVisibility(View.GONE);
					btnNewOrderAddProd.setVisibility(View.GONE);
					
				}catch(Exception e){
					Log.e("UpdaetOrderFragment","Sync Order"+String.valueOf(order.getID()),e);
					Toast.makeText(this.getActivity(), "Unable to sync the order ", 10).show();
				}				
				
			}else{
				Toast.makeText(getActivity(), "Unable to sync", 10).show();
			}
			return;
		}
		
		if(label.contentEquals("updatecustomerlocation")){
			if(response.contentEquals("OK")){
				Toast.makeText(getActivity(), "Location updated", 10).show();
			}
			return;
		}
		if(label.contentEquals("getpdf")){
			Toast.makeText(getActivity(), "Invoice downloaded successfully", 10).show();
			File pdfFile = new File(Environment.getExternalStorageDirectory() + "/invoices/" + response);  // -> filename = maven.pdf	        
	        
//	        Intent browserIntent = new Intent(Intent.ACTION_VIEW);
//	        browserIntent.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
//	        browserIntent.setData(Uri.fromFile(pdfFile));
//	        startActivity(browserIntent);
//			Intent i = new Intent(Intent.ACTION_VIEW);
//			i.setPackage("com.dynamixsoftware.printershare");
//			i.setDataAndType(Uri.fromFile(pdfFile),"text/html");
//			Intent i = new Intent("android.intent.action.MAIN");
//		    i.setComponent(ComponentName.unflattenFromString("com.android.chrome/com.android.chrome.Main"));
//		    i.addCategory("android.intent.category.LAUNCHER");
//		    i.setData(Uri.fromFile(pdfFile));
			
		    Intent i = new Intent(Intent.ACTION_VIEW);		    
		    i.setData(Uri.fromFile(pdfFile));
	        try{
	        	startActivity(i);	        	
	        }catch(ActivityNotFoundException e){
	        	Log.e("UpdaetOrderFragment","getPDF",e);
	            Toast.makeText(getActivity(), "No Application available to view HTML", Toast.LENGTH_SHORT).show();
	        }
			System.out.println("Downloaded");
			return;
		}
		String unit_price="";
		if(response.contentEquals("")){
			ETAddProdUnitPrice.setText(unit_price);
		}else if(response.contains("unit_price")){			
			try {
				JSONObject obj  = new JSONObject(response);
				unit_price = obj.getString("unit_price");
				String tax_percentage= obj.getString("default_tax_percentage");				
				
				unit_price = unit_price.trim();
				if(unit_price.contentEquals("") || tax_percentage.trim().contentEquals("")){
					unit_price="";
				}else{
					double dbl_cost= Double.parseDouble(unit_price);
					double dbl_tax_percentage= Double.parseDouble(tax_percentage);
					double dbl_unit_price = dbl_cost + (dbl_cost*(dbl_tax_percentage/100));
					unit_price = String.valueOf(dbl_unit_price);
				}
				
				final String quantity= obj.getString("quantity");
				final String free= obj.getString("free");
								
				ETAddProdQuantity.addTextChangedListener(new TextWatcher(){					   
					   @Override
					   public void afterTextChanged(Editable s) {
						   String changedText=s.toString();
						   if(changedText.isEmpty() || changedText.contentEquals("0") || quantity.contentEquals("0")||free.contentEquals("0") || quantity.isEmpty()||free.isEmpty()){
							   return;
						   }else{
							   int int_quantity= Integer.parseInt(quantity);
							   int int_free= Integer.parseInt(free);
							   int int_text = Integer.parseInt(changedText);
							   int_text = int_text - (int_text%int_quantity);							   
							   ETAddProdQuantityFree.setText(String.valueOf((int_text/int_quantity)*int_free)); 
						   }
					   }
					 		
					   @Override    
					   public void beforeTextChanged(CharSequence s, int start,
					     int count, int after) {
					   }

					   @Override    
					   public void onTextChanged(CharSequence s, int start,
					     int before, int count) {
					      
					   }
					
				}); 

				
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				Log.e("UpdaetOrderFragment","asyncreponse",e);
				unit_price="";
				
				
			}
			
		}
		
		ETAddProdUnitPrice.setText(util.StringroundTwoDecimals(unit_price));
		
	}
	
	public void BuildOrderItemsTable() {
		OrderProdCursor=controller.getOrderProducts(getArguments().getString("localOrderID"));
		tblNewOrderlayout.removeAllViews();
		if(OrderProdCursor==null || !OrderProdCursor.moveToFirst()){			
			txtUpdateOrderTotal.setText(String.valueOf("Total : " +"No Products added"));
			DatabaseHelper.closeCursor(OrderProdCursor);
			return;
		}	
			
			String[] keys={"ItemID","description","OrderUnitPrice","quantity", "free_qty","total"};
			String[] col_names={"ItemID","Product","Landing Price","Quantity", "Free","Total"};
			
			//Updating the table header
			TableRow header = new TableRow(getActivity());
			header.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
					  LayoutParams.WRAP_CONTENT));
			for(int j=0; j<col_names.length;j++){
				String key = (String)col_names[j];
				TextView tv = new TextView(getActivity());
				  tv.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
						  LayoutParams.WRAP_CONTENT));
				  tv.setBackgroundResource(R.drawable.cell_shape);
				  tv.setPadding(5, 5, 5, 5);
				  tv.setText(key);
				  if(key.contentEquals("ItemID")){
					  tv.setVisibility(View.GONE);
				  }
				  
				  header.addView(tv);
			}
			tblNewOrderlayout.addView(header);
			double total_value=0;
			
			do {
				TableRow row = new TableRow(getActivity());
				  row.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
						  LayoutParams.WRAP_CONTENT));
				  
				for(int j=0; j<keys.length;j++){
					  String key = (String)keys[j];					  
					  TextView tv = new TextView(getActivity());
					  tv.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
							  LayoutParams.WRAP_CONTENT));
					  tv.setBackgroundResource(R.drawable.cell_shape);
					  tv.setPadding(5, 5, 5, 5);
					  tv.setText(OrderProdCursor.getString(OrderProdCursor.getColumnIndex(key)));
					  if(key.contentEquals("description")){
						  String productName="";
						  productName = OrderProdCursor.getString(OrderProdCursor.getColumnIndex(key));
						  if(!OrderProdCursor.getString(OrderProdCursor.getColumnIndex("custom_field1")).contentEquals("") &&!OrderProdCursor.getString(OrderProdCursor.getColumnIndex("custom_field1")).contentEquals("null")){
							  productName = productName+", "+OrderProdCursor.getString(OrderProdCursor.getColumnIndex("custom_field1"));
						  }
						  if(!OrderProdCursor.getString(OrderProdCursor.getColumnIndex("custom_field2")).contentEquals("") &&!OrderProdCursor.getString(OrderProdCursor.getColumnIndex("custom_field1")).contentEquals("null")){
							  productName = productName+", MRP:"+OrderProdCursor.getString(OrderProdCursor.getColumnIndex("custom_field2"));
						  }
						  tv.setText(productName);
					  }
					  if(key.contentEquals("total") && !OrderProdCursor.getString(OrderProdCursor.getColumnIndex(key)).contentEquals("")){
						  total_value = total_value +Double.parseDouble(OrderProdCursor.getString(OrderProdCursor.getColumnIndex(key)));
					  }
					  if(key.contentEquals("ItemID")){
						  tv.setVisibility(View.GONE);
					  }
					  
					  row.addView(tv);					  
				  }
				
				if(order.getIndexID().contentEquals("0")){	
					row.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							ViewGroup vg = (ViewGroup)v;
							createEditDeleteDialog(((TextView)vg.getChildAt(0)).getText().toString(),((TextView)vg.getChildAt(1)).getText().toString()+", Free :" +((TextView)vg.getChildAt(4)).getText().toString(),((TextView)vg.getChildAt(3)).getText().toString()).show();
							//Toast.makeText(v.getContext(), ((TextView)vg.getChildAt(0)).getText(), 10).show();
							
						}
					});
				}
				  
				  
				tblNewOrderlayout.addView(row);							    
			}while (OrderProdCursor.moveToNext());
			txtUpdateOrderTotal.setText(String.valueOf("Total : " +order.getOrderTotal()));
			DatabaseHelper.closeCursor(OrderProdCursor);
	}
	
public AlertDialog createEditDeleteDialog(final String productItemID,String message, String quantity){
		
		LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
		View promptView = layoutInflater.inflate(R.layout.edit_product_popup, null);
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				getActivity());
		alertDialogBuilder.setView(promptView);	
		
		alertDialogBuilder.setTitle(getActivity().getResources().getString(R.string.edit));
		ETEditProdQuantity = (EditText)promptView.findViewById(R.id.ETEditProdQuantity);		
		ETEditProdQuantity.setText(quantity);
		btnProductDelete = (Button)promptView.findViewById(R.id.btnProductDelete);		
		alertDialogBuilder
		.setMessage(productItemID+","+message)
		.setCancelable(false)
		.setPositiveButton(getActivity().getResources().getString(R.string.delete),new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				if(!order.getIndexID().contentEquals("0")){
					Toast.makeText(getActivity(), "Order/Invoice synced. Cannot modify", 10).show();
					return;
				}
				controller.deleteOrderItem(productItemID);
				dialog.cancel();
				BuildOrderItemsTable();
				Toast.makeText(getActivity(), "Product deleted", 10).show();
				
			}
		  })
		.setNegativeButton(getActivity().getResources().getString(R.string.cancel),new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				// if this button is clicked, just close
				// the dialog box and do nothing	
				dialog.cancel();		
				
			}
		});

		// create alert dialog
		final AlertDialog alertDialog = alertDialogBuilder.create();
		btnProductDelete.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				alertDialog.cancel();
				if(!order.getIndexID().contentEquals("0")){
					Toast.makeText(getActivity(), "Order/Invoice synced. Cannot modify", 10).show();
					return;
				}
				if(ETEditProdQuantity.getText().toString().trim().contentEquals("")||ETEditProdQuantity.getText().toString().trim().contentEquals("0")){
					Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.quantity_required), 10).show();
					return;
				}
				
				controller.updateOrderItem(productItemID, ETEditProdQuantity.getText().toString().trim());
				Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.order_updated), 10).show();
				BuildOrderItemsTable();						
			}
		});
		
		return alertDialog;
	}

private class DownloadFile extends AsyncTask<String, Void, Void>{

    @Override
    protected Void doInBackground(String... strings) {
        String fileUrl = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf
        String fileName = strings[1];  // -> maven.pdf
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        File folder = new File(extStorageDirectory, "testthreepdf");
        folder.mkdir();

        File pdfFile = new File(folder, fileName);

        try{
            pdfFile.createNewFile();
        }catch (IOException e){
        	Log.e("UpdaetOrderFragment","DownloadFile",e);
        }
        FileDownloader.downloadFile(fileUrl, pdfFile);
        return null;
    }
}

@Override
public void onClick(View v) {
	// TODO Auto-generated method stub
	switch (v.getId()) {
    case -1: // Previous Button Clicked
    	
        hScrollView.scrollBy(-scrWidth, 0); // see -ve sign to scroll back horizontally only scrWidth is 80% of Total Screen
        break;
    case 0: // Next Button Clicked
        hScrollView.scrollBy(scrWidth, 0); // scrolls next horizontally only scrWidth is 80% of Total Screen
        break;

    case 1: // Menu Button1 Clicked
        Toast.makeText(v.getContext(), "Menu 1 Clicked",1).show();
        break;
    case 2: // Menu Button1 Clicked
        Toast.makeText(v.getContext(), "Menu 2 Clicked",1).show();
        break;
    case 3: // Menu Button1 Clicked
        Toast.makeText(v.getContext(), "Menu 3 Clicked",1).show();
        break;
    case 4: // Menu Button1 Clicked
        Toast.makeText(v.getContext(), "Menu 4 Clicked",1).show();
        break;
    case 5: // Menu Button1 Clicked
        Toast.makeText(v.getContext(), "Menu 5 Clicked",1).show();
        break;
        // repeat up to 6

    case 6:
        Toast.makeText(v.getContext(), "Menu 6 Clicked",1).show();
        break;
    default:
        break;
    }
	
}

}
package com.he.stockem.orders;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.he.stockem.R;
import com.he.stockem.db.model.CustomerProduct;
import com.he.stockem.db.model.Order;
import com.he.stockem.db.model.Product;
import com.he.stockem.lib.AsyncRequest;
import com.he.stockem.lib.AsyncRequest.OnAsyncRequestComplete;
import com.he.stockem.lib.CustomListAdapter;
import com.he.stockem.lib.CustomListItem;
import com.he.stockem.lib.DatabaseHelper;
import com.he.stockem.lib.UtilityFunctions;

public class AddProductFragment extends Fragment implements OnAsyncRequestComplete, SearchView.OnQueryTextListener{
	
	public AddProductFragment(){}
	String customerName,customerID;
	
	ProgressDialog prgDialog;
	DatabaseHelper controller;
	
	EditText ETRecentProdQuantity,ETRecentProdQuantityFree,ETRecentProdUnitPrice;	
	TextView TxtRecentProdInventory;
	TextView txtRecentCustomerName;
	Order order;
	UtilityFunctions util =new UtilityFunctions();
	private List<CustomListItem> customList = new ArrayList<CustomListItem>();
	private ListView listView;
	private CustomListAdapter adapter;
	private SearchView mSearchView;
	CustomListItem selectedItem;
	public Spinner spinAddProdFragCategory;
	SharedPreferences sharedPrefs;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_add_product, container, false);
        mSearchView = (SearchView)rootView.findViewById(R.id.seachAddProdFrag);
        controller = DatabaseHelper.getInstance(getActivity());
        order = controller.getOrder(getArguments().getString("localOrderID"));
        sharedPrefs = PreferenceManager
	            .getDefaultSharedPreferences(this.getActivity());
        
        if(order.getID().isEmpty()){        
        	getActivity().onBackPressed();
        	return rootView;
        }
        
        spinAddProdFragCategory = (Spinner)rootView.findViewById(R.id.spinAddProdFragCategory);
        
        
	    
	    if(sharedPrefs.getString("prefAddProductCategory", "supplier").toLowerCase().contentEquals("supplier")){
	    	ArrayAdapter<String> categoryAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, controller.getAllProductSupplier());    			    
		    categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); 
		    spinAddProdFragCategory.setAdapter(categoryAdapter);	
	    
	    spinAddProdFragCategory.setOnItemSelectedListener(new OnItemSelectedListener(){

			@Override
			public void onItemSelected(AdapterView<?> parent,
					View view, int position, long id) {
				// TODO Auto-generated method stub
				String item = parent.getItemAtPosition(position).toString();
				
				customList.clear();
				new getProductsTask(item,"supplier").execute();
								
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
	    	
	    });
	    
	    }else{
	    	ArrayAdapter<String> categoryAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, controller.getAllProductManufacturer());    			    
		    categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); 
		    spinAddProdFragCategory.setAdapter(categoryAdapter);		    
	    	spinAddProdFragCategory.setOnItemSelectedListener(new OnItemSelectedListener(){
	    		
				@Override
				public void onItemSelected(AdapterView<?> parent,
						View view, int position, long id) {
					// TODO Auto-generated method stub
					String item = parent.getItemAtPosition(position).toString();
					mSearchView.setQuery("", false);
					mSearchView.clearFocus();
					new getProductsTask(item,"manufacturer").execute();
									
				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {
					// TODO Auto-generated method stub
					
				}
		    	
		    });
	    	
	    	
	    }
        txtRecentCustomerName=(TextView)rootView.findViewById(R.id.txtAddProdFragCustomerName);
        
        listView = (ListView) rootView.findViewById(R.id.listAddProdFrag);
		adapter = new CustomListAdapter(getActivity(), customList);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				selectedItem = (CustomListItem)(parent.getItemAtPosition(position));
				if(controller.getConfigValue("online").contentEquals("true")){
					getPrice(order.getCustomerID(),selectedItem.getID());
				}else{
					showAddProductDialog(selectedItem, view.getContext(), "offline");
				}
				
				
			}
			
		});
		if(order==null){
			Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.unable_to_get_order), 10).show();			
		}else{
			txtRecentCustomerName.setText(order.getCustomer().getName());
			//getRecentProducts(order.getCustomerID());			
		}		
		
		listView.setTextFilterEnabled(false);
		setupSearchView();
		
        return rootView;
		
	}
	
	private void setupSearchView() {
		mSearchView.setIconifiedByDefault(false);
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setSubmitButtonEnabled(false);
        mSearchView.setQueryHint("Search Here");
    }
	
	public void getPrice(String customerID, String ProductID){	
		
		if(sharedPrefs.getString("prefWorkingMode", "online").toLowerCase().contentEquals("online")){
			
		
			ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("module", "invoices"));
			params.add(new BasicNameValuePair("view", "product_ajax"));
			params.add(new BasicNameValuePair("id", ProductID));
			params.add(new BasicNameValuePair("customer_id", customerID));		
			String url=getResources().getString(R.string.server);
			AsyncRequest getPosts = new AsyncRequest((Fragment)this, "GET", params,"getprice",getResources().getString(R.string.getting_price));
			getPosts.execute(url);
		}else{
			
			
			showAddProductDialog(selectedItem, this.getActivity(), controller.getCustomerProductPrice(ProductID,customerID));
		}
	}

	public void getRecentProducts(String customer_id) {
		// TODO Auto-generated method stub
		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("module", "api/orders"));
		params.add(new BasicNameValuePair("view", "recentproducts"));
		params.add(new BasicNameValuePair("customer_id", customer_id));
		
		String url=getResources().getString(R.string.server);
		AsyncRequest getPosts = new AsyncRequest(this, "GET", params,"getrecentproducts","Checking recent products");
		getPosts.execute(url);
		
	}

	@Override
	public void asyncResponse(String response, String label) {
		// TODO Auto-generated method stub
		
		
		if(label.contentEquals("getprice")){						
			showAddProductDialog(selectedItem, this.getActivity(), response);
			return;
		}
		
		JSONObject response_obj;
				
		try {
			response_obj = new JSONObject(response);
			JSONArray productsJSON=response_obj.getJSONArray("products");
			
			if(productsJSON.length()==0){
				Toast.makeText(getActivity(), "No recent products for this customer", 10).show();
				return;
			}
			customList.clear();
			for(int i=0; i<productsJSON.length();i++){
				JSONObject obj = (JSONObject)productsJSON.get(i);				
				CustomListItem listItem = new CustomListItem();
				listItem.setTitle(obj.getString("description"));
				listItem.setFooterLeft(obj.getString("custom_field1")+", MRP"+obj.getString("custom_field2"));
				listItem.setFooterRight("");
				listItem.setID(obj.getString("id"));
				listItem.setProp1(obj.getString("free_offer_qty"));
				listItem.setProp2(obj.getString("free_qty"));
				String unit_price = obj.getString("unit_price");
				if(unit_price.contentEquals("null")||unit_price.trim().contentEquals("")){
					unit_price =obj.getString("default_unit_price");
				}		
				if(unit_price.trim().contentEquals("")||unit_price.isEmpty()){
					unit_price="0";
				}
				String tax_percentage = controller.getTaxPercentage(obj.getString("default_tax_id"));
				double dbl_cost= Double.parseDouble(unit_price);
				double dbl_tax_percentage= Double.parseDouble(tax_percentage);
				double dbl_unit_price = dbl_cost + (dbl_cost*(dbl_tax_percentage/100));
				unit_price = String.valueOf(util.roundTwoDecimals(dbl_unit_price));
				listItem.setDescription(unit_price);
				int added_quantity= order.getOrderItemTotalQuantity(listItem.getID());
				if(added_quantity != 0){
					listItem.setFooterRight("Added(" +String.valueOf(added_quantity)+")");
				}
				listItem.setHideDescription(true);
				customList.add(listItem);				
			}
			adapter.notifyDataSetChanged();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			Log.e("AddProductFragment","asyncResponse",e);
			e.printStackTrace();
		}
	}

	public boolean onQueryTextChange(String newText) {
		
		Filter filter = adapter.getFilter();
        if (TextUtils.isEmpty(newText)) {
            filter.filter("");
        } else {
            filter.filter(newText);
        }
        
        
        return true;
    }

    public boolean onQueryTextSubmit(String query) {
        return false;
    }
    
    public void showAddProductDialog(final CustomListItem selectedItem, Context context, String response){    	
    	
    	JSONObject obj=null;
    	Product selectedProduct = controller.getProductById(selectedItem.getID());
    	
    	if(controller.getConfigValue("online").contentEquals("true")){
    		
    	}else{    		
    		String selectedProductTax="0";
    		try{
    			selectedProductTax = controller.getTaxPercentage(selectedItem.getID());
    		}catch(Exception e){
    			Log.e("AddProductFragment","ShowProductDialog",e);
    		}
    		String unit_price=String.valueOf(selectedProduct.getUnit_price());
    		
    		CustomerProduct cp = controller.getCustomerProductPriceObj(selectedProduct.getId(), order.getCustomerID());
    		
    		if(cp != null){
    			unit_price=cp.getUnit_price();
    		}
    		response = "{\"unit_price\":\""+unit_price+"\",\"quantity\":\"0\",\"free\":\"0\",\"default_tax_id\":\""+selectedProduct.getDefault_tax_id()+"\",\"default_tax_id_2\":null,\"default_tax_percentage\":\""+selectedProductTax+"\",\"avlquantity\":\"0.000000\"}";    		
    	}
    	
    	if(response.isEmpty() || response == null || response.contentEquals("network not available")){
    		Toast.makeText(context, "Empty response", 10).show();    		
    	}else{
    		try {
    			obj  = new JSONObject(response);
    		} catch (JSONException e) {
    			// TODO Auto-generated catch block
    			Toast.makeText(context, "Unable to parse the response", 10).show();
    			Log.e("AddProductFragment","ShowProductDialog",e);
    			return;
    		}
    	}
    	
    	try {
			selectedItem.setProp1(obj.getString("quantity"));
			selectedItem.setProp2(obj.getString("free"));
			
			String unit_price = obj.getString("unit_price");
			if(unit_price.contentEquals("null")||unit_price.trim().contentEquals("")){
				unit_price ="0";
			}		
			if(unit_price.trim().contentEquals("")||unit_price.isEmpty()){
				unit_price="0";
			}
			String tax_percentage = controller.getTaxPercentage(obj.getString("default_tax_id"));
			double dbl_cost= Double.parseDouble(unit_price);
			double dbl_tax_percentage= Double.parseDouble(tax_percentage);
			double dbl_unit_price = dbl_cost + (dbl_cost*(dbl_tax_percentage/100));
			unit_price = String.valueOf(util.roundTwoDecimals(dbl_unit_price));
			selectedItem.setDescription(unit_price);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
			Log.e("AddProductFragment","ShowProductDialog",e);
			selectedItem.setProp1("1");
			selectedItem.setProp2("0");
			
			String unit_price = "-0.01";
			
			String tax_percentage = "0";
			double dbl_cost= Double.parseDouble(unit_price);
			double dbl_tax_percentage= Double.parseDouble(tax_percentage);
			double dbl_unit_price = dbl_cost + (dbl_cost*(dbl_tax_percentage/100));
			unit_price = String.valueOf(util.roundTwoDecimals(dbl_unit_price));
			selectedItem.setDescription(unit_price);
			
		}
    	
    	
    	
		LayoutInflater layoutInflater = LayoutInflater.from(context);
		View promptView = layoutInflater.inflate(R.layout.add_recent_product_popup, null);
		TxtRecentProdInventory = (TextView)promptView.findViewById(R.id.TxtRecentProdInventory);
		ETRecentProdUnitPrice = (EditText)promptView.findViewById(R.id.ETRecentProdUnitPrice);
		ETRecentProdQuantity = (EditText)promptView.findViewById(R.id.ETRecentProdQuantity);
		ETRecentProdQuantityFree = (EditText)promptView.findViewById(R.id.ETRecentProdQuantityFree);		
		ETRecentProdUnitPrice.setText(selectedItem.getDescription());
		
		final String quantity= selectedItem.getProp1();
		final String free= selectedItem.getProp2();
						
		ETRecentProdQuantity.addTextChangedListener(new TextWatcher(){					   
			   @Override
			   public void afterTextChanged(Editable s) {
				   String changedText=s.toString();
				   if(changedText.isEmpty() || changedText.contentEquals("0") || quantity.contentEquals("0")||free.contentEquals("0") || quantity.isEmpty()||free.isEmpty()){
					   return;
				   }else{
					   int int_quantity= Integer.parseInt(quantity);
					   int int_free= Integer.parseInt(free);
					   int int_text = Integer.parseInt(changedText);
					   int_text = int_text - (int_text%int_quantity);
					   ETRecentProdQuantityFree.setText(String.valueOf((int)(int_text/int_quantity)*int_free)); 
				   }
			   }
			 		
			   @Override    
			   public void beforeTextChanged(CharSequence s, int start,
			     int count, int after) {
			   }

			   @Override    
			   public void onTextChanged(CharSequence s, int start,
			     int before, int count) {
			      
			   }
			
		});
		String inventoryText="";
		if(controller.getConfigValue("online").contentEquals("true")){
			try {
				inventoryText="Order "+String.valueOf(selectedProduct.getOnlineOrderQuantity(obj.getInt("order_quantity")));
				inventoryText =inventoryText+ ",\nAvailable "+obj.getString("avlquantity");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				inventoryText="Order "+String.valueOf(selectedProduct.getOfflineOrderQuantity());
				inventoryText =inventoryText+ ",\nAvailable "+selectedProduct.getAvailableQty();
				e.printStackTrace();
			}
			
		}else{
			inventoryText="Order "+String.valueOf(selectedProduct.getOfflineOrderQuantity());
			inventoryText =inventoryText+ ",\nAvailable "+selectedProduct.getAvailableQty();
		}
		TxtRecentProdInventory.setText(inventoryText);
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);
		alertDialogBuilder.setView(promptView);
		alertDialogBuilder.setTitle(selectedItem.getTitle());
		alertDialogBuilder
		.setMessage("Landing Price : "+selectedItem.getDescription()+". ")
		.setCancelable(false)
		.setPositiveButton(context.getResources().getString(R.string.save),new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				if(ETRecentProdQuantity.getText().toString().trim().contentEquals("")||ETRecentProdQuantity.getText().toString().trim().contentEquals("0")){
					Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.quantity_required), 10).show();
					return;
				}						
				String orderID=getArguments().getString("localOrderID");
				
				Product p=controller.getProductById(String.valueOf(selectedItem.getID()));
				
				String tax_id=String.valueOf(p.getDefault_tax_id());
				String price = ETRecentProdUnitPrice.getText().toString();
				selectedItem.setDescription(price);				
				
				
				String quantity = ETRecentProdQuantity.getText().toString();
				String freeQty = ETRecentProdQuantityFree.getText().toString().trim();
				String tax_percentage = controller.getTaxPercentage(tax_id);
				
				controller.insertProduct(orderID, quantity, selectedItem.getID(), price, tax_percentage, freeQty, tax_id);
									
				selectedItem.setFooterRight("Added("+order.getOrderItemTotalQuantity(selectedItem.getID())+")");
				adapter.notifyDataSetChanged();
				dialog.cancel();
			}
		  })
		.setNegativeButton(getActivity().getResources().getString(R.string.cancel),new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {	
				dialog.cancel();								
			}
		});
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
    }
    
    class getProductsTask extends AsyncTask<String, Integer, String>  {
    	String item="";
    	String category="";
    	ProgressDialog pDialog;
        public getProductsTask(String item, String category) {
			// TODO Auto-generated constructor stub
        	this.item=item;
        	this.category=category;
		}
        public void onPreExecute() {
    		
    		pDialog = new ProgressDialog(AddProductFragment.this.getActivity());
    		pDialog.setMessage("Please wait"); // typically you will define such
    		// strings in a remote file.
    		pDialog.show();		
    	}
        
		public String doInBackground(String... params) {
			// TODO Auto-generated method stub
			customList.clear();
			List<Product> products=new ArrayList<Product>();
			if(category.contentEquals("manufacturer")){
				products= controller.getAllProductsByManufacturer(item, getArguments().getString("localOrderID"));
			}else{
				products= controller.getAllProductsBySupplier(item,getArguments().getString("localOrderID"));				
			}
			
			for(Product product : products){								
				CustomListItem listItem = new CustomListItem();
				listItem.setTitle(product.getDisplayName());
				listItem.setID(product.getId());
				int added_quantity= product.getCurrent_order_qty();
				if(added_quantity != 0){
					listItem.setFooterRight("Added(" +String.valueOf(added_quantity)+")");
				}
				listItem.setFooterLeft("Available "+ product.getAvailableQty()+", Order " + String.valueOf((product.getOrderQty()+product.getUnsync_order_qty())));
				listItem.setHideDescription(true);
				customList.add(listItem);	
			}
			

			return "";
		}
		
		public void onPostExecute(String response) {			
			adapter.setOriginalData(customList);
			adapter.notifyDataSetChanged();
			pDialog.dismiss();
		}
    }
    
}

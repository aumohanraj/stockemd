package com.he.stockem.orders;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.Toast;

import com.he.stockem.R;
import com.he.stockem.db.model.AreaLine;
import com.he.stockem.db.model.Customer;
import com.he.stockem.db.model.Order;
import com.he.stockem.lib.AutoCompleteItem;
import com.he.stockem.lib.AutoCompleteView;
import com.he.stockem.lib.AutocompleteArrayAdapter;
import com.he.stockem.lib.CustomListAdapter;
import com.he.stockem.lib.CustomListItem;
import com.he.stockem.lib.DatabaseHelper;
import com.he.stockem.lib.UtilityFunctions;

public class OrdersFragment extends Fragment {
	
	public OrdersFragment(){}
	
	TableLayout tblOrders;
	Button btnOrdersNew;	
	DatabaseHelper controller;;
	ArrayAdapter<AutoCompleteItem> customerAdapter;	
	int selectedCustomerID=0;
	String selectedCustomerName="";	
	UtilityFunctions util;
	private List<CustomListItem> customList = new ArrayList<CustomListItem>();	
	private ListView listView;
	private CustomListAdapter adapter;
	ArrayList<AutoCompleteItem> customerArray;
	List<String> sIds = new ArrayList<String>();
	List<String> names = new ArrayList<String>();	
	SharedPreferences sharedPrefs;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_orders, container, false);
        
        listView = (ListView) rootView.findViewById(R.id.listOrders);
		adapter = new CustomListAdapter(getActivity(), customList);
				
		listView.setAdapter(adapter);
		sharedPrefs = PreferenceManager
	            .getDefaultSharedPreferences(this.getActivity());
		listView.setOnItemLongClickListener(new OnItemLongClickListener(){

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				final CustomListItem selectedItem = (CustomListItem)(parent.getItemAtPosition(position));
				
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(view.getContext());
				alertDialogBuilder.setTitle("Confirm Delete");
				alertDialogBuilder.setMessage("Do you want to delete the order of " + selectedItem.getTitle());
				alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {						
						controller.deleteOrder(selectedItem.getID());
						Toast.makeText(getActivity(), "Order Deleted", 10).show();
						BuildOrderTable();
					}
				});
				
				alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {						
						dialog.dismiss();
					}
				});
				
				alertDialogBuilder.show();
				return true;
				
			}		
			
		});
				
				
		listView.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				CustomListItem selectedItem = (CustomListItem)(parent.getItemAtPosition(position));
				
				Bundle args = new Bundle();
				
				args.putString("localOrderID", String.valueOf(selectedItem.getID()));
				UpdateOrderFragment nextFrag= new UpdateOrderFragment();
				nextFrag.setArguments(args);
			     getActivity().getFragmentManager().beginTransaction()
			     .replace(R.id.frame_container, nextFrag)											     
			     .addToBackStack("Update Order")
			     .commit();								
			}		
			
		});
		
        util= new UtilityFunctions(getActivity());	
        controller = DatabaseHelper.getInstance(getActivity());
        
        tblOrders = (TableLayout)rootView.findViewById(R.id.tblOrders);
        
        //Build Local Order table
        BuildOrderTable();
    	
		//Create the customer array and adapter
        customerArray = new  ArrayList<AutoCompleteItem>();
        customerArray  = (ArrayList<AutoCompleteItem>) controller.getAllCustomersItems();        
        customerAdapter = new AutocompleteArrayAdapter(getActivity(), R.layout.list_view_row_item, customerArray);
        
        //Creating new order
        btnOrdersNew=(Button)rootView.findViewById(R.id.btnOrdersNew);
        
        if(controller.getConfigValue("cusautocom").toString().contentEquals("Yes")){
        	btnOrdersNew.setOnClickListener(new View.OnClickListener() {			
    			@Override
    			public void onClick(View v) {
    				// TODO Auto-generated method stub
    				LayoutInflater layoutInflater = LayoutInflater.from(v.getContext());
    				View promptView = layoutInflater.inflate(R.layout.select_customer_popup, null);
    				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(v.getContext());
    				alertDialogBuilder.setView(promptView);						
    				
    				final AutoCompleteView ACSelectCustomer = (AutoCompleteView) promptView.findViewById(R.id.ACSelectCustomer);
    				ACSelectCustomer.setAdapter(customerAdapter);
    				ACSelectCustomer.setOnItemClickListener(new OnItemClickListener() {
    				    @Override
    				    public void onItemClick(AdapterView<?> parent, View arg1, int pos, long id) {
    				    	AutoCompleteItem selectedItem = (AutoCompleteItem)(parent.getItemAtPosition(pos));
    				    	ACSelectCustomer.setText(selectedItem.getName());
    				    	selectedCustomerID=selectedItem.getID();
    				    	selectedCustomerName=selectedItem.getName();
    				    	
    				    }
    				});
    				
    				alertDialogBuilder
    						.setCancelable(false)
    						.setPositiveButton(getResources().getString(R.string.create_order), new DialogInterface.OnClickListener() {
    									public void onClick(DialogInterface dialog, int id) {						
    										
    										if(controller.checkIsCustomerValid(selectedCustomerID, ACSelectCustomer.getText().toString())){
    											Order newOrder = new Order(getActivity());
    											newOrder.setCustomerID(String.valueOf(selectedCustomerID));
    											
    											String salesman = controller.getConfigValue("salesman");
    											if(salesman.contentEquals("")){
    												salesman="Offline user";
    											}	
    											
    											newOrder.setSalesman(salesman);
    											newOrder.setTypeID(Order.KEY_TYPE_ORDER);
    											int localOrderID=Integer.parseInt(newOrder.save());
    											if(localOrderID>0){
    												Bundle args = new Bundle();
    												args.putString("localOrderID", String.valueOf(localOrderID));												 
    												Toast.makeText(getActivity(), getResources().getString(R.string.order_created), 10).show();
    												UpdateOrderFragment nextFrag= new UpdateOrderFragment();
    												nextFrag.setArguments(args);
    											     getActivity().getFragmentManager().beginTransaction()
    											     .replace(R.id.frame_container, nextFrag)											     
    											     .addToBackStack("Update Order")
    											     .commit();
    											}else{
    												Toast.makeText(getActivity(), getResources().getString(R.string.unable_to_create_order), 10).show();												
    											}
    										}else{
    											Toast.makeText(getActivity(), getResources().getString(R.string.please_select_customer), 10).show();
    										}
    									}
    								})
    						.setNegativeButton(getResources().getString(R.string.cancel),
    								new DialogInterface.OnClickListener() {
    									public void onClick(DialogInterface dialog,	int id) {
    										dialog.cancel();
    									}
    								});

    				// create an alert dialog
    				AlertDialog alertD = alertDialogBuilder.create();

    				alertD.show();	
    			}
    		});
        	
        }else{
        	btnOrdersNew.setOnClickListener(new View.OnClickListener() {			
    			@Override
    			public void onClick(View v) {
    				// TODO Auto-generated method stub
    				LayoutInflater layoutInflater = LayoutInflater.from(v.getContext());
    				View promptView = layoutInflater.inflate(R.layout.select_customer_spinner_popup, null);
    				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(v.getContext());
    				alertDialogBuilder.setView(promptView);
    				
    				final AutoCompleteView ACSelectCustomer = (AutoCompleteView) promptView.findViewById(R.id.ACSelectCustomerSpin);
    				ACSelectCustomer.setAdapter(customerAdapter);
    				ACSelectCustomer.setOnItemClickListener(new OnItemClickListener() {
    				    @Override
    				    public void onItemClick(AdapterView<?> parent, View arg1, int pos, long id) {
    				    	AutoCompleteItem selectedItem = (AutoCompleteItem)(parent.getItemAtPosition(pos));
    				    	ACSelectCustomer.setText(selectedItem.getName());
    				    	selectedCustomerID=selectedItem.getID();
    				    	selectedCustomerName=selectedItem.getName();	
    				    }
    				});
    				    				
    				final Spinner spinCustomerCategory = (Spinner)promptView.findViewById(R.id.spinnerCustomerCategory);
    				final Spinner spinCustomer = (Spinner)promptView.findViewById(R.id.spinnerCustomers);
    				
    				List<String> categories = new ArrayList<String>();    				    				
    				List<AreaLine> area_lines= controller.getAllAreaLines();
    				
    				categories.add("All");
    				categories.add("Nearby Stores");
    				for(AreaLine area_line : area_lines){		
    					categories.add(area_line.getName());
    				}
    			         			      
    			      // Creating adapter for spinner
    			    ArrayAdapter<String> categoryAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, categories);    			    
    			    categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); 
    			    spinCustomerCategory.setAdapter(categoryAdapter);
    			    
    			    if(!controller.getConfigValue("last_cust").isEmpty()){
    			    	spinCustomerCategory.setSelection(Integer.parseInt(controller.getConfigValue("last_cust")));
    			    }
    			    
    			    final ArrayAdapter<String> customerSpinnerAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, names);    			    
    			    customerSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    			    spinCustomer.setAdapter(customerSpinnerAdapter);
    			    spinCustomer.setOnItemSelectedListener(new OnItemSelectedListener(){
						@Override
						public void onItemSelected(AdapterView<?> parent,
								View view, int position, long id) {
							// TODO Auto-generated method stub	
							//ACSelectCustomer.setText(names.get(position));							
							selectedCustomerID= Integer.parseInt(sIds.get(position));
							selectedCustomerName= names.get(position);
						}

						@Override
						public void onNothingSelected(AdapterView<?> parent) {
							// TODO Auto-generated method stub
							
						}
    			    	
    			    });
    			        			        			    
    			    spinCustomerCategory.setOnItemSelectedListener(new OnItemSelectedListener(){
    			    	@Override
    			    	   public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
    			    	      // On selecting a spinner item
    			    	      String item = parent.getItemAtPosition(position).toString();
    			    	      controller.insertUpdateConfig("last_cust", String.valueOf(position));
    			    	      names.clear();
    			    	      sIds.clear();
    			    	      if(item.contentEquals("All")){    			    	    	  
    			    	    	  for(Customer customer : controller.getAllCustomersByAreaLine(0)){
    			    	    		  names.add(customer.getName());
    			    	    		  sIds.add(customer.getID());
    			    	    	  }
    			    	      }else if(item.contentEquals("Nearby Stores")){
    			    	    	  for(Customer customer : controller.getNearestCustomer(sharedPrefs.getString("prefBuffer", "500"))){
    			    	    		  names.add(customer.getName());
    			    	    		  sIds.add(customer.getID());
    			    	    	  }    			    	    	  
    			    	      }else{
    			    	    	  AreaLine area_line = new AreaLine(getActivity());
    			    	    	  area_line.setName(item);    			    	    	  
    			    	    	  for(Customer customer : area_line.getCustomers()){
    			    	    		  names.add(customer.getName());
    			    	    		  sIds.add(customer.getID());
    			    	    	  }
    			    	      }
    			    	      customerSpinnerAdapter.notifyDataSetChanged();
    			    	      
    			    	   }
    			    	   public void onNothingSelected(AdapterView<?> arg0) {
    			    	      // TODO Auto-generated method stub
    			    	   }
    			    	
    			    });
    				    				
    				alertDialogBuilder
    						.setCancelable(false)
    						.setPositiveButton(getResources().getString(R.string.create_order), new DialogInterface.OnClickListener() {
    									public void onClick(DialogInterface dialog, int id) {		
    										//selectedCustomerID= Integer.parseInt(sIds.get(spinCustomer.getSelectedItemPosition()));
    										if(selectedCustomerID==0  && spinCustomer.getSelectedItemPosition()==-1){
    											Toast.makeText(getActivity(), "Please select the customer", 10).show();
    											return;
    										}
    										if(selectedCustomerID==0){
    											selectedCustomerID = Integer.parseInt(sIds.get(spinCustomer.getSelectedItemPosition()));
    											selectedCustomerName = names.get(spinCustomer.getSelectedItemPosition());
    										}
    										controller.insertUpdateConfig("last_cust1", selectedCustomerName);
    										Order newOrder = new Order(getActivity());
											newOrder.setCustomerID(String.valueOf(selectedCustomerID));
											
											String salesman = controller.getConfigValue("salesman");
											if(salesman.contentEquals("")){
												salesman="Offline user";
											}	
											
											newOrder.setSalesman(salesman);
											newOrder.setTypeID(Order.KEY_TYPE_ORDER);
											int localOrderID=Integer.parseInt(newOrder.save());
											if(localOrderID>0){
												Bundle args = new Bundle();
												args.putString("localOrderID", String.valueOf(localOrderID));												 
												Toast.makeText(getActivity(), getResources().getString(R.string.order_created), 10).show();
												UpdateOrderFragment nextFrag= new UpdateOrderFragment();
												nextFrag.setArguments(args);
											     getActivity().getFragmentManager().beginTransaction()
											     .replace(R.id.frame_container, nextFrag)											     
											     .addToBackStack("Update Order")
											     .commit();
											}else{
												Toast.makeText(getActivity(), getResources().getString(R.string.unable_to_create_order), 10).show();												
											}
    									}
    								})
    						.setNegativeButton(getResources().getString(R.string.cancel),
    								new DialogInterface.OnClickListener() {
    									public void onClick(DialogInterface dialog,	int id) {
    										dialog.cancel();
    									}
    								});

    				// create an alert dialog
    				AlertDialog alertD = alertDialogBuilder.create();
    				alertD.show();	
    			}
    		});
        }
         
        return rootView;
    }
	
	public void BuildOrderTable() {
		List<Order> orders = controller.getOrders(Order.KEY_TYPE_ORDER);
		customList.clear();		
			//Updating the orders in the table
			if(orders.size()==0){
				Toast.makeText(getActivity(), this.getResources().getString(R.string.no_orders), 10).show();
				adapter.notifyDataSetChanged();				
				return;
			}
			
			for(Order order : orders){				
				CustomListItem listItem = new CustomListItem();
				listItem.setID(order.getID());
				listItem.setTitle(order.getCustomer().getName());
				if(order.getIndexID().contentEquals("")||order.getIndexID().contentEquals("0")){
					listItem.setDescription("Not Synced");									
				}else{
					listItem.setDescription("OrderID" + order.getIndexID());
				}				
				listItem.setFooterLeft("Total :" +String.valueOf(order.getOrderTotal()));
				listItem.setFooterRight(order.getDate());
				customList.add(listItem);	
				
			}	
			adapter.notifyDataSetChanged();
			
			
		}
}

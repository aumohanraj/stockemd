package com.he.stockem.orders;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.he.stockem.R;
import com.he.stockem.db.model.Order;
import com.he.stockem.db.model.Product;
import com.he.stockem.lib.AsyncRequest;
import com.he.stockem.lib.AsyncRequest.OnAsyncRequestComplete;
import com.he.stockem.lib.CustomListAdapter;
import com.he.stockem.lib.CustomListItem;
import com.he.stockem.lib.DatabaseHelper;
import com.he.stockem.lib.UtilityFunctions;

public class RecentProductsFragment extends Fragment implements OnAsyncRequestComplete, SearchView.OnQueryTextListener{
	
	public RecentProductsFragment(){}
	String customerName,customerID;
	
	ProgressDialog prgDialog;
	DatabaseHelper controller;
	
	EditText ETRecentProdQuantity,ETRecentProdQuantityFree,ETRecentProdUnitPrice;
	TextView TxtRecentProdInventory;
	TextView txtRecentCustomerName;
	Order order;
	UtilityFunctions util =new UtilityFunctions();
	private List<CustomListItem> customList = new ArrayList<CustomListItem>();
	private ListView listView;
	private CustomListAdapter adapter;
	private SearchView mSearchView;
	SharedPreferences sharedPrefs;
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_recent, container, false);
        sharedPrefs = PreferenceManager
	            .getDefaultSharedPreferences(this.getActivity());
        mSearchView = (SearchView)rootView.findViewById(R.id.search_view);
        controller = DatabaseHelper.getInstance(getActivity());
        order = controller.getOrder(getArguments().getString("localOrderID"));
        if(order.getID().isEmpty()){        
        	getActivity().onBackPressed();
        	return rootView;
        }
        txtRecentCustomerName=(TextView)rootView.findViewById(R.id.txtRecentCustomerName);
        
        listView = (ListView) rootView.findViewById(R.id.listRecentProducts);
		adapter = new CustomListAdapter(getActivity(), customList);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				final CustomListItem selectedItem = (CustomListItem)(parent.getItemAtPosition(position));
				LayoutInflater layoutInflater = LayoutInflater.from(view.getContext());
				View promptView = layoutInflater.inflate(R.layout.add_recent_product_popup, null);
				TxtRecentProdInventory = (TextView)promptView.findViewById(R.id.TxtRecentProdInventory);
				ETRecentProdQuantity = (EditText)promptView.findViewById(R.id.ETRecentProdQuantity);
				ETRecentProdQuantityFree = (EditText)promptView.findViewById(R.id.ETRecentProdQuantityFree);
				ETRecentProdUnitPrice= (EditText)promptView.findViewById(R.id.ETRecentProdUnitPrice);
				ETRecentProdUnitPrice.setText(selectedItem.getDescription());
				
				final String quantity= selectedItem.getProp1();
				final String free= selectedItem.getProp2();
								
				ETRecentProdQuantity.addTextChangedListener(new TextWatcher(){					   
					   @Override
					   public void afterTextChanged(Editable s) {
						   String changedText=s.toString();
						   if(changedText.isEmpty() || changedText.contentEquals("0") || quantity.contentEquals("0")||free.contentEquals("0") || quantity.isEmpty()||free.isEmpty()){
							   return;
						   }else{
							   int int_quantity= Integer.parseInt(quantity);
							   int int_free= Integer.parseInt(free);
							   int int_text = Integer.parseInt(changedText);
							   int_text = int_text - (int_text%int_quantity);
							   ETRecentProdQuantityFree.setText(String.valueOf((int)(int_text/int_quantity)*int_free)); 
						   }
					   }
					 		
					   @Override    
					   public void beforeTextChanged(CharSequence s, int start,
					     int count, int after) {
					   }

					   @Override    
					   public void onTextChanged(CharSequence s, int start,
					     int before, int count) {
					      
					   }
					
				});
				String inventoryText="";
				Product p =controller.getProductById(selectedItem.getID());
				inventoryText="Order "+String.valueOf((Integer.parseInt(selectedItem.getProp4()) + p.getUnSyncOrderQuantity()));
				inventoryText =inventoryText+ ",\nAvailable "+selectedItem.getProp3();
				TxtRecentProdInventory.setText(inventoryText);
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						view.getContext());
				alertDialogBuilder.setView(promptView);
				alertDialogBuilder.setTitle(selectedItem.getTitle());
				alertDialogBuilder
				.setMessage("Landing Price : "+selectedItem.getDescription()+". ")
				.setCancelable(false)
				.setPositiveButton(view.getContext().getResources().getString(R.string.save),new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						if(ETRecentProdQuantity.getText().toString().trim().contentEquals("")||ETRecentProdQuantity.getText().toString().trim().contentEquals("0")){
							Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.quantity_required), 10).show();
							return;
						}						
						
						String orderID=getArguments().getString("localOrderID");
						Product p=controller.getProductById(String.valueOf(selectedItem.getID()));
						
						String tax_id=String.valueOf(p.getDefault_tax_id());
						String price = ETRecentProdUnitPrice.getText().toString();
						selectedItem.setDescription(price);
						
						String quantity = ETRecentProdQuantity.getText().toString();
						String freeQty = ETRecentProdQuantityFree.getText().toString().trim();
						String tax_percentage = controller.getTaxPercentage(tax_id);
						
						
						
						controller.insertProduct(orderID, quantity, selectedItem.getID(), price, tax_percentage, freeQty, tax_id);
						
						selectedItem.setFooterRight("Added("+order.getOrderItemTotalQuantity(selectedItem.getID())+")");
						adapter.notifyDataSetChanged();
						dialog.cancel();
					}
				  })
				.setNegativeButton(getActivity().getResources().getString(R.string.cancel),new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {	
						dialog.cancel();								
					}
				});
				AlertDialog alertDialog = alertDialogBuilder.create();
				alertDialog.show();
				
			}
			
		});
		if(order==null){
			Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.unable_to_get_order), 10).show();			
		}else{
			txtRecentCustomerName.setText(order.getCustomer().getName());
			getRecentProducts(order.getCustomerID());			
		}		
		
		listView.setTextFilterEnabled(false);
		setupSearchView();
		
        return rootView;
		
	}
	
	private void setupSearchView() {
		mSearchView.setIconifiedByDefault(false);
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setSubmitButtonEnabled(false);
        mSearchView.setQueryHint("Search Here");
    }

	public void getRecentProducts(String customer_id) {
		// TODO Auto-generated method stub
		
		if(sharedPrefs.getString("prefWorkingMode", "online").toLowerCase().contentEquals("online")){
			
			
			ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("module", "api/orders"));
			params.add(new BasicNameValuePair("view", "recentproducts"));
			params.add(new BasicNameValuePair("customer_id", customer_id));
			
			String url=getResources().getString(R.string.server);
			AsyncRequest getPosts = new AsyncRequest(this, "GET", params,"getrecentproducts","Checking recent products");
			getPosts.execute(url);
		}else{
			asyncResponse(controller.getRecentProducts(customer_id),"getrecentproducts");
			System.out.println(controller.getRecentProducts(customer_id));
		}
		
		
	}

	@Override
	public void asyncResponse(String response, String label) {
		// TODO Auto-generated method stub
		
		JSONObject response_obj;
				
		try {
			response_obj = new JSONObject(response);
			JSONArray productsJSON=response_obj.getJSONArray("products");
			
			if(productsJSON.length()==0){
				Toast.makeText(getActivity(), "No recent products for this customer", 10).show();
				return;
			}
			customList.clear();
			for(int i=0; i<productsJSON.length();i++){
				JSONObject obj = (JSONObject)productsJSON.get(i);				
				CustomListItem listItem = new CustomListItem();
				listItem.setTitle(obj.getString("description")+","+obj.getString("custom_field1")+", MRP"+obj.getString("custom_field2"));
				
				listItem.setFooterRight("");
				listItem.setID(obj.getString("id"));
				listItem.setProp1(obj.getString("free_offer_qty"));
				listItem.setProp2(obj.getString("free_qty"));
				listItem.setProp3(obj.getString("avlquantity"));
				listItem.setProp4(obj.getString("order_quantity"));	
				listItem.setFooterLeft("Available "+ listItem.getProp3()+", Order "+ listItem.getProp4());
				String unit_price = obj.getString("unit_price");
				if(unit_price.contentEquals("null")||unit_price.trim().contentEquals("")){
					unit_price =obj.getString("default_unit_price");
				}		
				if(unit_price.trim().contentEquals("")||unit_price.isEmpty()){
					unit_price="0";
				}
				String tax_percentage = controller.getTaxPercentage(obj.getString("default_tax_id"));
				double dbl_cost= Double.parseDouble(unit_price);
				double dbl_tax_percentage= Double.parseDouble(tax_percentage);
				double dbl_unit_price = dbl_cost + (dbl_cost*(dbl_tax_percentage/100));
				unit_price = String.valueOf(util.roundTwoDecimals(dbl_unit_price));
				listItem.setDescription(unit_price);
				int added_quantity= order.getOrderItemTotalQuantity(listItem.getID());
				if(added_quantity != 0){
					listItem.setFooterRight("Added(" +String.valueOf(added_quantity)+")");
				}
				listItem.setHideDescription(true);
				customList.add(listItem);				
			}
			adapter.notifyDataSetChanged();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			Log.e("RecentProductsFragment","asyncResponse",e);
		}
	}

	public boolean onQueryTextChange(String newText) {
		
		Filter filter = adapter.getFilter();
        if (TextUtils.isEmpty(newText)) {
            filter.filter("");
        } else {
            filter.filter(newText);
        }
        
        
        return true;
    }

    public boolean onQueryTextSubmit(String query) {
        return false;
    }
}

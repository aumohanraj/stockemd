package com.he.stockem;


import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.he.stockem.db.model.Config;
import com.he.stockem.lib.AsyncRequest;
import com.he.stockem.lib.AsyncRequest.OnAsyncRequestComplete;
import com.he.stockem.lib.DatabaseHelper;

public class LoginActivity extends Activity  implements OnAsyncRequestComplete {
	
	Button btnLogin,btnOffline;
	EditText etUsername,etPassword;
	DatabaseHelper controller;
	Config configOnline;
	SharedPreferences sharedPrefs;
	private static final int RESULT_SETTINGS = 1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		 sharedPrefs = PreferenceManager
		            .getDefaultSharedPreferences(this);
		 
		controller = DatabaseHelper.getInstance(this);
		
		etUsername = (EditText)findViewById(R.id.etUsername);
		etPassword = (EditText)findViewById(R.id.etPassword);
		
		etUsername.setText(controller.getConfigValue("username"));		
		
		configOnline=controller.getConfig("online");
		if(configOnline.getConfigValue().contentEquals("true")){
		
			etPassword.setText(controller.getConfigValue("password"));
			ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("action", "login"));
			params.add(new BasicNameValuePair("user", etUsername.getText().toString()));
			params.add(new BasicNameValuePair("pass", etPassword.getText().toString()));
			
			String url=getResources().getString(R.string.server)+"?module=api&view=login";
			AsyncRequest getPosts = new AsyncRequest(this, "POST", params,"login",this.getResources().getString(R.string.verifying));
			
			if(sharedPrefs.getString("prefURL", "").contentEquals("")){
				Toast.makeText(this, "Please set the URL", 10).show();
			}else{
				getPosts.execute(url);
			}
		
		}
		
		btnOffline = (Button)findViewById(R.id.btnOffline);
		btnOffline.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// Logout to clear previous session history and open Main Activity
				controller.logout();
				Intent i = new Intent(LoginActivity.this, MainActivity.class);
				v.getContext().startActivity(i);
			}
		});
		
		
		btnLogin =(Button)findViewById(R.id.btnLogin);
		btnLogin.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// Verify the username and password
				
				if(sharedPrefs.getString("prefURL", "").contentEquals("")){
					Toast.makeText(v.getContext(), "Please set the URL", 10).show();
					return;
				}
				ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
				if( etUsername.getText().toString().trim().contentEquals("")|| etPassword.getText().toString().trim().contentEquals("")){
					Toast.makeText(v.getContext(), v.getContext().getResources().getString(R.string.username_password_empty), 10).show();
					return;
				}
				params.add(new BasicNameValuePair("action", "login"));
				params.add(new BasicNameValuePair("user", etUsername.getText().toString()));
				params.add(new BasicNameValuePair("pass", etPassword.getText().toString()));
				
				String url=getResources().getString(R.string.server)+"?module=api&view=login";
				AsyncRequest getPosts = new AsyncRequest((Activity) v.getContext(), "POST", params,"login",v.getContext().getResources().getString(R.string.verifying));
				getPosts.execute(url);				
			}
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.update_order, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		switch (item.getItemId()) {
		 
        case R.id.action_preference_settings:
            Intent i = new Intent(this, SettingActivity.class);
            startActivityForResult(i, RESULT_SETTINGS);
            break;
        case R.id.action_preference_clear_data:        	
        	this.deleteDatabase("stockem.db");        	
        	controller = new DatabaseHelper(this); 
        	Toast.makeText(this, "Data Cleared!!!", 10).show();
        	
        	//Restarting Application
        	Intent mStartActivity = new Intent(this, LoginActivity.class);
        	int mPendingIntentId = 123456;
        	PendingIntent mPendingIntent = PendingIntent.getActivity(this, mPendingIntentId,    mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
        	AlarmManager mgr = (AlarmManager)this.getSystemService(Context.ALARM_SERVICE);
        	mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
        	android.os.Process.killProcess(android.os.Process.myPid());
        	//System.exit(0);
            break;
 
        }
 
		return true;
	}

	@Override
	public void asyncResponse(String response, String label) {
		// TODO Auto-generated method stub
		
		if(label.contentEquals("sessioncheck")){
			if(response.contentEquals("1")){
				Intent i = new Intent(LoginActivity.this, MainActivity.class);
				LoginActivity.this.startActivity(i);
				finish();				
			}if(response.contains("Login Required")){
				controller.logout();
				return;
			}
		}
		
		if(label.contentEquals("login")){
			if(response.toLowerCase().contains("role_name")&&response.toLowerCase().contains("email")){
				try {
					JSONObject obj = new JSONObject(response);							
					controller.insertUpdateConfig("email", obj.getString("email"));
					controller.insertUpdateConfig("role_name", obj.getString("role_name"));
					controller.insertUpdateConfig("domain_id", obj.getString("domain_id"));
					controller.insertUpdateConfig("salesman", obj.getString("email"));					
					controller.insertUpdateConfig("online", "true");
					controller.insertUpdateConfig("username", etUsername.getText().toString());
					controller.insertUpdateConfig("password", etPassword.getText().toString());
				} catch (JSONException e) {
					// TODO Auto-generated catch block					
					Log.e("LoginActivity", "Login Response Exception", e);					
				}
				Toast.makeText(getApplicationContext(),getApplicationContext().getResources().getString(R.string.login_success) , 10).show();
				Intent i = new Intent(LoginActivity.this, MainActivity.class);
				LoginActivity.this.startActivity(i);
				finish();
			}else{
				Toast.makeText(getApplicationContext(),getApplicationContext().getResources().getString(R.string.invalid_credentials), 10).show();
			}			
		}		
	}
}

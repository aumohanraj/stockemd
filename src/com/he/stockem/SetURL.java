package com.he.stockem;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.he.stockem.lib.AsyncRequest;
import com.he.stockem.lib.AsyncRequest.OnAsyncRequestComplete;

public class SetURL extends Activity implements OnAsyncRequestComplete {
	Button btnSetURL;
	EditText etDistPhone;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_set_url);
		btnSetURL = (Button)findViewById(R.id.btnSetURL);
		etDistPhone =(EditText)findViewById(R.id.etDistPhone);
		
		btnSetURL.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String phone = etDistPhone.getText().toString();				
				if(phone.length()<10){
					Toast.makeText(v.getContext(), "Check the mobile number", 10).show();
					return;
				}
				
				ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair("module", "api"));
				params.add(new BasicNameValuePair("view", "getDistributorURL"));
				params.add(new BasicNameValuePair("mobile_phone", phone));
				String url="http://stockem.in/admin/index.php";
				AsyncRequest getPosts = new AsyncRequest((Activity) v.getContext(), "GET", params,"getdistributorurl","Please wait");
				getPosts.execute(url);
			}
		});
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void asyncResponse(String response, String label) {
		// TODO Auto-generated method stub
		
		SharedPreferences sharedPrefs;
		sharedPrefs = PreferenceManager
		                .getDefaultSharedPreferences(this);				
				SharedPreferences.Editor prefEditor = sharedPrefs.edit();
		        prefEditor.putString("prefURL", response);
		        prefEditor.putString("prefAddProduct", "List");    
		        prefEditor.putString("prefAddProductCategory", "Manufacturer");
		        prefEditor.putString("prefWorkingMode", "Offline");
		        prefEditor.putString("prefBuffer", "200");
		        prefEditor.putString("prefCustomerProductPrice", "Disabled");
		        prefEditor.commit();
		if(!sharedPrefs.getString("prefURL", "").isEmpty() && sharedPrefs.getString("prefURL", "").startsWith("http")){
			Intent i = new Intent(SetURL.this, LoginActivity.class);
            startActivity(i);
            finish();   
		}else{
			prefEditor.putString("prefURL", "");        
	        prefEditor.commit();
	        Toast.makeText(this, "Please check the mobile number", 10).show();
		}
	}
}

package com.he.stockem.lib;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Location;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import com.he.stockem.db.model.AreaLine;
import com.he.stockem.db.model.Config;
import com.he.stockem.db.model.Customer;
import com.he.stockem.db.model.CustomerProduct;
import com.he.stockem.db.model.InvoiceStatement;
import com.he.stockem.db.model.Order;
import com.he.stockem.db.model.OrderItem;
import com.he.stockem.db.model.OrderItemTax;
import com.he.stockem.db.model.Payment;
import com.he.stockem.db.model.Position;
import com.he.stockem.db.model.Product;
import com.he.stockem.db.model.Supplier;
import com.loopj.android.http.RequestParams;

public class DatabaseHelper  extends SQLiteOpenHelper {	
	
	private static DatabaseHelper sInstance;
		
	//Table Names
	public static final String TABLE_CONFIG="si_config";
	public static final String TABLE_ORDER="si_orders";
	public static final String TABLE_ORDER_ITEM="si_order_items";
	public static final String TABLE_ORDER_ITEM_TAX="si_order_item_tax";
	public static final String TABLE_CUSTOMER="si_customers";
	public static final String TABLE_PRODUCT="si_products";
	public static final String TABLE_TAX="si_tax";
	public static final String TABLE_AREA_LINE="si_area_line";
	public static final String TABLE_POSITION="si_position";
	public static final String TABLE_SUPPLIER="si_supplier";
	public static final String TABLE_PAYMENT_MOBILE="si_payment_mobile";
		
	//Config Table Columns
	public static final String KEY_CONFIG_CONFIGNAME = "config_name";
	public static final String KEY_CONFIG_CONFIGVALUE = "config_value";
		
	//Order Table Columns
	public static final String KEY_ORDER_ID="id";
	public static final String KEY_ORDER_INDEX_ID="index_id";
	public static final String KEY_ORDER_DOMAIN_ID="domain_id";
	public static final String KEY_ORDER_BILLER_ID="biller_id";
	public static final String KEY_ORDER_CUSTOMER_ID="customer_id";
	public static final String KEY_ORDER_TYPE_ID="type_id";
	public static final String KEY_ORDER_PREFERENCE_ID="preference_id";
	public static final String KEY_ORDER_DATE="date";
	public static final String KEY_ORDER_CUSTOM_FIELD1="custom_field1";
	public static final String KEY_ORDER_CUSTOM_FIELD2="custom_field2";
	public static final String KEY_ORDER_CUSTOM_FIELD3="custom_field3";
	public static final String KEY_ORDER_CUSTOM_FIELD4="custom_field4";
	public static final String KEY_ORDER_NOTE="note";
	public static final String KEY_ORDER_SALESMAN="salesman";
	public static final String KEY_ORDER_STATUS="status";
	public static final String KEY_ORDER_INVOICE_ID="invoice_id";	
	
	//Order Item table columns
	public static final String KEY_ORDERITEM_ID="id";
	public static final String KEY_ORDERITEM_ORDER_ID="order_id";
	public static final String KEY_ORDERITEM_QUANTITY="quantity";
	public static final String KEY_ORDERITEM_PRODUCT_ID="product_id";
	public static final String KEY_ORDERITEM_UNIT_PRICE="unit_price";
	public static final String KEY_ORDERITEM_TAX_AMOUNT="tax_amount";
	public static final String KEY_ORDERITEM_GROSS_TOTAL="gross_total";
	public static final String KEY_ORDERITEM_DESCRIPTION="description";
	public static final String KEY_ORDERITEM_TOTAL="total";
	public static final String KEY_ORDERITEM_FREE_ID="free_id";
	public static final String KEY_ORDERITEM_FREE_QTY="free_qty";
	public static final String KEY_ORDERITEM_BILLED="billed";
	
	// Order Item Tax table columns
	public static final String KEY_ORDERITEM_TAX_ID = "id";
	public static final String KEY_ORDERITEM_TAX_ORDER_ITEM_ID = "order_item_id";
	public static final String KEY_ORDERITEM_TAX_TAX_ID = "tax_id";
	public static final String KEY_ORDERITEM_TAX_TAX_TYPE = "tax_type";
	public static final String KEY_ORDERITEM_TAX_TAX_RATE = "tax_rate";
	public static final String KEY_ORDERITEM_TAX_TAX_AMOUNT = "tax_amount";
	
	//Customer table columns
	public static final String KEY_CUSTOMER_ID="id";
	public static final String KEY_CUSTOMER_DOMAIN_ID="domain_id";
	public static final String KEY_CUSTOMER_ATTENTION="attention";
	public static final String KEY_CUSTOMER_NAME="name";
	public static final String KEY_CUSTOMER_STREET_ADDRESS="street_address";
	public static final String KEY_CUSTOMER_STREET_ADDRESS2="street_address2";
	public static final String KEY_CUSTOMER_CITY="city";
	public static final String KEY_CUSTOMER_STATE="state";
	public static final String KEY_CUSTOMER_ZIP_CODE="zip_code";
	public static final String KEY_CUSTOMER_COUNTRY="country";
	public static final String KEY_CUSTOMER_PHONE="phone";
	public static final String KEY_CUSTOMER_MOBILE_PHONE="mobile_phone";
	public static final String KEY_CUSTOMER_FAX="fax";
	public static final String KEY_CUSTOMER_EMAIL="email";
	public static final String KEY_CUSTOMER_CREDIT_CARD_HOLDER_NAME="credit_card_holder_name";
	public static final String KEY_CUSTOMER_CREDIT_CARD_NUMBER="credit_card_number";
	public static final String KEY_CUSTOMER_CREDIT_CARD_EXPIRY_MONTH="credit_card_expiry_month";
	public static final String KEY_CUSTOMER_CREDIT_CARD_EXPIRY_YEAR="credit_card_expiry_year";
	public static final String KEY_CUSTOMER_NOTES="notes";
	public static final String KEY_CUSTOMER_CUSTOM_FIELD1="custom_field1";
	public static final String KEY_CUSTOMER_CUSTOM_FIELD2="custom_field2";
	public static final String KEY_CUSTOMER_CUSTOM_FIELD3="custom_field3";
	public static final String KEY_CUSTOMER_CUSTOM_FIELD4="custom_field4";
	public static final String KEY_CUSTOMER_AREA_LINE="area_line";
	public static final String KEY_CUSTOMER_LATITUDE="latitude";
	public static final String KEY_CUSTOMER_LONGITUDE="longitude";
	
	public static final String KEY_CUSTOMER_ENABLED="enabled";
	
	//Product table columns
	public static final String KEY_ID="ID";
	public static final String KEY_DOMAIN_ID="DOMAIN_ID";
	public static final String KEY_DESCRIPTION="DESCRIPTION";
	public static final String KEY_UNIT_PRICE="UNIT_PRICE";
	public static final String KEY_DEFAULT_TAX_ID="DEFAULT_TAX_ID";
	public static final String KEY_DEFAULT_TAX_ID_2="DEFAULT_TAX_ID_2";
	public static final String KEY_COST="COST";
	public static final String KEY_REORDER_LEVEL="REORDER_LEVEL";
	public static final String KEY_CUSTOM_FIELD1="CUSTOM_FIELD1";
	public static final String KEY_CUSTOM_FIELD2="CUSTOM_FIELD2";
	public static final String KEY_CUSTOM_FIELD3="CUSTOM_FIELD3";
	public static final String KEY_CUSTOM_FIELD4="CUSTOM_FIELD4";
	public static final String KEY_NOTES="NOTES";
	public static final String KEY_ENABLED="ENABLED";
	public static final String KEY_VISIBLE="VISIBLE";
	public static final String KEY_SUPPLIER="SUPPLIER";
	
	//Tax table columns
	public static final String KEY_TAX_TAX_ID="tax_id";
	public static final String KEY_TAX_TAX_DESCRIPTION="tax_description";
	public static final String KEY_TAX_TAX_PERCENTAGE="tax_percentage";
	public static final String KEY_TAX_TYPE="type";
	public static final String KEY_TAX_TAX_ENABLED="tax_enabled";
	public static final String KEY_TAX_DOMAIN_ID="domain_id";
	
	//Area Lines
	public static final String KEY_AREA_LINE_ID="id";	
	public static final String KEY_AREA_LINE_DOMAIN_ID="domain_id";
	public static final String KEY_AREA_LINE_NAME="name";
	
	//Location table key columns
	public static final String KEY_LOCATION_ID="id";
	public static final String KEY_LOCATION_DEVICEID="deviceId";
	public static final String KEY_LOCATION_DEVICETIME="deviceTime";
	public static final String KEY_LOCATION_FIXTIME="fixTime";
	public static final String KEY_LOCATION_VALID="valid";
	public static final String KEY_LOCATION_LATITUDE="latitude";
	public static final String KEY_LOCATION_LONGITUDE="longitude";
	public static final String KEY_LOCATION_ALTITUDE="altitude";
	public static final String KEY_LOCATION_SPEED="speed";
	public static final String KEY_LOCATION_COURSE="course";
	public static final String KEY_LOCATION_ATTRIBUTES="attributes";
	public static final String KEY_LOCATION_ACCURACY="accuracy";
	
	public static final String KEY_SUPPLIER_ID="id";
	public static final String KEY_SUPPLIER_DOMAIN_ID="domain_id";
	public static final String KEY_SUPPLIER_NAME="name";
	public static final String KEY_SUPPLIER_STREET_ADDRESS="street_address";
	public static final String KEY_SUPPLIER_STREET_ADDRESS2="street_address2";
	public static final String KEY_SUPPLIER_CITY="city";
	public static final String KEY_SUPPLIER_STATE="state";
	public static final String KEY_SUPPLIER_ZIP_CODE="zip_code";
	public static final String KEY_SUPPLIER_COUNTRY="country";
	public static final String KEY_SUPPLIER_PHONE="phone";
	public static final String KEY_SUPPLIER_MOBILE_PHONE="mobile_phone";
	public static final String KEY_SUPPLIER_FAX="fax";
	public static final String KEY_SUPPLIER_EMAIL="email";
	public static final String KEY_SUPPLIER_LOGO="logo";
	public static final String KEY_SUPPLIER_FOOTER="footer";
	public static final String KEY_SUPPLIER_PAYPAL_BUSINESS_NAME="paypal_business_name";
	public static final String KEY_SUPPLIER_PAYPAL_NOTIFY_URL="paypal_notify_url";
	public static final String KEY_SUPPLIER_PAYPAL_RETURN_URL="paypal_return_url";
	public static final String KEY_SUPPLIER_EWAY_CUSTOMER_ID="eway_customer_id";
	public static final String KEY_SUPPLIER_NOTES="notes";
	public static final String KEY_SUPPLIER_CUSTOM_FIELD1="custom_field1";
	public static final String KEY_SUPPLIER_CUSTOM_FIELD2="custom_field2";
	public static final String KEY_SUPPLIER_CUSTOM_FIELD3="custom_field3";
	public static final String KEY_SUPPLIER_CUSTOM_FIELD4="custom_field4";
	public static final String KEY_SUPPLIER_ENABLED="enabled";
	
	public static final String KEY_PAYMENTMOBILE_ID="id";
	public static final String KEY_PAYMENTMOBILE_AC_INV_ID="ac_inv_id";
	public static final String KEY_PAYMENTMOBILE_AC_AMOUNT="ac_amount";
	public static final String KEY_PAYMENTMOBILE_AC_NOTES="ac_notes";
	public static final String KEY_PAYMENTMOBILE_AC_DATE="ac_date";
	public static final String KEY_PAYMENTMOBILE_AC_PAYMENT_TYPE="ac_payment_type";
	public static final String KEY_PAYMENTMOBILE_DOMAIN_ID="domain_id";
	public static final String KEY_PAYMENTMOBILE_ONLINE_PAYMENT_ID="online_payment_id";
	public static final String KEY_PAYMENTMOBILE_CHEQUE_NO="cheque_no";
	public static final String KEY_PAYMENTMOBILE_CHEQUE_BANK="cheque_bank";
	public static final String KEY_PAYMENTMOBILE_CHEQUE_STATUS="cheque_status";
	public static final String KEY_PAYMENTMOBILE_STATUS="status";
	public static final String KEY_PAYMENTMOBILE_USER_ID="user_id";
	
	
	private static final String ALTER_PRODUCT_ADD_AVAILABLE_QTY = "ALTER TABLE SI_PRODUCTS ADD available_qty INT(11) DEFAULT 0";
	private static final String ALTER_PRODUCT_ADD_ORDER_QTY = "ALTER TABLE SI_PRODUCTS ADD order_qty INT(11) DEFAULT 0";
	
	public Context context;
	public SQLiteDatabase dbSQL;
	SharedPreferences sharedPrefs;
	private static final int DATABASE_VERSION = 2;
	public DatabaseHelper(Context context) {		
        super(context.getApplicationContext(), "stockem.db", null, DATABASE_VERSION);
        context = context.getApplicationContext();
        dbSQL = this.getWritableDatabase();
        sharedPrefs = PreferenceManager
	            .getDefaultSharedPreferences(context);
    }
	
	public static synchronized DatabaseHelper getInstance(Context context) {
        // Use the application context, which will ensure that you 
        // don't accidentally leak an Activity's context.
		
        if (sInstance == null) {
            sInstance = new DatabaseHelper(context.getApplicationContext());
        }
        return sInstance;
    }
	
	//Creates Table
	@Override
	public void onCreate(SQLiteDatabase database) {        
        CreateTables(database);        
	}
	@Override
	public void onUpgrade(SQLiteDatabase database, int version_old, int current_version) {
		if (version_old < 2) {
			database.execSQL(ALTER_PRODUCT_ADD_AVAILABLE_QTY);
			database.execSQL(ALTER_PRODUCT_ADD_ORDER_QTY);			
		}        
	}
	public Config getConfig(String configName){
		Config config=new Config();		
	    SQLiteDatabase database = dbSQL;	    
	    Cursor cursor = database.query(TABLE_CONFIG, null, KEY_CONFIG_CONFIGNAME+"=?", new String[] {configName}, null, null, null);
	    if(cursor.moveToFirst()){
	    	config.setID(cursor.getString(cursor.getColumnIndex("id")));
	    	config.setConfigName(cursor.getString(cursor.getColumnIndex(KEY_CONFIG_CONFIGNAME)));
	    	config.setConfigValue(cursor.getString(cursor.getColumnIndex(KEY_CONFIG_CONFIGVALUE)));
	    }
	    
	    closeCursor(cursor);
	    
	    
		return config;
	}
	
	public AreaLine getRoute(String routeName){
		AreaLine route = new AreaLine(context);	
	    SQLiteDatabase database = dbSQL;	    
	    Cursor cursor = database.query(TABLE_AREA_LINE, null, KEY_AREA_LINE_NAME+"=?", new String[] {routeName}, null, null, null);
	    if(cursor.moveToFirst()){
	    	route.setId(cursor.getString(cursor.getColumnIndex("id")));
	    	route.setDomain_id("1");
	    	route.setName(routeName);
	    }
	    closeCursor(cursor);   
	    
		return route;
	}
	
		
	public String getLastupdatedTime(){
		SQLiteDatabase database = dbSQL;
		Cursor cursor = database.query(TABLE_CONFIG, null,KEY_CONFIG_CONFIGNAME+"=?", new String[] {"last_updated"}, null, null, null);
		String lastupdatedtime="0000-00-00 00:00:00";
		if(cursor.moveToFirst()){
			lastupdatedtime = cursor.getString(cursor.getColumnIndex(KEY_CONFIG_CONFIGVALUE));									
		}else{
			ContentValues values = new ContentValues();			
			values.put(KEY_CONFIG_CONFIGNAME, "last_updated");
			values.put(KEY_CONFIG_CONFIGVALUE, "0000-00-00 00:00:00");
			database.insert(TABLE_CONFIG, null, values);												
		}
		
		closeCursor(cursor);
		return lastupdatedtime;
	}
		
	public void UpdatetLastupdatedTime(String timestamp){
		SQLiteDatabase database = dbSQL;
		Cursor cursor = database.query(TABLE_CONFIG, null, KEY_CONFIG_CONFIGNAME+"=?", new String[] {"last_updated"}, null, null, null);		
		if(cursor.moveToFirst()){
			ContentValues values = new ContentValues();			
			values.put(KEY_CONFIG_CONFIGVALUE, timestamp);	
			database.update(TABLE_CONFIG, values, KEY_CONFIG_CONFIGNAME+"=?", new String[] {"last_updated"});
		}else{
			ContentValues values = new ContentValues();		
			values.put(KEY_CONFIG_CONFIGNAME, "last_updated");
			values.put(KEY_CONFIG_CONFIGVALUE, "0000-00-00 00:00:00");
			database.insert(TABLE_CONFIG, null, values);												
		}
		closeCursor(cursor);		
	}
	
	public void insertUpdateConfig(String config_name, String config_value){
		SQLiteDatabase database = dbSQL;
		Cursor cursor = database.query(TABLE_CONFIG, null, KEY_CONFIG_CONFIGNAME+"=?", new String[] {config_name}, null, null, null);
				
		if(cursor.moveToFirst()){
			ContentValues values = new ContentValues();			
			values.put(KEY_CONFIG_CONFIGVALUE, config_value);	
			database.update(TABLE_CONFIG, values, KEY_CONFIG_CONFIGNAME+"=?", new String[] {config_name});
		}else{
			ContentValues values = new ContentValues();		
			values.put(KEY_CONFIG_CONFIGNAME, config_name);
			values.put(KEY_CONFIG_CONFIGVALUE, config_value);
			database.insert(TABLE_CONFIG, null, values);												
		}		
		closeCursor(cursor);
	}	
		
	public void CreateTables(SQLiteDatabase database){
		CreateBillersTable(database);
		CreateSuppliersTable(database);
		CreateCustomersTable(database);
		CreateProductsTable(database);        
        CreateTaxTable(database);
        CreateOrderTable(database);
        CreateOrderItemTable(database);
        CreateOrderItemTaxTable(database);
        CreateConfigTable(database);
        CreateAreaLineTable(database);
        CreateLocationTable(database);
        CreateLPaymentTable(database);
        CreateLInvoiceStatmentTable(database);
        CreateCustomerPriceTable(database);
	}
	
	public void CreateBillersTable(SQLiteDatabase database) {
		String query = "CREATE TABLE `si_biller` (" +
				"	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
				"	`domain_id`	int(11) NOT NULL DEFAULT '1'," +
				"	`name`	varchar(255) DEFAULT NULL," +
				"	`street_address`	varchar(255) DEFAULT NULL," +
				"	`street_address2`	varchar(255) DEFAULT NULL," +
				"	`city`	varchar(255) DEFAULT NULL," +
				"	`state`	varchar(255) DEFAULT NULL," +
				"	`zip_code`	varchar(255) DEFAULT NULL," +
				"	`country`	varchar(255) DEFAULT NULL," +
				"	`phone`	varchar(255) DEFAULT NULL," +
				"	`mobile_phone`	varchar(255) DEFAULT NULL," +
				"	`fax`	varchar(255) DEFAULT NULL," +
				"	`email`	varchar(255) DEFAULT NULL," +
				"	`logo`	varchar(255) DEFAULT NULL," +
				"	`footer`	text," +
				"	`paypal_business_name`	varchar(255) DEFAULT NULL," +
				"	`paypal_notify_url`	varchar(255) DEFAULT NULL," +
				"	`paypal_return_url`	varchar(255) DEFAULT NULL," +
				"	`eway_customer_id`	varchar(255) DEFAULT NULL," +
				"	`notes`	text," +
				"	`custom_field1`	varchar(255) DEFAULT NULL," +
				"	`custom_field2`	varchar(255) DEFAULT NULL," +
				"	`custom_field3`	varchar(255) DEFAULT NULL," +
				"	`custom_field4`	varchar(255) DEFAULT NULL," +
				"	`enabled`	varchar(1) NOT NULL DEFAULT '1'" +
				");";
        database.execSQL(query);
	}
	
	public void CreateSuppliersTable(SQLiteDatabase database) {
		String query = "CREATE TABLE IF NOT EXISTS `si_supplier` (" +
				"  `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
				"  `domain_id` int(11) NOT NULL DEFAULT '1'," +
				"  `name` varchar(255)  DEFAULT NULL," +
				"  `street_address` varchar(255)  DEFAULT NULL," +
				"  `street_address2` varchar(255)  DEFAULT NULL," +
				"  `city` varchar(255)  DEFAULT NULL," +
				"  `state` varchar(255)  DEFAULT NULL," +
				"  `zip_code` varchar(255)  DEFAULT NULL," +
				"  `country` varchar(255)  DEFAULT NULL," +
				"  `phone` varchar(255)  DEFAULT 	NULL," +
				"  `mobile_phone` varchar(255)  DEFAULT NULL," +
				"  `fax` varchar(255)  DEFAULT NULL," +
				"  `email` varchar(255)  DEFAULT NULL," +
				"  `logo` varchar(255)  DEFAULT NULL," +
				"  `footer` text ," +
				"  `paypal_business_name` varchar(255)  DEFAULT NULL," +
				"  `paypal_notify_url` varchar(255)  DEFAULT NULL," +
				"  `paypal_return_url` varchar(255)  DEFAULT NULL," +
				"  `eway_customer_id` varchar(255)  DEFAULT NULL," +
				"  `notes` text ," +
				"  `custom_field1` varchar(255)  DEFAULT NULL," +
				"  `custom_field2` varchar(255)  DEFAULT NULL," +
				"  `custom_field3` varchar(255)  DEFAULT NULL," +
				"  `custom_field4` varchar(255)  DEFAULT NULL," +
				"  `enabled` varchar(1)  NOT NULL DEFAULT '1'" +
				");";
        database.execSQL(query);
	}
	public void CreateProductsTable(SQLiteDatabase database) {
		String query = "CREATE TABLE `si_products` (" +
				"	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
				"	`domain_id`	int(11) NOT NULL DEFAULT '1'," +
				"	`description`	text NOT NULL," +
				"	`unit_price`	decimal(25,6) DEFAULT '0.000000'," +
				"	`default_tax_id`	int(11) DEFAULT NULL," +
				"	`default_tax_id_2`	int(11) DEFAULT NULL," +
				"	`cost`	decimal(25,6) DEFAULT '0.000000'," +
				"	`reorder_level`	int(11) DEFAULT NULL," +
				"	`custom_field1`	varchar(255) DEFAULT NULL," +
				"	`custom_field2`	varchar(255) DEFAULT NULL," +
				"	`custom_field3`	varchar(255) DEFAULT NULL," +
				"	`custom_field4`	varchar(255) DEFAULT NULL," +
				"	`article_no`	varchar(255) DEFAULT NULL," +
				"	`ean`	varchar(255) DEFAULT NULL," +
				"	`notes`	text NOT NULL," +
				"	`enabled`	varchar(1) NOT NULL DEFAULT '1'," +
				"	`visible`	varchar(1) NOT NULL DEFAULT '1'," +
				"	`supplier`	varchar(30) NOT NULL," +
				"	`available_qty`	int(11) DEFAULT 0," +
				"	`order_qty`	int(11) DEFAULT 0," +
				"	`manufacturer`	varchar(30) NOT NULL" +
				")";
        database.execSQL(query);
	}
	
	public void CreateCustomersTable(SQLiteDatabase database) {
		String query = "CREATE TABLE `si_customers` (" +
				"	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
				"	`domain_id`	int(11) NOT NULL DEFAULT '1'," +
				"	`attention`	varchar(255) DEFAULT NULL," +
				"	`name`	varchar(255) DEFAULT NULL," +
				"	`street_address`	varchar(255) DEFAULT NULL," +
				"	`street_address2`	varchar(255) DEFAULT NULL," +
				"	`city`	varchar(255) DEFAULT NULL," +
				"	`state`	varchar(255) DEFAULT NULL," +
				"	`zip_code`	varchar(255) DEFAULT NULL," +
				"	`country`	varchar(255) DEFAULT NULL," +
				"	`phone`	varchar(255) DEFAULT NULL," +
				"	`mobile_phone`	varchar(255) DEFAULT NULL," +
				"	`fax`	varchar(255) DEFAULT NULL," +
				"	`email`	varchar(255) DEFAULT NULL," +
				"	`credit_card_holder_name`	varchar(255) DEFAULT NULL," +
				"	`credit_card_number`	varchar(255) DEFAULT NULL," +
				"	`credit_card_expiry_month`	varchar(2) DEFAULT NULL," +
				"	`credit_card_expiry_year`	varchar(4) DEFAULT NULL," +
				"	`notes`	text," +
				"	`custom_field1`	varchar(255) DEFAULT NULL," +
				"	`custom_field2`	varchar(255) DEFAULT NULL," +
				"	`custom_field3`	varchar(255) DEFAULT NULL," +
				"	`custom_field4`	varchar(255) DEFAULT NULL," +
				"	`area_line`	int(11) DEFAULT NULL," +
				"  `latitude` double default 0," +
				"  `longitude` double default 0," +
				"	`enabled`	varchar(1) NOT NULL DEFAULT '1'" +
				")";
        database.execSQL(query);
	}
	
	public void CreateTaxTable(SQLiteDatabase database) {		
		String query = "CREATE TABLE `si_tax` (" +
				"	`tax_id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
				"	`tax_description`	varchar(50) DEFAULT NULL," +
				"	`tax_percentage`	decimal(25,6) DEFAULT '0.000000'," +
				"	`type`	varchar(1) DEFAULT NULL," +
				"	`tax_enabled`	varchar(1) NOT NULL DEFAULT '1'," +
				"	`domain_id`	int(11) NOT NULL" +
				");";
        database.execSQL(query);
	}
	
	public void CreateOrderTable(SQLiteDatabase database) {		
		String query = "CREATE TABLE `si_orders` (" +
				"	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
				"	`index_id`	int(11) NOT NULL," +
				"	`domain_id`	int(11) NOT NULL DEFAULT '1'," +
				"	`biller_id`	int(10) NOT NULL DEFAULT '0'," +
				"	`customer_id`	int(10) NOT NULL DEFAULT '0'," +
				"	`type_id`	int(10) NOT NULL DEFAULT '0'," +
				"	`preference_id`	int(10) NOT NULL DEFAULT '0'," +
				"	`date`	datetime NOT NULL DEFAULT '0000-00-00 00:00:00'," +
				"	`custom_field1`	varchar(50) DEFAULT NULL," +
				"	`custom_field2`	varchar(50) DEFAULT NULL," +
				"	`custom_field3`	varchar(50) DEFAULT NULL," +
				"	`custom_field4`	varchar(50) DEFAULT NULL," +
				"	`note`	text," +
				"	`salesman`	varchar(30) NOT NULL," +
				"	`status`	int(11) NOT NULL DEFAULT '1'," +
				"	`invoice_id`	int(11) NOT NULL" +
				");";
        database.execSQL(query);
	}
	
	public void CreateOrderItemTable(SQLiteDatabase database) {		
		String query = "CREATE TABLE `si_order_items` (" +
				"	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
				"	`order_id`	int(10) NOT NULL DEFAULT '0'," +
				"	`quantity`	decimal(25,6) NOT NULL DEFAULT '0.000000'," +
				"	`product_id`	int(10) DEFAULT '0'," +
				"	`unit_price`	decimal(25,6) DEFAULT '0.000000'," +
				"	`tax_amount`	decimal(25,6) DEFAULT '0.000000'," +
				"	`gross_total`	decimal(25,6) DEFAULT '0.000000'," +
				"	`description`	text," +
				"	`total`	decimal(25,6) DEFAULT '0.000000'," +
				"	`free_id`	int(10) DEFAULT '0'," +
				"	`free_qty`	int(10) DEFAULT '0'," +
				"	`isfree`	int(10) DEFAULT '0'," +
				"	`billed`	varchar(10) NOT NULL" +
				");";
        database.execSQL(query);
	}	
	
	
	public void CreateOrderItemTaxTable(SQLiteDatabase database) {		
		String query = "CREATE TABLE `si_order_item_tax` (" +
				"	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
				"	`order_item_id`	int(11) NOT NULL," +
				"	`tax_id`	int(11) NOT NULL," +
				"	`tax_type`	varchar(1) NOT NULL," +
				"	`tax_rate`	decimal(25,6) NOT NULL," +
				"	`tax_amount`	decimal(25,6) NOT NULL" +
				");";
        database.execSQL(query);
	}
	
	public void CreateConfigTable(SQLiteDatabase database) {		
		String query = "CREATE TABLE `si_config` (" +
				"	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
				"	`config_name`	varchar(10) NOT NULL," +
				"	`config_value`	varchar(10) NOT NULL" +
				");";
        database.execSQL(query);
	}
	
	public void CreateAreaLineTable(SQLiteDatabase database) {		
		String query = "CREATE TABLE IF NOT EXISTS `si_area_line` ("+
				  "`id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
				  "`domain_id` int(11) NOT NULL DEFAULT '1'," +
				  "`name` varchar(255));";				
        database.execSQL(query);
	}
	
	public void CreateLocationTable(SQLiteDatabase database) {		
		String query = "CREATE TABLE `si_position` (" +
				"  `id` integer PRIMARY KEY AUTOINCREMENT," +
				"  `deviceId` int(11) NOT NULL," +
				"  `deviceTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP," +
				" `fixTime` timestamp NOT NULL," +
				"  `valid` bit(1) NOT NULL," +
				"  `latitude` double NOT NULL," +
				"  `longitude` double NOT NULL," +
				"  `altitude` float NOT NULL," +
				"  `speed` float NOT NULL," +
				"  `course` float NOT NULL," +
				"  `attributes` varchar(4096) NOT NULL," +
				"  `accuracy` int(11) NOT NULL" +
				")";				
        database.execSQL(query);        
	}
	
	public void CreateLPaymentTable(SQLiteDatabase database) {		
		String query = "CREATE TABLE IF NOT EXISTS `si_payment_mobile` (" +
				"  `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
				" `ac_inv_id` int(11) NOT NULL," +
				"  `ac_amount` decimal(25,6) NOT NULL," +
				"  `ac_notes` text NOT NULL," +
				"  `ac_date` datetime NOT NULL," +
				"  `ac_payment_type` int(10) NOT NULL DEFAULT '1'," +
				"  `domain_id` int(11) NOT NULL," +
				"  `online_payment_id` varchar(255) DEFAULT NULL," +
				"  `cheque_no` int(11) NOT NULL," +
				"  `cheque_bank` varchar(30) NOT NULL," +
				"  `cheque_status` int(11) NOT NULL DEFAULT '1'," +
				"  `status` int(11) NOT NULL DEFAULT '0'," +
				"  `user_id` int(11) DEFAULT NULL" +
				")";				
        database.execSQL(query);        
	}
	
	public void CreateLInvoiceStatmentTable(SQLiteDatabase database) {		
		String query = "CREATE TABLE `si_invoice_statement` (" +
				"	`id`	INTEGER NOT NULL," +
				"	`index_id`	INTEGER NOT NULL," +
				"	`customer_id`	INTEGER NOT NULL," +
				"	`owing`	decimal(25,2) NOT NULL," +
				"	`paid`	decimal(25,2) NOT NULL," +
				"	`total`	decimal(25,2) NOT NULL" +
				")";				
        database.execSQL(query);        
	}
	
	public void CreateCustomerPriceTable(SQLiteDatabase database) {		
		String query = "CREATE TABLE `si_customer_products` (" +
				"  `customer_id` int(11) NOT NULL," +
				"  `product_id` int(11) NOT NULL," +
				"  `unit_price` decimal(25,6) NOT NULL," +
				"  `logtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP," +
				"  `quantity` int(11) DEFAULT '0'," +
				"  `id` int(11) DEFAULT '0'," +
				"  `free` int(11) DEFAULT '0'" +
				")";				
        database.execSQL(query);        
	}
	
	public void SyncCustomerProducts(String data){
		new AsyncTaskRunner().execute(data);
	}
	
	public void SyncDB(String response) {
		// TODO Auto-generated method stub
		
		try {
			JSONObject obj = new JSONObject(response);			
			JSONArray billerJSON=obj.getJSONArray("billers");
			JSONArray customerJSON=obj.getJSONArray("customers");
			JSONArray taxJSON=obj.getJSONArray("taxes");
			JSONArray productJSON=obj.getJSONArray("products");
			JSONArray configsJSON=obj.getJSONArray("configs");
			JSONArray AreaLineJSON=obj.getJSONArray("area_lines");
			JSONArray supplierJSON=obj.getJSONArray("suppliers");
			String customer_products=obj.getString("customer_products");
			SyncCustomerProducts(customer_products);
			
			try{
				JSONObject		paymentJSON = obj.getJSONObject("payments");
				updatePayment(paymentJSON);				
			}catch(Exception e){
				e.printStackTrace();
			}
			
			//JSONArray customerPriceJSON=obj.getJSONArray("customerproducts");
			
			SyncBillerTable(billerJSON);
			SyncCustomerTable(customerJSON);
			SyncTaxTable(taxJSON);
			SyncProductTable(productJSON);
			SyncconfigsTable(configsJSON);
			SyncAreaLineTable(AreaLineJSON);
			SyncSupplierTable(supplierJSON);
			
			//SyncCustomerPriceTable(customerPriceJSON);
			
			
			Log.i("DatabaseHelper","SyncDB : DB Sync Completed");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			
			Log.e("DatabaseHelper", "SyncDB", e);
		}
		
		
	}
	
	public void SyncRoute(JSONArray arr){
		try {
			
			
			for(int i=0; i<arr.length();i++){
				JSONObject obj = (JSONObject)arr.get(i);
				InvoiceStatement invoice = new InvoiceStatement(context);
				invoice.setCustomer_id(obj.getString("customer_id"));
				invoice.setId(obj.getString("id"));
				invoice.setIndex_id(obj.getString("index_id"));
				invoice.setOwing(obj.getString("owing"));
				invoice.setTotal(obj.getString("invoice_total"));
				invoice.setPaid(obj.getString("INV_PAID"));
				invoice.save(context);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void SyncProductTable(JSONArray productJSON) {
		// TODO Auto-generated method stub
		SQLiteDatabase database = dbSQL;
		
		try{
			for(int i=0; i<productJSON.length();i++){
				JSONObject obj = (JSONObject)productJSON.get(i);
				ContentValues values = new ContentValues();				
				
				values.put("domain_id", obj.getString("domain_id"));
				values.put("description", obj.getString("description"));
				values.put("unit_price", obj.getString("unit_price"));
				values.put("default_tax_id", obj.getString("default_tax_id"));
				values.put("default_tax_id_2", obj.getString("default_tax_id_2"));
				values.put("cost", obj.getString("cost"));
				values.put("reorder_level", obj.getString("reorder_level"));
				values.put("custom_field1", obj.getString("custom_field1"));
				values.put("custom_field2", obj.getString("custom_field2"));
				values.put("custom_field3", obj.getString("custom_field3"));
				values.put("custom_field4", obj.getString("custom_field4"));
				values.put("notes", obj.getString("notes"));
				values.put("enabled", obj.getString("enabled"));
				values.put("visible", obj.getString("visible"));
				values.put("supplier", obj.getString("supplier"));
				values.put("article_no", obj.getString("article_no"));
				values.put("ean", obj.getString("ean"));
				values.put("manufacturer", obj.getString("manufacturer"));
				
				String qty_out= obj.getString("qty_out");
				String qty_in= obj.getString("qty_in");
				int available_qty=0;
				try{
					if(qty_out.isEmpty() || qty_in.isEmpty() || qty_out.toLowerCase().contains("null") || qty_in.toLowerCase().contains("null")){
						
					}else{
						available_qty = Integer.parseInt(qty_in) - Integer.parseInt(qty_out);
					}
				}catch(Exception e){
					Log.e("Product Sync", "Could not calculate available qty");
				}
				
				values.put("available_qty", available_qty);
				values.put("order_qty", obj.getString("order_quantity"));
				String selectQuery = "SELECT  * FROM si_products where id="+obj.getString("id");
				Cursor cursor = database.rawQuery(selectQuery, null);
				if(cursor.moveToFirst()){
					database.update("si_products", values, "id=?", new String[] {obj.getString("id")});						
				}else{
					values.put("id", obj.getString("id"));
					database.insert("si_products", null, values);												
				}
				closeCursor(cursor);
			}			
		}
		catch(JSONException e){
			
			Log.i("DatabaseHelper", "Unable Sync Product table");
			Log.e("DatabaseHelper", "Sync Product Table", e);
		}
		
	}
	
	
	private void SyncAreaLineTable(JSONArray AreaLineJSON) {
		// TODO Auto-generated method stub
		SQLiteDatabase database = dbSQL;
		
		try{
			for(int i=0; i<AreaLineJSON.length();i++){
				JSONObject obj = (JSONObject)AreaLineJSON.get(i);
				
				ContentValues values = new ContentValues();		
				values.put("domain_id", obj.getString("domain_id"));
				values.put("name", obj.getString("name"));
								
				String selectQuery = "SELECT  * FROM si_area_line where id='"+obj.getString("id")+"'";
				Cursor cursor = database.rawQuery(selectQuery, null);
				if(cursor.moveToFirst()){
					database.update("si_area_line", values, "id=?", new String[] {obj.getString("id")});						
				}else{
					values.put("id", obj.getString("id"));
					database.insert("si_area_line", null, values);												
				}
				closeCursor(cursor);
			}			
		}
		catch(JSONException e){
			
			Log.i("DatabaseHelper", "Unable Sync Area Line table");
			Log.e("DatabaseHelper", "Sync Area Line Table", e);
		}
		
	}
	
	private void SyncconfigsTable(JSONArray configsJSON) {
		// TODO Auto-generated method stub
		SQLiteDatabase database = dbSQL;
		
		try{
			for(int i=0; i<configsJSON.length();i++){
				JSONObject obj = (JSONObject)configsJSON.get(i);
				
				ContentValues values = new ContentValues();		
				values.put(KEY_CONFIG_CONFIGVALUE, obj.getString(KEY_CONFIG_CONFIGVALUE));			
								
				String selectQuery = "SELECT  * FROM si_config where config_name='"+obj.getString(KEY_CONFIG_CONFIGNAME)+"'";
				Cursor cursor = database.rawQuery(selectQuery, null);
				if(cursor.moveToFirst()){
					database.update(TABLE_CONFIG, values, "config_name=?", new String[] {obj.getString(KEY_CONFIG_CONFIGNAME)});						
				}else{
					values.put(KEY_CONFIG_CONFIGNAME, obj.getString(KEY_CONFIG_CONFIGNAME));
					database.insert(TABLE_CONFIG, null, values);												
				}
				closeCursor(cursor);
			}			
		}
		catch(JSONException e){			
			Log.i("DatabaseHelper", "Unable Sync Config table");
			Log.e("DatabaseHelper", "Sync Config Table", e);
		}
		
	}
	
	public void SyncCustomerTable(JSONArray customerJSON) {
		// TODO Auto-generated method stub
		
		SQLiteDatabase database = dbSQL;
				
		try{
			for(int i=0; i<customerJSON.length();i++){
				JSONObject obj = (JSONObject)customerJSON.get(i);
				ContentValues values = new ContentValues();				
				
				values.put("domain_id", obj.getString("domain_id"));
				values.put("attention", obj.getString("attention"));
				values.put("name", obj.getString("name"));
				values.put("street_address", obj.getString("street_address"));
				values.put("street_address2", obj.getString("street_address2"));
				values.put("city", obj.getString("city"));
				values.put("state", obj.getString("state"));
				values.put("zip_code", obj.getString("zip_code"));
				values.put("country", obj.getString("country"));
				values.put("phone", obj.getString("phone"));
				values.put("mobile_phone", obj.getString("mobile_phone"));
				values.put("fax", obj.getString("fax"));
				values.put("email", obj.getString("email"));
				values.put("credit_card_holder_name", obj.getString("credit_card_holder_name"));
				values.put("credit_card_number", obj.getString("credit_card_number"));
				values.put("credit_card_expiry_month", obj.getString("credit_card_expiry_month"));
				values.put("custom_field1", obj.getString("custom_field1"));
				values.put("custom_field2", obj.getString("custom_field2"));
				values.put("custom_field3", obj.getString("custom_field3"));
				values.put("custom_field4", obj.getString("custom_field4"));
				values.put("area_line", obj.getString("area_line"));
				values.put("enabled", obj.getString("enabled"));
				values.put("latitude", obj.getString("latitude"));
				values.put("longitude", obj.getString("longitude"));
				
				String selectQuery = "SELECT  * FROM si_customers where id="+obj.getString("id");
				Cursor cursor = database.rawQuery(selectQuery, null);
				if(cursor.moveToFirst()){
					database.update("si_customers", values, "id=?", new String[] {obj.getString("id")});						
				}else{
					values.put("id", obj.getString("id"));
					database.insert("si_customers", null, values);					
				}	
				closeCursor(cursor);
			}			
		}
		catch(JSONException e){
						
			Log.i("DatabaseHelper", "Unable Sync Customer table");
			Log.e("DatabaseHelper", "Sync Customer Table", e);
		}
				
	}
	
	public void SyncSupplierTable(JSONArray supplierJSON) {
		// TODO Auto-generated method stub
		
		SQLiteDatabase database = dbSQL;
				
		try{
			for(int i=0; i<supplierJSON.length();i++){
				JSONObject obj = (JSONObject)supplierJSON.get(i);
				ContentValues values = new ContentValues();				
				
				
				
				  values.put("domain_id", obj.getString("domain_id"));
				  values.put("name", obj.getString("name"));
				  values.put("street_address", obj.getString("street_address"));
				  values.put("street_address2", obj.getString("street_address2"));
				  values.put("city", obj.getString("city"));
				  values.put("state", obj.getString("state"));
				  values.put("zip_code", obj.getString("zip_code"));
				  values.put("country", obj.getString("country"));
				  values.put("phone", obj.getString("phone"));
				  values.put("mobile_phone", obj.getString("mobile_phone"));
				  values.put("fax", obj.getString("fax"));
				  values.put("email", obj.getString("email"));
				  values.put("logo", obj.getString("logo"));
				  values.put("footer", obj.getString("footer"));
				  values.put("paypal_business_name", obj.getString("paypal_business_name"));
				  values.put("paypal_notify_url", obj.getString("paypal_notify_url"));
				  values.put("paypal_return_url", obj.getString("paypal_return_url"));
				  values.put("eway_customer_id", obj.getString("eway_customer_id"));
				  values.put("notes", obj.getString("notes"));
				  values.put("custom_field1", obj.getString("custom_field1"));
				  values.put("custom_field2", obj.getString("custom_field2"));
				  values.put("custom_field3", obj.getString("custom_field3"));
				  values.put("custom_field4", obj.getString("custom_field4"));
				  values.put("enabled", obj.getString("enabled"));
				
				
				String selectQuery = "SELECT  * FROM si_supplier where id="+obj.getString("id");
				Cursor cursor = database.rawQuery(selectQuery, null);
				if(cursor.moveToFirst()){
					database.update("si_supplier", values, "id=?", new String[] {obj.getString("id")});						
				}else{
					values.put("id", obj.getString("id"));
					database.insert("si_supplier", null, values);												
				}
				closeCursor(cursor);
													
			}			
		}
		catch(JSONException e){			
			
			Log.i("DatabaseHelper", "Unable Sync Supplier table");
			Log.e("DatabaseHelper", "Sync Supplier Table", e);
		}
				
	}
	
	
	public void updatePayment(JSONObject paymentJSON) {
		// TODO Auto-generated method stub
		Iterator<String> iter = paymentJSON.keys();
		while (iter.hasNext()) {
		    String key = iter.next();
		    try {
		        String value = paymentJSON.getString(key);
		        Payment payment = new Payment(context);
		        payment.setId(key);
		        payment.setStatus(value);
		        payment.updateStatus();
		    } catch (JSONException e) {
		        // Something went wrong!
		    }
		}
	}
	public void SyncTaxTable(JSONArray taxJSON) {
		// TODO Auto-generated method stub
		
		SQLiteDatabase database = dbSQL;
				
		try{
			for(int i=0; i<taxJSON.length();i++){
				JSONObject obj = (JSONObject)taxJSON.get(i);
				ContentValues values = new ContentValues();				
				
				values.put("tax_description", obj.getString("tax_description"));
				values.put("tax_percentage", obj.getString("tax_percentage"));
				values.put("type", obj.getString("type"));
				values.put("tax_enabled", obj.getString("tax_enabled"));
				values.put("domain_id", obj.getString("domain_id"));
								
				String selectQuery = "SELECT  * FROM si_tax where tax_id="+obj.getString("tax_id");
				Cursor cursor = database.rawQuery(selectQuery, null);
				if(cursor.moveToFirst()){
					database.update("si_tax", values, "tax_id=?", new String[] {obj.getString("tax_id")});						
				}else{
					values.put("tax_id", obj.getString("tax_id"));
					database.insert("si_tax", null, values);												
				}								
				closeCursor(cursor);					
			}			
		}
		catch(JSONException e){			
			Log.i("DatabaseHelper", "Unable Sync Tax table");
			Log.e("DatabaseHelper", "Sync Tax Table", e);
		}
			
		
		
	}
	public void SyncBillerTable(JSONArray billerJSON) {
		// TODO Auto-generated method stub
		SQLiteDatabase database = dbSQL;
		
		
		try{
			for(int i=0; i<billerJSON.length();i++){
				JSONObject obj = (JSONObject)billerJSON.get(i);			
						ContentValues values = new ContentValues();						
						values.put("domain_id", obj.getString("domain_id"));
						values.put("name", obj.getString("name"));
						values.put("street_address", obj.getString("street_address"));
						values.put("street_address2", obj.getString("street_address2"));
						values.put("city", obj.getString("city"));
						values.put("state", obj.getString("state"));
						values.put("zip_code", obj.getString("zip_code"));
						values.put("country", obj.getString("country"));
						values.put("phone", obj.getString("phone"));
						values.put("mobile_phone", obj.getString("mobile_phone"));
						values.put("fax", obj.getString("fax"));
						values.put("email", obj.getString("email"));
						values.put("logo", obj.getString("logo"));
						values.put("footer", obj.getString("footer"));
						values.put("paypal_business_name", obj.getString("paypal_business_name"));				
						values.put("paypal_notify_url", obj.getString("paypal_notify_url"));
						values.put("paypal_return_url", obj.getString("paypal_return_url"));
						values.put("eway_customer_id", obj.getString("eway_customer_id"));
						values.put("notes", obj.getString("notes"));
						values.put("custom_field1", obj.getString("custom_field1"));
						values.put("custom_field2", obj.getString("custom_field2"));
						values.put("custom_field3", obj.getString("custom_field3"));
						values.put("custom_field4", obj.getString("custom_field4"));
						values.put("enabled", obj.getString("enabled"));
						String selectQuery = "SELECT  * FROM si_biller where id="+obj.getString("id");
						Cursor cursor = database.rawQuery(selectQuery, null);
						if(cursor.moveToFirst()){
							database.update("si_biller", values, "id=?", new String[] {obj.getString("id")});						
						}else{
							values.put("id", obj.getString("id"));
							database.insert("si_biller", null, values);
						}
						closeCursor(cursor);
			}			
		}
		catch(JSONException e){			
			
			Log.i("DatabaseHelper", "Unable Sync Biller table");
			Log.e("DatabaseHelper", "Sync Biller Table", e);
		}			
		
	}
	
	public List<String> getAllProducts(){  
        List<String> list = new ArrayList<String>();        
        // Select All Query  
        String selectQuery = "SELECT description||custom_field1||custom_field2||'###'||id as prod_desc FROM si_products WHERE enabled  ORDER BY description" ;  
                
        SQLiteDatabase db = dbSQL;  
        Cursor cursor = db.rawQuery(selectQuery, null);//selectQuery,selectedArguments  
   
        // looping through all rows and adding to list  
        if (cursor.moveToFirst()) {  
            do {            	
                //list.add(cursor.getString(0));//adding 2nd column data
                list.add(cursor.getString(0));
            } while (cursor.moveToNext());  
        }  
        // closing connection  
        closeCursor(cursor);
                     
        
        return list;  
    }
	
	public List<String> getAllProductSupplier(){  
        List<String> list = new ArrayList<String>();        
        // Select All Query  
        String selectQuery = "SELECT distinct supplier FROM si_products WHERE enabled  ORDER BY supplier" ;  
                
        SQLiteDatabase db = dbSQL;  
        Cursor cursor = db.rawQuery(selectQuery, null);//selectQuery,selectedArguments  
        list.add("All");
        // looping through all rows and adding to list  
        if (cursor.moveToFirst()) {  
            do {            	
                //list.add(cursor.getString(0));//adding 2nd column data
                list.add(cursor.getString(0));
            } while (cursor.moveToNext());  
        }  
        // closing connection  
        closeCursor(cursor); 
                          
        
        return list;  
    }
	
	public List<String> getAllProductManufacturer(){  
        List<String> list = new ArrayList<String>();        
        // Select All Query  
        String selectQuery = "SELECT distinct manufacturer FROM si_products WHERE enabled  ORDER BY supplier" ;  
                
        SQLiteDatabase db = dbSQL;  
        Cursor cursor = db.rawQuery(selectQuery, null);//selectQuery,selectedArguments  
        list.add("All");
        // looping through all rows and adding to list  
        if (cursor.moveToFirst()) {  
            do {            	
                //list.add(cursor.getString(0));//adding 2nd column data
            	if(!cursor.getString(0).isEmpty()){
            		list.add(cursor.getString(0));            		
            	}
                
            } while (cursor.moveToNext());  
        }  
        // closing connection  
        closeCursor(cursor);
                          
        
        return list;  
    }
	public List<String> getAllCustomers(){  
        List<String> list = new ArrayList<String>();        
        // Select All Query  
        String selectQuery = "SELECT name || street_address||'###'||id as concatname FROM si_customers WHERE enabled != 0 ORDER BY name" ;  
                
        SQLiteDatabase db = dbSQL;  
        Cursor cursor = db.rawQuery(selectQuery, null);//selectQuery,selectedArguments  
   
        // looping through all rows and adding to list  
        if (cursor.moveToFirst()) {  
            do {            	
                //list.add(cursor.getString(0));//adding 2nd column data
                list.add(cursor.getString(0));
            } while (cursor.moveToNext());  
        }  
        // closing connection  
        closeCursor(cursor);
                                  
        return list;  
    }
	
	public List<Product> getAllProductsBySupplier(String supplier, String orderID){  
        List<Product> list = new ArrayList<Product>();        
        // Select All Query  
        
        String selectQuery = "SELECT description||', '||custom_field1||', '||custom_field2 as prod_desc, p.*,(SELECT ifnull(SUM(QUANTITY),0) as qty FROM SI_ORDER_ITEMS WHERE PRODUCT_ID=p.id and order_id="+orderID+") AS qty,(select ifnull(sum(quantity),0) + ifnull(sum(free_qty),0) from si_order_items i join si_orders o on o.id=i.order_id where index_id=0 and o.type_id=2 and product_id=p.id) as unsync_qty FROM si_products p WHERE enabled !=0 and supplier = '"+supplier+"'  ORDER BY qty desc, description asc" ;
        if(supplier.toLowerCase().contentEquals("all")){
        	selectQuery = "SELECT description||', '||custom_field1||', '||custom_field2 as prod_desc, p.*,(SELECT ifnull(SUM(QUANTITY),0) as qty FROM SI_ORDER_ITEMS WHERE PRODUCT_ID=p.id and order_id="+orderID+") AS qty,(select ifnull(sum(quantity),0) + ifnull(sum(free_qty),0) from si_order_items i join si_orders o on o.id=i.order_id where index_id=0 and o.type_id=2 and product_id=p.id) as unsync_qty FROM si_products p WHERE enabled !=0   ORDER BY qty desc, description asc" ;
        }
        SQLiteDatabase db = dbSQL;  
        Cursor cursor = db.rawQuery(selectQuery, null);//selectQuery,selectedArguments  
        
        // looping through all rows and adding to list  
        if (cursor.moveToFirst()) {  
            do {            	
                //list.add(cursor.getString(0));//adding 2nd column data
                //list.add(this.getProductById(cursor.getString(1)));                
            	Product product = new Product(this.context);
            	product.setId(cursor.getString(cursor.getColumnIndex("id")));
    			product.setDomain_id(cursor.getString(cursor.getColumnIndex("domain_id")));
    			product.setDescription(cursor.getString(cursor.getColumnIndex("description")));
    			product.setUnit_price(cursor.getDouble(cursor.getColumnIndex("unit_price")));
    			product.setDefault_tax_id(cursor.getInt(cursor.getColumnIndex("default_tax_id")));
    			product.setDefault_tax_id_2(cursor.getInt(cursor.getColumnIndex("default_tax_id_2")));
    			product.setCost(cursor.getDouble(cursor.getColumnIndex("cost")));
    			product.setReorder_level(cursor.getString(cursor.getColumnIndex("reorder_level")));
    			product.setCustom_field1(cursor.getString(cursor.getColumnIndex("custom_field1")));
    			product.setCustom_field2(cursor.getString(cursor.getColumnIndex("custom_field2")));
    			product.setCustom_field3(cursor.getString(cursor.getColumnIndex("custom_field3")));
    			product.setCustom_field4(cursor.getString(cursor.getColumnIndex("custom_field4")));
    			product.setNotes(cursor.getString(cursor.getColumnIndex("notes")));
    			product.setEnabled(cursor.getString(cursor.getColumnIndex("enabled")));
    			product.setVisible(cursor.getString(cursor.getColumnIndex("visible")));
    			product.setSupplier(cursor.getString(cursor.getColumnIndex("supplier")));
    			product.setManufacturer(cursor.getString(cursor.getColumnIndex("manufacturer")));
    			product.setAvailableQty(cursor.getString(cursor.getColumnIndex("available_qty")));
    			product.setOrderQty(cursor.getInt(cursor.getColumnIndex("order_qty")));
    			product.setCurrent_order_qty(cursor.getInt(cursor.getColumnIndex("qty")));
    			product.setUnsync_order_qty(cursor.getInt(cursor.getColumnIndex("unsync_qty")));
            	list.add(product);
            } while (cursor.moveToNext());  
        }  
        // closing connection  
        closeCursor(cursor);  
                                  
        return list;  
    }
	
	public List<Product> getAllProductsByManufacturer(String manufacturer, String orderID){  
        List<Product> list = new ArrayList<Product>();        
        // Select All Query  
        
        String selectQuery = "SELECT description||', '||custom_field1||', '||custom_field2 as prod_desc, p.*,(SELECT ifnull(SUM(QUANTITY),0) as qty FROM SI_ORDER_ITEMS WHERE PRODUCT_ID=p.id and order_id="+orderID+") AS qty,(select ifnull(sum(quantity),0) + ifnull(sum(free_qty),0) from si_order_items i join si_orders o on o.id=i.order_id where index_id=0 and o.type_id=2 and product_id=p.id) as unsync_qty FROM si_products p WHERE enabled !=0 and manufacturer = '"+manufacturer+"' ORDER BY qty desc, description asc" ;
        if(manufacturer.toLowerCase().contentEquals("all")){
        	selectQuery = "SELECT description||', '||custom_field1||', '||custom_field2 as prod_desc, p.*,(SELECT ifnull(SUM(QUANTITY),0) as qty FROM SI_ORDER_ITEMS WHERE PRODUCT_ID=p.id and order_id="+orderID+") AS qty,(select ifnull(sum(quantity),0) + ifnull(sum(free_qty),0) from si_order_items i join si_orders o on o.id=i.order_id where index_id=0 and o.type_id=2 and product_id=p.id) as unsync_qty FROM si_products p WHERE enabled !=0  ORDER BY qty desc, description asc" ;
        }
        SQLiteDatabase db = dbSQL;  
        Cursor cursor = db.rawQuery(selectQuery, null);//selectQuery,selectedArguments  
        
        // looping through all rows and adding to list  
        if (cursor.moveToFirst()) {  
            do {            	
                //list.add(cursor.getString(0));//adding 2nd column data
                //list.add(this.getProductById(cursor.getString(1)));                
            	Product product = new Product(this.context);
            	product.setId(cursor.getString(cursor.getColumnIndex("id")));
    			product.setDomain_id(cursor.getString(cursor.getColumnIndex("domain_id")));
    			product.setDescription(cursor.getString(cursor.getColumnIndex("description")));
    			product.setUnit_price(cursor.getDouble(cursor.getColumnIndex("unit_price")));
    			product.setDefault_tax_id(cursor.getInt(cursor.getColumnIndex("default_tax_id")));
    			product.setDefault_tax_id_2(cursor.getInt(cursor.getColumnIndex("default_tax_id_2")));
    			product.setCost(cursor.getDouble(cursor.getColumnIndex("cost")));
    			product.setReorder_level(cursor.getString(cursor.getColumnIndex("reorder_level")));
    			product.setCustom_field1(cursor.getString(cursor.getColumnIndex("custom_field1")));
    			product.setCustom_field2(cursor.getString(cursor.getColumnIndex("custom_field2")));
    			product.setCustom_field3(cursor.getString(cursor.getColumnIndex("custom_field3")));
    			product.setCustom_field4(cursor.getString(cursor.getColumnIndex("custom_field4")));
    			product.setNotes(cursor.getString(cursor.getColumnIndex("notes")));
    			product.setEnabled(cursor.getString(cursor.getColumnIndex("enabled")));
    			product.setVisible(cursor.getString(cursor.getColumnIndex("visible")));
    			product.setSupplier(cursor.getString(cursor.getColumnIndex("supplier")));
    			product.setManufacturer(cursor.getString(cursor.getColumnIndex("manufacturer")));
    			product.setAvailableQty(cursor.getString(cursor.getColumnIndex("available_qty")));
    			product.setOrderQty(cursor.getInt(cursor.getColumnIndex("order_qty")));
    			product.setCurrent_order_qty(cursor.getInt(cursor.getColumnIndex("qty")));
    			product.setUnsync_order_qty(cursor.getInt(cursor.getColumnIndex("unsync_qty")));
            	list.add(product);
            } while (cursor.moveToNext());  
        }  
        // closing connection  
        closeCursor(cursor);
                                  
        return list;  
    }
	
	public List<Supplier> getAllSupplier(){  
        List<Supplier> list = new ArrayList<Supplier>();        
        // Select All Query  
        
        String selectQuery = "SELECT id from si_supplier order by id asc" ;        
        SQLiteDatabase db = dbSQL;  
        Cursor cursor = db.rawQuery(selectQuery, null);//selectQuery,selectedArguments  
        
        // looping through all rows and adding to list  
        if (cursor.moveToFirst()) {  
            do {            	
                //list.add(cursor.getString(0));//adding 2nd column data
                list.add(this.getSupplier(cursor.getString(0)));
            } while (cursor.moveToNext());  
        }  
        // closing connection  
        closeCursor(cursor);
                                  
        return list;  
    }
	public List<Customer> getAllCustomersByAreaLine(int area_line){  
        List<Customer> list = new ArrayList<Customer>();        
        // Select All Query
        String selectQuery="";
        if(area_line ==0){
        	selectQuery = "SELECT id FROM si_customers WHERE enabled != 0 ORDER BY name" ;        	
        }else{
        	selectQuery = "SELECT id FROM si_customers WHERE enabled != 0 and area_line="+String.valueOf(area_line)+" ORDER BY name" ;
        }
          
                
        SQLiteDatabase db = dbSQL;  
        Cursor cursor = db.rawQuery(selectQuery, null);//selectQuery,selectedArguments  
   
        // looping through all rows and adding to list  
        if (cursor.moveToFirst()) {  
            do {            	
                //list.add(cursor.getString(0));//adding 2nd column data
                list.add(this.getCustomer(cursor.getString(0)));
            } while (cursor.moveToNext());  
        }  
        // closing connection  
        closeCursor(cursor);
                                  
        return list;  
    }
	
	
	public List<Customer> getNearestCustomer( String bufferDistance){  
        List<Customer> list = new ArrayList<Customer>();        
        // Select All Query
        String selectQuery="";
        selectQuery = "SELECT id FROM si_customers WHERE enabled != 0 ORDER BY name" ;  
        
        int intBufferDistance = 500;  
                
        SQLiteDatabase db = dbSQL;  
        Cursor cursor = db.rawQuery(selectQuery, null);//selectQuery,selectedArguments
        Position position = getLastKnownLocation();
        if(position == null){        	
        	Log.i("DatabaseHelper", "getNearestCustomer :Last location is unknown");
        	return list;
        }
        Location loc = new Location("");
        loc.setLatitude(position.getLatitude());
        loc.setLongitude(position.getLongitude());
        try{
        	intBufferDistance = Integer.parseInt(bufferDistance);
        }catch(Exception e){
        	Log.i("DatabaseHelper","getNearestCustomer",e);
        	e.printStackTrace();
        }
        // looping through all rows and adding to list  
        if (cursor.moveToFirst()) {  
            do {            	
                //list.add(cursor.getString(0));//adding 2nd column data
            	Customer customer = getCustomer(cursor.getString(0));            	
            	if(customer.distance(loc)<intBufferDistance){
            		list.add(customer);
            	}       
                
            } while (cursor.moveToNext());  
        }  
        // closing connection  
        closeCursor(cursor); 
                                  
        return list;  
    }
	public List<AutoCompleteItem> getAllCustomersItems(){  
        
        ArrayList<AutoCompleteItem> list = new  ArrayList<AutoCompleteItem>();
        // Select All Query  
        String selectQuery = "SELECT name as concatname,id FROM si_customers WHERE enabled != 0 ORDER BY name" ;  
                
        SQLiteDatabase db = dbSQL;  
        Cursor cursor = db.rawQuery(selectQuery, null);//selectQuery,selectedArguments  
   
        // looping through all rows and adding to list  
        if (cursor.moveToFirst()) {  
            do {            	
                //list.add(cursor.getString(0));//adding 2nd column data                
                list.add(new AutoCompleteItem(cursor.getString(cursor.getColumnIndex("concatname")),cursor.getInt(cursor.getColumnIndex("id"))));              
                
            } while (cursor.moveToNext());  
        }  
        // closing connection  
        closeCursor(cursor);
                                  
        return list;  
    }
	
public List<AreaLine> getAllAreaLines(){  
        
        ArrayList<AreaLine> list = new  ArrayList<AreaLine>();
        // Select All Query  
        String selectQuery = "SELECT * from si_area_line order by name" ;  
                
        SQLiteDatabase db = dbSQL;  
        Cursor cursor = db.rawQuery(selectQuery, null);//selectQuery,selectedArguments  
   
        // looping through all rows and adding to list  
        if (cursor.moveToFirst()) {  
            do {            	
                //list.add(cursor.getString(0));//adding 2nd column data
            	AreaLine arealine = new AreaLine(context);
            	arealine.setId(cursor.getString(cursor.getColumnIndex("id")));
            	arealine.setName(cursor.getString(cursor.getColumnIndex("name")));
                list.add(arealine);                
            } while (cursor.moveToNext());  
        }  
        // closing connection  
        closeCursor(cursor); 
                                  
        return list;  
    }
	

public AreaLine getRouteById(int id){  
    
    AreaLine route = null;
    // Select All Query  
    String selectQuery = "SELECT * from si_area_line where id="+id+" order by name" ;  
            
    SQLiteDatabase db = dbSQL;  
    Cursor cursor = db.rawQuery(selectQuery, null);//selectQuery,selectedArguments  

    // looping through all rows and adding to list  
    if (cursor.moveToFirst()) {  
              	
            //list.add(cursor.getString(0));//adding 2nd column data
        	route = new  AreaLine(context);
        	route.setId(cursor.getString(cursor.getColumnIndex("id")));
        	route.setName(cursor.getString(cursor.getColumnIndex("name")));
                           
        
    }  
    // closing connection  
    closeCursor(cursor); 
                              
    return route;  
}


public List<InvoiceStatement> getCustomerPendingPayments(String customer_id){  
    
    ArrayList<InvoiceStatement> list = new  ArrayList<InvoiceStatement>();
    // Select All Query  
    String selectQuery = "SELECT * from si_invoice_statement where customer_id = "+customer_id+" order by id asc" ;  
            
    SQLiteDatabase db = dbSQL;  
    Cursor cursor = db.rawQuery(selectQuery, null);//selectQuery,selectedArguments  

    // looping through all rows and adding to list  
    if (cursor.moveToFirst()) {  
        do {            	
            //list.add(cursor.getString(0));//adding 2nd column data
        	InvoiceStatement invoice = new InvoiceStatement(context);
        	invoice.setId(cursor.getString(cursor.getColumnIndex("id")));
        	invoice.setCustomer_id(cursor.getString(cursor.getColumnIndex("customer_id")));
			invoice.setId(cursor.getString(cursor.getColumnIndex("id")));
			invoice.setIndex_id(cursor.getString(cursor.getColumnIndex("index_id")));
			invoice.setOwing(cursor.getString(cursor.getColumnIndex("owing")));
			invoice.setTotal(cursor.getString(cursor.getColumnIndex("total")));
			invoice.setPaid(cursor.getString(cursor.getColumnIndex("paid")));
            list.add(invoice);                
        } while (cursor.moveToNext());  
    }  
    // closing connection  
    closeCursor(cursor); 
                              
    return list;  
}

	public List<AutoCompleteItem> getAllProductsItems(){  
        
        ArrayList<AutoCompleteItem> list = new  ArrayList<AutoCompleteItem>();
        // Select All Query  
        String selectQuery = "SELECT description||', '||custom_field1||', '||custom_field2 as prod_desc,id FROM si_products WHERE enabled != 0  ORDER BY description" ;  
                
        SQLiteDatabase db = dbSQL;  
        Cursor cursor = db.rawQuery(selectQuery, null);//selectQuery,selectedArguments  
   
        // looping through all rows and adding to list  
        if (cursor.moveToFirst()) {  
            do {            	
                //list.add(cursor.getString(0));//adding 2nd column data                
                list.add(new AutoCompleteItem(cursor.getString(cursor.getColumnIndex("prod_desc")),cursor.getInt(cursor.getColumnIndex("id"))));              
                
            } while (cursor.moveToNext());  
        }  
        // closing connection  
        closeCursor(cursor);  
                                  
        return list;  
    }
	
	
	
	public Order getOrder(String OrderID){
		Order order = new Order(this.context);
		SQLiteDatabase database = dbSQL;
		
		Cursor cursor = database.query(TABLE_ORDER, null, KEY_ORDER_ID+"=?", new String[] {OrderID}, null, null, null);		
		if(cursor.moveToFirst()){			
			order.setID(cursor.getString(cursor.getColumnIndex(KEY_ORDER_ID)));			
			order.setIndexID(cursor.getString(cursor.getColumnIndex(KEY_ORDER_INDEX_ID)));
			order.setDomainID(cursor.getString(cursor.getColumnIndex(KEY_ORDER_DOMAIN_ID)));
			order.setBillerID(cursor.getString(cursor.getColumnIndex(KEY_ORDER_BILLER_ID)));
			order.setCustomerID(cursor.getString(cursor.getColumnIndex(KEY_ORDER_CUSTOMER_ID)));
			order.setTypeID(cursor.getString(cursor.getColumnIndex(KEY_ORDER_TYPE_ID)));			
			order.setPreferenceID(cursor.getString(cursor.getColumnIndex(KEY_ORDER_PREFERENCE_ID)));
			order.setDate(cursor.getString(cursor.getColumnIndex(KEY_ORDER_DATE)));			
			order.setCustomField1(cursor.getString(cursor.getColumnIndex(KEY_ORDER_CUSTOM_FIELD1)));
			order.setCustomField2(cursor.getString(cursor.getColumnIndex(KEY_ORDER_CUSTOM_FIELD2)));
			order.setCustomField3(cursor.getString(cursor.getColumnIndex(KEY_ORDER_CUSTOM_FIELD3)));
			order.setCustomField4(cursor.getString(cursor.getColumnIndex(KEY_ORDER_CUSTOM_FIELD4)));			
			order.setNote(cursor.getString(cursor.getColumnIndex(KEY_ORDER_NOTE)));			
			order.setSalesman(cursor.getString(cursor.getColumnIndex(KEY_ORDER_SALESMAN)));			
			order.setStatus(cursor.getString(cursor.getColumnIndex(KEY_ORDER_STATUS)));			
			order.setInvoiceID(cursor.getString(cursor.getColumnIndex(KEY_ORDER_INVOICE_ID)));
			
		}else{
						
		}	
		closeCursor(cursor);
		return order;
	}
	
	
	public JSONArray getUnSyncPaymentJSON(){
		
		SQLiteDatabase database = dbSQL;
		
		Cursor cursor = database.query(TABLE_PAYMENT_MOBILE, null, KEY_PAYMENTMOBILE_STATUS+"=?", new String[] {"0"}, null, null, null);
		JSONArray array = new UtilityFunctions().getResults(cursor);
			
		closeCursor(cursor);
		return array;
	}
	
	public OrderItem getOrderItem(String OrderItem){
		OrderItem item = new OrderItem(this.context);
		SQLiteDatabase database = dbSQL;		
		Cursor cursor = database.query(TABLE_ORDER_ITEM, null, KEY_ORDERITEM_ID+"=?", new String[] {OrderItem}, null, null, null);		
		if(cursor.moveToFirst()){
			item.setID(cursor.getString(cursor.getColumnIndex(KEY_ORDERITEM_ID)));
			item.setBilled(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_ORDERITEM_BILLED)));
			item.setDescription(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_ORDERITEM_DESCRIPTION)));
			item.setFreeID(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.KEY_ORDERITEM_FREE_ID)));
			item.setFreeQty(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.KEY_ORDERITEM_FREE_QTY)));
			item.setOrderID(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_ORDERITEM_ORDER_ID)));
			item.setProductID(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.KEY_ORDERITEM_PRODUCT_ID)));
			item.setQuantity(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.KEY_ORDERITEM_QUANTITY)));
			item.setTaxAmount(cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.KEY_ORDERITEM_TAX_AMOUNT)));
			item.setGrossAmount(cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.KEY_ORDERITEM_GROSS_TOTAL)));
			item.setUnitPrice(cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.KEY_ORDERITEM_UNIT_PRICE)));
		}else{
						
		}	
		closeCursor(cursor);
		return item;
	}
	
	public OrderItemTax getOrderItemTax(String OrderItemTaxID){
		OrderItemTax itemTax = new OrderItemTax(this.context);
		SQLiteDatabase database = dbSQL;		
		Cursor cursor = database.query(TABLE_ORDER_ITEM_TAX, null, KEY_ORDERITEM_TAX_ID+"=?", new String[] {OrderItemTaxID}, null, null, null);		
		if(cursor.moveToFirst()){
			itemTax.setID(String.valueOf(cursor.getInt(cursor.getColumnIndex(KEY_ORDERITEM_TAX_ID))));
			itemTax.setOrderItemID(cursor.getInt(cursor.getColumnIndex(KEY_ORDERITEM_TAX_ORDER_ITEM_ID)));
			itemTax.setTaxAmount(cursor.getDouble(cursor.getColumnIndex(KEY_ORDERITEM_TAX_TAX_AMOUNT)));
			itemTax.setTaxID(cursor.getInt(cursor.getColumnIndex(KEY_ORDERITEM_TAX_TAX_ID)));
			itemTax.setTaxRate(cursor.getDouble(cursor.getColumnIndex(KEY_ORDERITEM_TAX_TAX_RATE)));
			itemTax.setTaxType(cursor.getString(cursor.getColumnIndex(KEY_ORDERITEM_TAX_TAX_TYPE)));			
		}else{
						
		}	
		closeCursor(cursor);
		return itemTax;
	}
	
	public Customer getCustomer(String CustomerID){
		Customer customer = new Customer(context);
		SQLiteDatabase database = dbSQL;		
		Cursor cursor = database.query(TABLE_CUSTOMER, null, KEY_CUSTOMER_ID+"=?", new String[] {CustomerID}, null, null, null);		
		if(cursor.moveToFirst()){
			
			customer.setID(cursor.getString(cursor.getColumnIndex(KEY_CUSTOMER_ID)));			
			customer.setAttention(cursor.getString(cursor.getColumnIndex(KEY_CUSTOMER_ATTENTION)));			
			customer.setCity(cursor.getString(cursor.getColumnIndex(KEY_CUSTOMER_CITY)));			
			customer.setCountry(cursor.getString(cursor.getColumnIndex(KEY_CUSTOMER_COUNTRY)));			
			customer.setCreditCardExpiryMonth(cursor.getString(cursor.getColumnIndex(KEY_CUSTOMER_CREDIT_CARD_EXPIRY_MONTH)));			
			customer.setCreditCardExpiryYear(cursor.getString(cursor.getColumnIndex(KEY_CUSTOMER_CREDIT_CARD_EXPIRY_YEAR)));			
			customer.setCreditCardHolderName(cursor.getString(cursor.getColumnIndex(KEY_CUSTOMER_CREDIT_CARD_HOLDER_NAME)));			
			customer.setCreditCardNumber(cursor.getString(cursor.getColumnIndex(KEY_CUSTOMER_CREDIT_CARD_NUMBER)));			
			customer.setCustomField1(cursor.getString(cursor.getColumnIndex(KEY_CUSTOMER_CUSTOM_FIELD1)));			
			customer.setCustomField2(cursor.getString(cursor.getColumnIndex(KEY_CUSTOMER_CUSTOM_FIELD2)));			
			customer.setCustomField3(cursor.getString(cursor.getColumnIndex(KEY_CUSTOMER_CUSTOM_FIELD3)));			
			customer.setCustomField4(cursor.getString(cursor.getColumnIndex(KEY_CUSTOMER_CUSTOM_FIELD4)));			
			customer.setDomainID(cursor.getString(cursor.getColumnIndex(KEY_CUSTOMER_DOMAIN_ID)));			
			customer.setEmail(cursor.getString(cursor.getColumnIndex(KEY_CUSTOMER_EMAIL)));			
			customer.setEnabled(cursor.getString(cursor.getColumnIndex(KEY_CUSTOMER_ENABLED)));			
			customer.setFax(cursor.getString(cursor.getColumnIndex(KEY_CUSTOMER_FAX)));			
			customer.setMobilePhone(cursor.getString(cursor.getColumnIndex(KEY_CUSTOMER_MOBILE_PHONE)));			
			customer.setName(cursor.getString(cursor.getColumnIndex(KEY_CUSTOMER_NAME)));
			customer.setNotes(cursor.getString(cursor.getColumnIndex(KEY_CUSTOMER_NOTES)));
			customer.setPhone(cursor.getString(cursor.getColumnIndex(KEY_CUSTOMER_PHONE)));
			customer.setState(cursor.getString(cursor.getColumnIndex(KEY_CUSTOMER_STATE)));
			customer.setStreetAddress(cursor.getString(cursor.getColumnIndex(KEY_CUSTOMER_STREET_ADDRESS)));
			customer.setStreetAddress2(cursor.getString(cursor.getColumnIndex(KEY_CUSTOMER_STREET_ADDRESS2)));
			customer.setZipCode(cursor.getString(cursor.getColumnIndex(KEY_CUSTOMER_ZIP_CODE)));
			customer.setAreaLine(cursor.getInt(cursor.getColumnIndex(KEY_CUSTOMER_AREA_LINE)));
			customer.setLatitude(cursor.getDouble(cursor.getColumnIndex(KEY_CUSTOMER_LATITUDE)));
			customer.setLongitude(cursor.getDouble(cursor.getColumnIndex(KEY_CUSTOMER_LONGITUDE)));
		}else{
						
		}	
		closeCursor(cursor);
		return customer;
	}
	
	public Supplier getSupplier(String id){
		Supplier supplier = new Supplier(context);
		SQLiteDatabase database = dbSQL;		
		Cursor cursor = database.query(TABLE_SUPPLIER, null, KEY_SUPPLIER_ID+"=?", new String[] {id}, null, null, null);		
		if(cursor.moveToFirst()){			
						
			supplier.setId(cursor.getString(cursor.getColumnIndex(this.KEY_SUPPLIER_ID)));			
			supplier.setDomain_id(cursor.getString(cursor.getColumnIndex(this.KEY_SUPPLIER_DOMAIN_ID)));
			supplier.setName(cursor.getString(cursor.getColumnIndex(this.KEY_SUPPLIER_NAME)));
			supplier.setStreet_address(cursor.getString(cursor.getColumnIndex(this.KEY_SUPPLIER_STREET_ADDRESS)));
			supplier.setStreet_address2(cursor.getString(cursor.getColumnIndex(this.KEY_SUPPLIER_STREET_ADDRESS2)));
			supplier.setCity(cursor.getString(cursor.getColumnIndex(this.KEY_SUPPLIER_CITY)));
			supplier.setState(cursor.getString(cursor.getColumnIndex(this.KEY_SUPPLIER_STATE)));
			supplier.setZip_code(cursor.getString(cursor.getColumnIndex(this.KEY_SUPPLIER_ZIP_CODE)));
			supplier.setCountry(cursor.getString(cursor.getColumnIndex(this.KEY_SUPPLIER_COUNTRY)));
			supplier.setPhone(cursor.getString(cursor.getColumnIndex(this.KEY_SUPPLIER_PHONE)));
			supplier.setMobile_phone(cursor.getString(cursor.getColumnIndex(this.KEY_SUPPLIER_MOBILE_PHONE)));
			supplier.setFax(cursor.getString(cursor.getColumnIndex(this.KEY_SUPPLIER_FAX)));
			supplier.setEmail(cursor.getString(cursor.getColumnIndex(this.KEY_SUPPLIER_EMAIL)));
			supplier.setLogo(cursor.getString(cursor.getColumnIndex(this.KEY_SUPPLIER_LOGO)));
			supplier.setFooter(cursor.getString(cursor.getColumnIndex(this.KEY_SUPPLIER_FOOTER)));
			supplier.setPaypal_business_name(cursor.getString(cursor.getColumnIndex(this.KEY_SUPPLIER_PAYPAL_BUSINESS_NAME)));
			supplier.setPaypal_notify_url(cursor.getString(cursor.getColumnIndex(this.KEY_SUPPLIER_PAYPAL_NOTIFY_URL)));
			supplier.setPaypal_return_url(cursor.getString(cursor.getColumnIndex(this.KEY_SUPPLIER_PAYPAL_RETURN_URL)));
			supplier.setEway_customer_id(cursor.getString(cursor.getColumnIndex(this.KEY_SUPPLIER_EWAY_CUSTOMER_ID)));
			supplier.setNotes(cursor.getString(cursor.getColumnIndex(this.KEY_SUPPLIER_NOTES)));
			supplier.setCustom_field1(cursor.getString(cursor.getColumnIndex(this.KEY_SUPPLIER_CUSTOM_FIELD1)));
			supplier.setCustom_field2(cursor.getString(cursor.getColumnIndex(this.KEY_SUPPLIER_CUSTOM_FIELD2)));
			supplier.setCustom_field3(cursor.getString(cursor.getColumnIndex(this.KEY_SUPPLIER_CUSTOM_FIELD3)));
			supplier.setCustom_field4(cursor.getString(cursor.getColumnIndex(this.KEY_SUPPLIER_CUSTOM_FIELD4)));
			supplier.setEnabled(cursor.getString(cursor.getColumnIndex(this.KEY_SUPPLIER_ENABLED)));
		}else{
						
		}	
		closeCursor(cursor);
		return supplier;
	}
	
	public Position getLastKnownLocation(){
		Position position= null;
		SQLiteDatabase database = dbSQL;		
		
		Cursor cursor = database.query(TABLE_POSITION, new String[] {DatabaseHelper.KEY_LOCATION_ID}, null, null, null, null, "id desc", "1");
		if(cursor.moveToFirst()){
			position = getPosition(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.KEY_LOCATION_ID)));
		}
		closeCursor(cursor);
		return position;
	}
	
	public Position getPosition(int postionID){
		Position position = new Position(this.context);
		SQLiteDatabase database = dbSQL;		
		Cursor cursor = database.query(TABLE_POSITION, null, KEY_LOCATION_ID+"=?", new String[] {String.valueOf(postionID)}, null, null, null);		
		if(cursor.moveToFirst()){
			position.setId(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.KEY_LOCATION_ID)));
			position.setAccuracy(cursor.getFloat(cursor.getColumnIndex(DatabaseHelper.KEY_LOCATION_ACCURACY)));
			position.setAltitude(cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.KEY_LOCATION_ALTITUDE)));
			position.setAttributes(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_LOCATION_ATTRIBUTES)));
			position.setCourse(cursor.getFloat(cursor.getColumnIndex(DatabaseHelper.KEY_LOCATION_COURSE)));
			position.setDeviceid(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_LOCATION_DEVICEID)));
			position.setDevicetime(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_LOCATION_DEVICETIME)));
			position.setFixtime(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_LOCATION_FIXTIME)));
			position.setLatitude(cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.KEY_LOCATION_LATITUDE)));
			position.setLongitude(cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.KEY_LOCATION_LONGITUDE)));
			position.setSpeed(cursor.getFloat(cursor.getColumnIndexOrThrow(DatabaseHelper.KEY_LOCATION_SPEED)));
			position.setValid(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_LOCATION_VALID)));
		}else{
						
		}	
		closeCursor(cursor);
		return position;
	}
	public Product getProductById(String ProductID){
		Product product = new Product(this.context);
		SQLiteDatabase database = dbSQL;		
		Cursor cursor = database.query(TABLE_PRODUCT, null, "id=?", new String[] {ProductID}, null, null, null);		
		if(cursor.moveToFirst()){
			product.setId(cursor.getString(cursor.getColumnIndex("id")));
			product.setDomain_id(cursor.getString(cursor.getColumnIndex("domain_id")));
			product.setDescription(cursor.getString(cursor.getColumnIndex("description")));
			product.setUnit_price(cursor.getDouble(cursor.getColumnIndex("unit_price")));
			product.setDefault_tax_id(cursor.getInt(cursor.getColumnIndex("default_tax_id")));
			product.setDefault_tax_id_2(cursor.getInt(cursor.getColumnIndex("default_tax_id_2")));
			product.setCost(cursor.getDouble(cursor.getColumnIndex("cost")));
			product.setReorder_level(cursor.getString(cursor.getColumnIndex("reorder_level")));
			product.setCustom_field1(cursor.getString(cursor.getColumnIndex("custom_field1")));
			product.setCustom_field2(cursor.getString(cursor.getColumnIndex("custom_field2")));
			product.setCustom_field3(cursor.getString(cursor.getColumnIndex("custom_field3")));
			product.setCustom_field4(cursor.getString(cursor.getColumnIndex("custom_field4")));
			product.setNotes(cursor.getString(cursor.getColumnIndex("notes")));
			product.setEnabled(cursor.getString(cursor.getColumnIndex("enabled")));
			product.setVisible(cursor.getString(cursor.getColumnIndex("visible")));
			product.setSupplier(cursor.getString(cursor.getColumnIndex("supplier")));
			product.setManufacturer(cursor.getString(cursor.getColumnIndex("manufacturer")));
			product.setAvailableQty(cursor.getString(cursor.getColumnIndex("available_qty")));
			product.setOrderQty(cursor.getInt(cursor.getColumnIndex("order_qty")));
								
		}else{
						
		}	
		closeCursor(cursor);
		return product;
	}
	
	public CustomerProduct getCustomerProductPriceObj(String ProductID, String CustomerID){
		
		
		if(sharedPrefs.getString("prefCustomerProductPrice", "").toLowerCase().contentEquals("disabled")){
			return null;
		}
		CustomerProduct product = new CustomerProduct(this.context);
		SQLiteDatabase database = dbSQL;		
		Cursor cursor = database.query("si_customer_products", null, "customer_id=? and product_id=?", new String[] {CustomerID, ProductID}, null, null, null);		
		if(cursor.moveToFirst()){
			product.setId(cursor.getString(cursor.getColumnIndex("id")));
			product.setCustomer_id(cursor.getString(cursor.getColumnIndex("customer_id")));
			product.setProduct_id(cursor.getString(cursor.getColumnIndex("product_id")));
			product.setUnit_price(cursor.getString(cursor.getColumnIndex("unit_price")));
			product.setFree(cursor.getString(cursor.getColumnIndex("free")));
			product.setQuantity(cursor.getString(cursor.getColumnIndex("quantity")));			
		}else{
			closeCursor(cursor);
			return null;
		}	
		closeCursor(cursor);
		return product;
	}
	
	public String getCustomerProductPrice(String ProductID, String CustomerID){
		Product product = this.getProductById(ProductID);
		CustomerProduct customerproduct = this.getCustomerProductPriceObj(ProductID, CustomerID);
		
		JSONObject obj =null;
		try {
			obj = new JSONObject();
			
			if(customerproduct == null){
				obj.put("unit_price", String.valueOf(product.getUnit_price()).trim());
				obj.put("quantity", "0");
				obj.put("free", "0");				
			}else{
				obj.put("unit_price", customerproduct.getUnit_price());
				obj.put("quantity", customerproduct.getQuantity());
				obj.put("free", customerproduct.getFree());
			}
			
			obj.put("default_tax_id", product.getDefault_tax_id());
			obj.put("default_tax_id_2", product.getDefault_tax_id_2());			 
			obj.put("default_tax_percentage", this.getTaxPercentage(String.valueOf(product.getDefault_tax_id())));
			obj.put("avlquantity", "0");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(obj == null){
			return "offline";
		}
		System.out.println(obj.toString());
		return obj.toString();		
	}
	
	public String getRecentProducts(String CustomerID){
		
		SQLiteDatabase database = dbSQL;		
		//Cursor cursor = database.query("si_customer_products", null, "customer_id=?", new String[] {CustomerID}, null, null, null);
		Cursor cursor = database.rawQuery("select cp.* from si_customer_products cp join si_products p on cp.product_id=p.id where cp.customer_id="+CustomerID+" order by p.supplier", null);
		
		JSONArray arr = new JSONArray();
		if(cursor.moveToFirst()){
			do {            	
                //list.add(cursor.getString(0));//adding 2nd column data
				Product product = this.getProductById(cursor.getString(cursor.getColumnIndex("product_id")));
				CustomerProduct customerproduct = this.getCustomerProductPriceObj(product.getId(), CustomerID);
				
				JSONObject obj =null;
				try {
					obj = new JSONObject();
					
					if(customerproduct == null){
						obj.put("unit_price", String.valueOf(product.getUnit_price()).trim());
						obj.put("free_offer_qty", "0");
						obj.put("free_qty", "0");				
					}else{
						obj.put("unit_price", customerproduct.getUnit_price());
						obj.put("free_offer_qty", customerproduct.getQuantity());
						obj.put("free_qty", customerproduct.getFree());
					}
					
					
					obj.put("default_tax_id", product.getDefault_tax_id());
					obj.put("default_tax_id_2", product.getDefault_tax_id_2());			 
					obj.put("default_tax_percentage", this.getTaxPercentage(String.valueOf(product.getDefault_tax_id())));
					obj.put("avlquantity", "0");
					obj.put("id", product.getId());
					obj.put("description", product.getDescription());
					obj.put("default_unit_price", product.getUnit_price());
					obj.put("custom_field1", product.getCustom_field1());
					obj.put("custom_field2", product.getCustom_field2());
					obj.put("order_quantity", product.getOfflineOrderQuantity());
					obj.put("avlquantity", product.getAvailableQty());
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(product.getEnabled().contentEquals("0") || product.getId().isEmpty()){
					
				}else{
					arr.put(obj);
				}
				
            } while (cursor.moveToNext());
		}else{
			return "";
		}
		
		JSONObject result = new JSONObject();
		try {
			result.put("products", arr);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		closeCursor(cursor);
		return result.toString();		
	}
	public String getConfigValue(String configName){
		SQLiteDatabase database = dbSQL;
		String selectQuery = "SELECT  config_value FROM si_config where config_name='"+configName+"'";
		Cursor cursor = database.rawQuery(selectQuery, null);		
		if(cursor.moveToFirst()){
			String str = cursor.getString(0);
			closeCursor(cursor);
			return str;
		}else{
			return "";
		}
		
	}
	
	public String getBillerColumn(String columnName, String billerID){
		SQLiteDatabase database = dbSQL;
		String selectQuery = "select "+columnName+" from si_biller where id="+billerID;
		Cursor cursor = database.rawQuery(selectQuery, null);		
		if(cursor.moveToFirst()){
			String str = cursor.getString(0);
			closeCursor(cursor);
			return str;
		}else{
			return "";
		}
		
	}
	
	public Cursor getProduct(String ProductID){
		SQLiteDatabase database = dbSQL;
		String selectQuery = "select *, cost*tax_percentage/100 as calc_tax_amount, (cost+(cost*tax_percentage/100)) as MRP from si_products join si_tax on si_products.default_tax_id=si_tax.tax_id where id="+ProductID;
		Cursor cursor = database.rawQuery(selectQuery, null);		
		if(cursor.moveToFirst()){
			
			return cursor;
		}else{
			return null;
		}		
	}	
	
	public String getTaxPercentage(String taxID){
		SQLiteDatabase database = dbSQL;
		String selectQuery = "select * from si_tax where tax_id="+taxID;
		Cursor cursor = database.rawQuery(selectQuery, null);		
		if(cursor.moveToFirst()){
			String tax_percentage=cursor.getString(cursor.getColumnIndex("tax_percentage"));
			closeCursor(cursor);
			
			return tax_percentage;
		}else{
			return "0";
		}		
	}
	
	public int insertProduct(String order_id,String quantity,String product_id,String unit_price, String tax_percentage ){
		SQLiteDatabase database = dbSQL;
		ContentValues values = new ContentValues();				
		values.put("order_id", order_id);
		values.put("quantity", quantity);
		values.put("product_id", product_id);
		values.put("unit_price", unit_price);
		double int_tax_percentage=Double.parseDouble(tax_percentage);
		double int_quantity=Double.parseDouble(quantity);
		double int_unitprice=Double.parseDouble(unit_price);
		values.put("tax_amount", String.valueOf((int_quantity*int_unitprice*int_tax_percentage)/100));
		values.put("gross_total", String.valueOf(int_quantity*int_unitprice));
		values.put("description", "");
		values.put("total", String.valueOf((int_quantity*int_unitprice*int_tax_percentage/100)+(int_quantity*int_unitprice)));
		values.put("billed", "off");
				
		int insertID = (int) database.insert(TABLE_ORDER_ITEM, null, values);		
			
		return insertID;
	}
	
	
	public int insertProduct(String order_id,String quantity,String product_id,String price, String tax_percentage, String freeQty, String tax_id ){
		
		if(price.toString().trim().contentEquals("")||price.isEmpty()){									
			price="-1";
		}else{											
			double dbl_cost= Double.parseDouble(price);
			double dbl_tax_percentage= Double.parseDouble(tax_percentage);
			double dbl_unit_price = (dbl_cost/(1+(dbl_tax_percentage/100)));
			price = String.valueOf(dbl_unit_price);											
		}
		freeQty=freeQty.trim();
		if(freeQty.isEmpty()){
			freeQty="0";
		}
		String unit_price = price;
		SQLiteDatabase database = dbSQL;
		ContentValues values = new ContentValues();				
		values.put("order_id", order_id);
		values.put("quantity", quantity);
		values.put("free_qty", freeQty);
		values.put("product_id", product_id);
		values.put("unit_price", unit_price);
		double int_tax_percentage=Double.parseDouble(tax_percentage);
		double int_quantity=Double.parseDouble(quantity);
		double int_unitprice=Double.parseDouble(unit_price);
		values.put("tax_amount", String.valueOf((int_quantity*int_unitprice*int_tax_percentage)/100));
		values.put("gross_total", String.valueOf(int_quantity*int_unitprice));
		values.put("description", "");
		values.put("total", String.valueOf((int_quantity*int_unitprice*int_tax_percentage/100)+(int_quantity*int_unitprice)));
		values.put("billed", "off");
				
		int insertID = (int) database.insert(TABLE_ORDER_ITEM, null, values);		
		this.insertProductTax(String.valueOf(insertID), quantity, product_id, unit_price, tax_percentage, tax_id);	
		return insertID;
	}
	
	
	public void updateOrderProductQuantity(String order_item_id,String quantity, String tax_amount ){
		SQLiteDatabase database = dbSQL;	
		
		ContentValues values = new ContentValues();					
		values.put("quantity", quantity);
		values.put("tax_amount", tax_amount);	
		database.update(TABLE_ORDER_ITEM, values, "id=?", new String[] {order_item_id});		
					
	}
	
	public void updateOrderItem(String order_item_id,String quantity){
		String initial_tax_amount="0";
		String initial_quantity="0";
		String initial_unit_price="0";
		SQLiteDatabase database = dbSQL;	
		
		String selectQuery = "SELECT * FROM si_order_items WHERE id="+order_item_id;
		Cursor cursor = database.rawQuery(selectQuery, null);
		
		if(cursor.moveToFirst()){
			initial_quantity = cursor.getString(cursor.getColumnIndex("quantity"));
			initial_tax_amount = cursor.getString(cursor.getColumnIndex("tax_amount"));
			initial_unit_price = cursor.getString(cursor.getColumnIndex("unit_price"));
		}
		
		Double dbl_quantity=Double.parseDouble(quantity);
		Double dbl_initial_quantity= Double.parseDouble(initial_quantity);
		Double dbl_initial_tax_amount= Double.parseDouble(initial_tax_amount);
		Double dbl_initial_unit_price= Double.parseDouble(initial_unit_price);
		String tax_amount = String.valueOf((dbl_initial_tax_amount/dbl_initial_quantity)*Double.parseDouble(quantity));
				
		ContentValues values = new ContentValues();					
		values.put("quantity", quantity);
		values.put("tax_amount", tax_amount);
		values.put("gross_total", String.valueOf(dbl_initial_unit_price*dbl_quantity));
		values.put("total", String.valueOf((dbl_initial_unit_price*dbl_quantity)+(Double.parseDouble(tax_amount))));
		database.update(TABLE_ORDER_ITEM, values, "id=?", new String[] {order_item_id});
		
		ContentValues values1 = new ContentValues();
		values1.put("tax_amount", tax_amount);	
		database.update(TABLE_ORDER_ITEM_TAX, values1, "order_item_id=?", new String[] {order_item_id});
		closeCursor(cursor);
					
	}
	
	public void updateOrderItemFree(String order_item_id,String free_id,String free_qty){
		
		SQLiteDatabase database = dbSQL;	
		
		String selectQuery = "SELECT * FROM si_order_items WHERE id="+order_item_id;
		Cursor cursor = database.rawQuery(selectQuery, null);		
		if(cursor.moveToFirst()){
			ContentValues values = new ContentValues();					
			values.put("free_id", free_id);
			values.put("free_qty", free_qty);			
			database.update(TABLE_ORDER_ITEM, values, "id=?", new String[] {order_item_id});			
		}	
		
		selectQuery = "SELECT * FROM si_order_items WHERE id="+free_id;
		cursor = database.rawQuery(selectQuery, null);
		if(cursor.moveToFirst()){
			ContentValues values = new ContentValues();					
			values.put("isfree", "1");					
			database.update(TABLE_ORDER_ITEM, values, "id=?", new String[] {free_id});	
			closeCursor(cursor);
		}	
					
	}
	public void updateOrderProductTax(String order_item_id,String quantity ){
		SQLiteDatabase database = dbSQL;
		database.execSQL("update si_order_item_tax set tax_amount=(select (select tax_amount from si_order_item_tax where order_item_id="+order_item_id+")/(select quantity from si_order_items where id="+order_item_id+") *"+quantity+") where order_item_id="+order_item_id);
		
					
	}
	
	public int insertProductTax(String order_item_id, String quantity,String product_id,String unit_price, String tax_percentage,String tax_id ){
		SQLiteDatabase database = dbSQL;
		ContentValues values = new ContentValues();				
		
		values.put("order_item_id", order_item_id);
		values.put("tax_id", tax_id);
		values.put("tax_type", "%");
		double int_tax_percentage=Double.parseDouble(tax_percentage);
		double int_quantity=Double.parseDouble(quantity);
		double int_unitprice=Double.parseDouble(unit_price);
		values.put("tax_amount", String.valueOf((int_quantity*int_unitprice*int_tax_percentage)/100));
		values.put("tax_rate", tax_percentage);		
				
		int insertID = (int) database.insert(TABLE_ORDER_ITEM_TAX, null, values);		
			
		return insertID;		
	}
	
	public String getProductPrice(String customer_id,String product_id){
		SQLiteDatabase database = dbSQL;
		String selectQuery = "select si_customer_products.unit_price, tax_percentage, si_customer_products.unit_price+(si_customer_products.unit_price*tax_percentage/100) as StorePrice from si_customer_products join si_products on si_customer_products.product_id=si_products.id join si_tax on si_products.default_tax_id=si_tax.tax_id where customer_id="+customer_id+" and product_id="+product_id;
		Cursor cursor = database.rawQuery(selectQuery, null);		
		if(cursor.moveToFirst()){
			String price =cursor.getString(2);
			closeCursor(cursor);
			return price;
		}else{
			selectQuery = "select IFNULL(unit_price, 0) from si_products where id="+product_id;
			cursor = database.rawQuery(selectQuery, null);
			closeCursor(cursor);
			return null;
		}
	}
	
	public Cursor getOrderProducts(String OrderID){
		
			SQLiteDatabase database = dbSQL;
			String selectQuery ="select *,si_order_items.id as ItemID, round(round(si_order_items.total,2)/si_order_items.quantity,2) as OrderUnitPrice from si_order_items join si_products on si_order_items.product_id=si_products.id where order_id="+OrderID;
			Cursor cursor = database.rawQuery(selectQuery, null);		
			if(cursor.moveToFirst()){				
				return cursor;
			}else{
				return null;
			}
	}	
	
	public Cursor getPurchaseOrderProducts(String OrderID){
		
		SQLiteDatabase database = dbSQL;
		String selectQuery ="select *,si_order_items.id as ItemID, round(round(si_order_items.total,2)/si_order_items.quantity,2) as OrderUnitPrice from si_order_items join si_products on si_order_items.product_id=si_products.id where order_id="+OrderID+" order by si_products.custom_field4 asc";
		Cursor cursor = database.rawQuery(selectQuery, null);		
		if(cursor.moveToFirst()){				
			return cursor;
		}else{
			return null;
		}
}
	
	
	
	public List<Order> getOrders(String type){	
		List<Order> orders = new ArrayList<Order>();   
		SQLiteDatabase database = dbSQL;
		
		Cursor cursor = database.query(TABLE_ORDER, new String[]{"id"}, KEY_ORDER_TYPE_ID+"=?", new String[] {type}, null, null, "id desc","100");
		if(cursor.moveToFirst()){
			do{
				orders.add(getOrder(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_ORDER_ID))));
			}while(cursor.moveToNext());
		}			
		closeCursor(cursor);
		return orders;
	}
	
	
	public RequestParams prepareOrderSyncParameter(RequestParams params, String order_id){		
		SQLiteDatabase database = dbSQL;
		
		params.add("action", "insert");		
		params.add("source", "order");
		
		String selectQuery ="select * from si_orders where id="+order_id;
		Cursor cursor = database.rawQuery(selectQuery, null);
		cursor.moveToFirst();
		
		params.add("type", cursor.getString(cursor.getColumnIndex("type_id")));
		params.add("biller_id", cursor.getString(cursor.getColumnIndex("biller_id")));
		params.add("customer_id", cursor.getString(cursor.getColumnIndex("customer_id")));
		params.add("preference_id", cursor.getString(cursor.getColumnIndex("preference_id")));
		params.add("date", cursor.getString(cursor.getColumnIndex("date")));
		params.add("note", cursor.getString(cursor.getColumnIndex("note")));
		params.add("customField1", cursor.getString(cursor.getColumnIndex("custom_field1")));
		params.add("customField2", cursor.getString(cursor.getColumnIndex("custom_field2")));
		params.add("customField3", cursor.getString(cursor.getColumnIndex("custom_field3")));
		params.add("customField4", cursor.getString(cursor.getColumnIndex("custom_field4")));
		params.add("salesman", cursor.getString(cursor.getColumnIndex("salesman")));
		
		selectQuery ="select si_order_items.*,tax_id from si_order_items join si_order_item_tax on si_order_items.id=si_order_item_tax.order_item_id where order_id="+order_id;
		Cursor Productcursor = database.rawQuery(selectQuery, null);
		
		params.add("max_items", String.valueOf(Productcursor.getCount()));
		Productcursor.moveToFirst();
		int i=0;
		do{
			params.add("quantity"+String.valueOf(i), Productcursor.getString(Productcursor.getColumnIndex("quantity")));
			params.add("freequantity"+String.valueOf(i), Productcursor.getString(Productcursor.getColumnIndex("free_qty")));			
			params.add("products"+String.valueOf(i), Productcursor.getString(Productcursor.getColumnIndex("product_id")));
			
			params.add("description"+String.valueOf(i), Productcursor.getString(Productcursor.getColumnIndex("description")));
			params.add("unit_price"+String.valueOf(i), Productcursor.getString(Productcursor.getColumnIndex("unit_price")));
			params.add("tax_id["+String.valueOf(i)+"][1]", Productcursor.getString(Productcursor.getColumnIndex("tax_id")));
			params.add("unbilled"+String.valueOf(i), "off");			
			i++;			
		}while(Productcursor.moveToNext());	
				
		closeCursor(cursor);	
		closeCursor(Productcursor);		
		return params;
	}
	
	public ArrayList<NameValuePair> prepareOrderSyncParameter(ArrayList<NameValuePair> params, String order_id){		
		SQLiteDatabase database = dbSQL;
		
		params.add(new BasicNameValuePair("action", "insert"));		
		params.add(new BasicNameValuePair("source", "order"));
				
		String selectQuery ="select * from si_orders where id="+order_id;
		Cursor cursor = database.rawQuery(selectQuery, null);
		cursor.moveToFirst();
		params.add(new BasicNameValuePair("type", cursor.getString(cursor.getColumnIndex("type_id"))));
		params.add(new BasicNameValuePair("biller_id", cursor.getString(cursor.getColumnIndex("biller_id"))));
		params.add(new BasicNameValuePair("customer_id", cursor.getString(cursor.getColumnIndex("customer_id"))));
		params.add(new BasicNameValuePair("preference_id", cursor.getString(cursor.getColumnIndex("preference_id"))));
		params.add(new BasicNameValuePair("date", cursor.getString(cursor.getColumnIndex("date"))));
		params.add(new BasicNameValuePair("note", cursor.getString(cursor.getColumnIndex("note"))));
		params.add(new BasicNameValuePair("customField1", cursor.getString(cursor.getColumnIndex("custom_field1"))));
		params.add(new BasicNameValuePair("customField2", cursor.getString(cursor.getColumnIndex("custom_field2"))));
		params.add(new BasicNameValuePair("customField3", cursor.getString(cursor.getColumnIndex("custom_field3"))));
		params.add(new BasicNameValuePair("customField4", cursor.getString(cursor.getColumnIndex("custom_field4"))));
		params.add(new BasicNameValuePair("salesman", cursor.getString(cursor.getColumnIndex("salesman"))));				
				
		selectQuery ="select si_order_items.*,tax_id from si_order_items left join si_order_item_tax on si_order_items.id=si_order_item_tax.order_item_id where order_id="+order_id;
		Cursor Productcursor = database.rawQuery(selectQuery, null);
		
		params.add(new BasicNameValuePair("max_items", String.valueOf(Productcursor.getCount())));
		
		Productcursor.moveToFirst();
		int i=0;
		do{
			params.add(new BasicNameValuePair("quantity"+String.valueOf(i), Productcursor.getString(Productcursor.getColumnIndex("quantity"))));
			params.add(new BasicNameValuePair("products"+String.valueOf(i), Productcursor.getString(Productcursor.getColumnIndex("product_id"))));
			params.add(new BasicNameValuePair("description"+String.valueOf(i), Productcursor.getString(Productcursor.getColumnIndex("description"))));
			params.add(new BasicNameValuePair("unit_price"+String.valueOf(i), Productcursor.getString(Productcursor.getColumnIndex("unit_price"))));
			params.add(new BasicNameValuePair("tax_id["+String.valueOf(i)+"][1]", Productcursor.getString(Productcursor.getColumnIndex("tax_id"))));
			params.add(new BasicNameValuePair("unbilled"+String.valueOf(i), "off"));
			params.add(new BasicNameValuePair("freequantity"+String.valueOf(i), Productcursor.getString(Productcursor.getColumnIndex("free_qty"))));
			
			i++;			
		}while(Productcursor.moveToNext());	
		
		closeCursor(cursor);	
		closeCursor(Productcursor);	
				
		return params;
	}
	
	public void UpdatetOrderIndexID(String OrderID, String IndexID){
		SQLiteDatabase database = dbSQL;
		ContentValues values = new ContentValues();			
		values.put("index_id", IndexID);	
		database.update(TABLE_ORDER, values, "id=?", new String[] {OrderID});
		Order order = this.getOrder(OrderID);
		
	}
	
	public void deleteZeroProductsOrders(){
		SQLiteDatabase database = dbSQL;
		database.execSQL("delete from si_orders where id in (select id from (select (select count(*) from si_order_items where order_id=si_orders.id) as order_total,si_orders.id as id from si_orders join si_customers on si_orders.customer_id=si_customers.id where index_id=0 order by date desc) as order_sum where order_total is null or order_total=0)");		
	}
	
	public boolean checkIsCustomerValid(int id, String name){		
		SQLiteDatabase database = dbSQL;
		String selectQuery ="SELECT name as concatname,id FROM si_customers WHERE id=? and concatname=? ORDER BY name";
		Cursor cursor = database.rawQuery(selectQuery, new String[]{String.valueOf(id),name});		
		if(cursor.moveToFirst()){
			closeCursor(cursor);
			return true;
		}else{
			closeCursor(cursor);
			return false;
		}		
	}
	
	public boolean checkIsProductValid(int id, String name){		
		SQLiteDatabase database = dbSQL;
		String selectQuery ="SELECT description||', '||custom_field1||', '||custom_field2 as prod_desc,id FROM si_products WHERE id=? and prod_desc=?";
		
		Cursor cursor = database.rawQuery(selectQuery, new String[]{String.valueOf(id),name});		
		if(cursor.moveToFirst()){
			closeCursor(cursor);						
			return true;
		}else{
			closeCursor(cursor);						
			return false;
		}		
	}
	
	public int UnSyncOrderCount(){
		int count=0;
		SQLiteDatabase database = dbSQL;
		String selectQuery ="select * from si_orders where index_id=0";
		Cursor cursor = database.rawQuery(selectQuery, null);
		if(cursor.moveToFirst()){
			count = cursor.getCount();
			closeCursor(cursor);						
			return count;
		}
		return 0;
	}
	
	public String getOrderIDforSync(){
		String id="0";
		SQLiteDatabase database = dbSQL;
		String selectQuery ="select * from si_orders where index_id=0 and type_id<=5 limit 1";
		Cursor cursor = database.rawQuery(selectQuery, null);
		if(cursor.moveToFirst()){
			id = cursor.getString(cursor.getColumnIndex("id"));
			closeCursor(cursor);						
			return id;
		}
		return "0";
	}
	
	public void deleteOrderItem(String orderItemID){
		SQLiteDatabase database = dbSQL;
		
		String selectQuery ="select * from si_order_items where id="+orderItemID;
		Cursor cursor = database.rawQuery(selectQuery, null);		
		if(cursor.moveToFirst()){
			String free_id = cursor.getString(cursor.getColumnIndex("free_id"));
			if(!free_id.contentEquals("0"))
				database.delete(TABLE_ORDER_ITEM,  "id=?", new String[] {free_id});
		}		
		database.delete(TABLE_ORDER_ITEM,  "id=?", new String[] {orderItemID});
		closeCursor(cursor);
				
	}
	
	public void deleteOrder(String orderID){
		SQLiteDatabase database = dbSQL;
		database.delete(TABLE_ORDER,  "id=?", new String[] {orderID});		
	}
	
	public void editOrderItem(String orderItemID){
		SQLiteDatabase database = dbSQL;	
		database.delete(TABLE_ORDER_ITEM,  "id=?", new String[] {orderItemID});				
	}
	
	public void truncateTable(String tableName){
		SQLiteDatabase database = dbSQL;			
		database.delete(tableName,  null, null);
	}
	
	public void logout(){				
		this.insertUpdateConfig("cookie", "");
		this.insertUpdateConfig("salesman", "");
		this.insertUpdateConfig("email", "");				
		this.insertUpdateConfig("role_name", "");
		this.insertUpdateConfig("domain_id", "");
		this.insertUpdateConfig("password", "");
		this.insertUpdateConfig("online", "false");
	}
	
	public SQLiteDatabase getDB(){		
		if(this.dbSQL==null){
			dbSQL = this.getWritableDatabase();
		}		
		return this.dbSQL;
	}
	
	public static void closeCursor(Cursor cursor){
		if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
	}
	
	
	private class AsyncTaskRunner extends AsyncTask<String, String, String> {		  @Override
		  protected String doInBackground(String... params) {
			  String data = params[0];	
			  
			  if(data.isEmpty()){
				  return "Completed";
			  }
			  SQLiteDatabase database = dbSQL;
			  Cursor c = database.rawQuery("select max(id) from si_customer_products", null);
			  int max=0;
			  if(c.moveToFirst()){
				  max=c.getInt(0);
			  }
			  closeCursor(c);
			
			  
			  String[] split = data.split(";");
				for(int i=0; i<split.length ;i++){
					String row = split[i].toString();
					String[] row_split = row.split(",");
					CustomerProduct customerProd = new CustomerProduct(context);
					customerProd.setCustomer_id(row_split[0]);
					customerProd.setProduct_id(row_split[1]);
					customerProd.setUnit_price(row_split[2]);
					customerProd.setLogtime(row_split[3]);
					customerProd.setQuantity(row_split[4]);
					customerProd.setFree(row_split[5]);
					customerProd.setId(row_split[6]);
					int flag=1;
					if(Integer.parseInt(customerProd.getId().trim())>max){
						flag=0;
					}
					customerProd.save(flag);
				}
			  System.out.println("Customer Product Table sync completed");
			  return "Completed";
		  }
	
			
	}
}



package com.he.stockem.purchaseorder;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.he.stockem.R;
import com.he.stockem.db.model.Order;
import com.he.stockem.lib.AsyncRequest;
import com.he.stockem.lib.AsyncRequest.OnAsyncRequestComplete;
import com.he.stockem.lib.AutoCompleteItem;
import com.he.stockem.lib.AutocompleteArrayAdapter;
import com.he.stockem.lib.DatabaseHelper;
import com.he.stockem.lib.FileDownloader;
import com.he.stockem.lib.UtilityFunctions;
import com.ngx.BluetoothPrinter;

public class UpdatePurchaseOrderFragment extends Fragment implements OnAsyncRequestComplete {
	
	public UpdatePurchaseOrderFragment(){}

	Button  btnProductDelete, btnNewPuchaseOrderAddProd,btnPurchaseOrderShare;
	DatabaseHelper controller;
	TextView txtUpdatePurchaseOrderDate, txtUpdatePurchaseOrderCustID,txtAddProdMRP,txtUpdatePurchaseOrderID;
	EditText ETAddProdQuantity, ETAddProdUnitPrice, ETEditProdQuantity, ETAddProdQuantityFree;
	Order order;
	TableLayout tblNewOrderlayout;
	JSONObject obj;
	String unit_price="-1";
	UtilityFunctions util =new UtilityFunctions();
	ArrayAdapter<AutoCompleteItem> productAdapter;
	int selectedProductID=0;
	String OrderItemID="0";
	
	Cursor OrderProdCursor;
	
	List<String> sIds = new ArrayList<String>();
	List<String> names = new ArrayList<String>();
			
	public BluetoothPrinter mBtp= BluetoothPrinter.INSTANCE;
		
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		controller = DatabaseHelper.getInstance(getActivity());
        View rootView = inflater.inflate(R.layout.fragment_update_purchase_order, container, false);
        
        order = controller.getOrder(getArguments().getString("localOrderID"));
        if(order.getID().isEmpty()){        
        	getActivity().onBackPressed();
        	return rootView;
        }
        
        ArrayList<AutoCompleteItem> productArray = new  ArrayList<AutoCompleteItem>();
		productArray  = (ArrayList<AutoCompleteItem>) controller.getAllProductsItems();
		productAdapter = new AutocompleteArrayAdapter(getActivity(), R.layout.list_view_row_item, productArray);
		
		tblNewOrderlayout = (TableLayout)rootView.findViewById(R.id.tblNewOrderPurchaseProducts);
		txtUpdatePurchaseOrderDate = (TextView)rootView.findViewById(R.id.txtUpdatePurchaseOrderDate);
		txtUpdatePurchaseOrderCustID = (TextView)rootView.findViewById(R.id.txtUpdatePurchaseOrderCustID);
		txtUpdatePurchaseOrderID= (TextView)rootView.findViewById(R.id.txtUpdatePurchaseOrderID);
						
		
		
		if(order==null){
			Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.unable_to_get_order), 10).show();			
		}else{
			txtUpdatePurchaseOrderCustID.setText("Customer Name : "+order.getSupplier().getName());
			txtUpdatePurchaseOrderDate.setText("Date : "+order.getDate());
			
			txtUpdatePurchaseOrderID.setText("ID: " + order.getIndexID());			
		}
		BuildOrderItemsTable();
		btnPurchaseOrderShare = (Button)rootView.findViewById(R.id.btnPurchaseOrderShare);
		btnPurchaseOrderShare.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				PackageManager pm=v.getContext().getPackageManager();
				StringBuilder str = new StringBuilder("");
				String text = "YOUR TEXT HERE";
			    try {

			        Intent waIntent = new Intent(Intent.ACTION_SEND);
			        waIntent.setType("text/plain");
			        
			        OrderProdCursor=controller.getPurchaseOrderProducts(getArguments().getString("localOrderID"));
					if(OrderProdCursor==null || !OrderProdCursor.moveToFirst()){
						Toast.makeText(getActivity(), "No products in the order to print", 10).show();
						return;
					}
					String biller_id=controller.getConfigValue("biller_id");
					str.append(controller.getBillerColumn("name", biller_id));str.append("\n");		
					str.append(controller.getBillerColumn("city", biller_id));str.append("\n");
					
//					str.append("Hari Enteprises");str.append("\n");		
//					str.append("Trichy -21");str.append("\n");	
					str.append("\n");	
					str.append("Date : " + order.getDate());str.append("\n");					
					str.append("\n");
					str.append("-----------Order Summary-------------");str.append("\n");	
					str.append(order.getSupplier().getName());str.append("\n");	
//					str.append(order.getCustomer().getCustomField1());
//					
//					if(!order.getCustomer().getCustomField2().isEmpty()){
//						str.append(order.getCustomer().getCustomField2());					
//					}				
//					str.append(order.getCustomer().getStreetAddress());
					str.append("\n");
					
						
						
						String[] keys={"description","OrderUnitPrice","quantity","total"};
						String[] col_names={"Product","Landing Price","Quantity","Total"};
						
						str.append("-----------------Items------------------");	str.append("\n");	
						do {
							String key = (String)keys[0];				
							str.append(OrderProdCursor.getString(OrderProdCursor.getColumnIndex(key))+","+OrderProdCursor.getString(OrderProdCursor.getColumnIndex("custom_field1")));str.append("\n");	
							String line2 ="";
							line2 = "Qty:" +OrderProdCursor.getString(OrderProdCursor.getColumnIndex(keys[2]));
							line2 = String.format("%-10s", line2);
							line2 = line2+""+OrderProdCursor.getString(OrderProdCursor.getColumnIndex("article_no"));
							line2 = String.format("%-30s", line2);
							line2 = line2+OrderProdCursor.getString(OrderProdCursor.getColumnIndex("ean"));
							
							str.append(line2);str.append("\n");	
							str.append("----------------------------------------");str.append("\n");	
						}while (OrderProdCursor.moveToNext());
												
						
			        text = str.toString();
			        

//			        PackageInfo info=pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
			        //Check if package exists or not. If not then code 
			        //in catch block will be called
			        //waIntent.setPackage("com.whatsapp");

//			        waIntent.putExtra(Intent.EXTRA_TEXT, text);
			        //startActivity(Intent.createChooser(waIntent, "Share with"));
			        Intent sendIntent = new Intent(); 
	                sendIntent.setAction(Intent.ACTION_SEND); 
	                sendIntent.putExtra(Intent.EXTRA_TEXT, text); 
	                sendIntent.setType("text/plain"); 
	                startActivity(Intent.createChooser(sendIntent, "Share with"));

			   } catch (Exception e) {
				   Log.e("UpdatePurchaseOrderFragment","Share",e);
			        Toast.makeText(getActivity(), "Unable to send", Toast.LENGTH_SHORT)
			                .show();
			   }
			    
			    			    
			    
			}
		});
		
				
		btnNewPuchaseOrderAddProd = (Button)rootView.findViewById(R.id.btnNewPuchaseOrderAddProd);
		btnNewPuchaseOrderAddProd.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {				
				// TODO Auto-generated method stub
				Bundle args = new Bundle();
				args.putString("localOrderID", getArguments().getString("localOrderID"));
				AddPOProductsFragment nextFrag= new AddPOProductsFragment();
				nextFrag.setArguments(args);
			     getActivity().getFragmentManager().beginTransaction()
			     .replace(R.id.frame_container, nextFrag)											     
			     .addToBackStack("Add PO Products")
			     .commit();
				
				
			}
		});
		
        return rootView;
    }
	
	public void getPrice(String customerID, String ProductID){		
		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("module", "invoices"));
		params.add(new BasicNameValuePair("view", "product_ajax"));
		params.add(new BasicNameValuePair("id", ProductID));
		params.add(new BasicNameValuePair("customer_id", customerID));		
		String url=getResources().getString(R.string.server);
		AsyncRequest getPosts = new AsyncRequest((Fragment)this, "GET", params,"getprice",getResources().getString(R.string.getting_price));
		getPosts.execute(url);
	}

	
	public void asyncResponse(String response, String label) {
		// TODO Auto-generated method stub
		
		response = response.trim();
		
		if(label.contentEquals("updatecustomerlocation")){
			if(response.contentEquals("OK")){
				Toast.makeText(getActivity(), "Location updated", 10).show();
			}
			return;
		}
		if(label.contentEquals("getpdf")){
			Toast.makeText(getActivity(), "Invoice downloaded successfully", 10).show();
			File pdfFile = new File(Environment.getExternalStorageDirectory() + "/invoices/" + response);  // -> filename = maven.pdf	        
	        
//	        Intent browserIntent = new Intent(Intent.ACTION_VIEW);
//	        browserIntent.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
//	        browserIntent.setData(Uri.fromFile(pdfFile));
//	        startActivity(browserIntent);
//			Intent i = new Intent(Intent.ACTION_VIEW);
//			i.setPackage("com.dynamixsoftware.printershare");
//			i.setDataAndType(Uri.fromFile(pdfFile),"text/html");
//			Intent i = new Intent("android.intent.action.MAIN");
//		    i.setComponent(ComponentName.unflattenFromString("com.android.chrome/com.android.chrome.Main"));
//		    i.addCategory("android.intent.category.LAUNCHER");
//		    i.setData(Uri.fromFile(pdfFile));
			
		    Intent i = new Intent(Intent.ACTION_VIEW);		    
		    i.setData(Uri.fromFile(pdfFile));
	        try{
	        	startActivity(i);	        	
	        }catch(ActivityNotFoundException e){
	        	Log.e("UpdatePurchaseOrderFragment","asyncresponse",e);
	            Toast.makeText(getActivity(), "No Application available to view HTML", Toast.LENGTH_SHORT).show();
	        }
			
			Log.i("UpdatePurchaseOrderFragment", "Downloaded");
			return;
		}
		String unit_price="";
		if(response.contentEquals("")){
			ETAddProdUnitPrice.setText(unit_price);
		}else if(response.contains("unit_price")){			
			try {
				JSONObject obj  = new JSONObject(response);
				unit_price = obj.getString("unit_price");
				String tax_percentage= obj.getString("default_tax_percentage");				
				
				unit_price = unit_price.trim();
				if(unit_price.contentEquals("") || tax_percentage.trim().contentEquals("")){
					unit_price="";
				}else{
					double dbl_cost= Double.parseDouble(unit_price);
					double dbl_tax_percentage= Double.parseDouble(tax_percentage);
					double dbl_unit_price = dbl_cost + (dbl_cost*(dbl_tax_percentage/100));
					unit_price = String.valueOf(dbl_unit_price);
				}
				
				final String quantity= obj.getString("quantity");
				final String free= obj.getString("free");
								
				ETAddProdQuantity.addTextChangedListener(new TextWatcher(){					   
					   @Override
					   public void afterTextChanged(Editable s) {
						   String changedText=s.toString();
						   if(changedText.isEmpty() || changedText.contentEquals("0") || quantity.contentEquals("0")||free.contentEquals("0") || quantity.isEmpty()||free.isEmpty()){
							   return;
						   }else{
							   int int_quantity= Integer.parseInt(quantity);
							   int int_free= Integer.parseInt(free);
							   int int_text = Integer.parseInt(changedText);
							   int_text = int_text - (int_text%int_quantity);							   
							   ETAddProdQuantityFree.setText(String.valueOf((int_text/int_quantity)*int_free)); 
						   }
					   }
					 		
					   @Override    
					   public void beforeTextChanged(CharSequence s, int start,
					     int count, int after) {
					   }

					   @Override    
					   public void onTextChanged(CharSequence s, int start,
					     int before, int count) {
					      
					   }
					
				}); 

				
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				Log.e("UpdatePurchaseOrderFragment","asyncresponse",e);
				unit_price="";		
				
			}
			
		}
		
		ETAddProdUnitPrice.setText(util.StringroundTwoDecimals(unit_price));
		
	}
	
	public void BuildOrderItemsTable() {
		OrderProdCursor=controller.getOrderProducts(getArguments().getString("localOrderID"));
		tblNewOrderlayout.removeAllViews();
		if(OrderProdCursor==null || !OrderProdCursor.moveToFirst()){			
			
			return;
		}	
			
			String[] keys={"ItemID","description","quantity"};
			String[] col_names={"ItemID","Product","Quantity"};
			
			//Updating the table header
			TableRow header = new TableRow(getActivity());
			header.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
					  LayoutParams.WRAP_CONTENT));
			for(int j=0; j<col_names.length;j++){
				String key = (String)col_names[j];
				TextView tv = new TextView(getActivity());
				  tv.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
						  LayoutParams.WRAP_CONTENT));
				  tv.setBackgroundResource(R.drawable.cell_shape);
				  tv.setPadding(5, 5, 5, 5);
				  tv.setText(key);
				  if(key.contentEquals("ItemID")){
					  tv.setVisibility(View.GONE);
				  }
				  
				  header.addView(tv);
			}
			tblNewOrderlayout.addView(header);
			double total_value=0;
			
			do {
				TableRow row = new TableRow(getActivity());
				  row.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
						  LayoutParams.WRAP_CONTENT));
				  
				for(int j=0; j<keys.length;j++){
					  String key = (String)keys[j];					  
					  TextView tv = new TextView(getActivity());
					  tv.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
							  LayoutParams.WRAP_CONTENT));
					  tv.setBackgroundResource(R.drawable.cell_shape);
					  tv.setPadding(5, 5, 5, 5);
					  tv.setText(OrderProdCursor.getString(OrderProdCursor.getColumnIndex(key)));
					  if(key.contentEquals("description")){
						  String productName="";
						  productName = OrderProdCursor.getString(OrderProdCursor.getColumnIndex(key));
						  if(!OrderProdCursor.getString(OrderProdCursor.getColumnIndex("custom_field1")).contentEquals("") &&!OrderProdCursor.getString(OrderProdCursor.getColumnIndex("custom_field1")).contentEquals("null")){
							  productName = productName+", "+OrderProdCursor.getString(OrderProdCursor.getColumnIndex("custom_field1"));
						  }
						  if(!OrderProdCursor.getString(OrderProdCursor.getColumnIndex("custom_field2")).contentEquals("") &&!OrderProdCursor.getString(OrderProdCursor.getColumnIndex("custom_field1")).contentEquals("null")){
							  productName = productName+", MRP:"+OrderProdCursor.getString(OrderProdCursor.getColumnIndex("custom_field2"));
						  }
						  tv.setText(productName);
					  }
					  
					  if(key.contentEquals("ItemID")){
						  tv.setVisibility(View.GONE);
					  }
					  
					  row.addView(tv);					  
				  }
				
				if(order.getIndexID().contentEquals("0")){	
					row.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							ViewGroup vg = (ViewGroup)v;
							createEditDeleteDialog(((TextView)vg.getChildAt(0)).getText().toString(),((TextView)vg.getChildAt(1)).getText().toString(),((TextView)vg.getChildAt(2)).getText().toString()).show();
							//Toast.makeText(v.getContext(), ((TextView)vg.getChildAt(0)).getText(), 10).show();
							
						}
					});
				}
				  
				  
				tblNewOrderlayout.addView(row);							    
			}while (OrderProdCursor.moveToNext());
						
	}
	
public AlertDialog createEditDeleteDialog(final String productItemID,String message, String quantity){
		
		LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
		View promptView = layoutInflater.inflate(R.layout.edit_product_popup, null);
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				getActivity());
		alertDialogBuilder.setView(promptView);	
		
		alertDialogBuilder.setTitle(getActivity().getResources().getString(R.string.edit));
		ETEditProdQuantity = (EditText)promptView.findViewById(R.id.ETEditProdQuantity);		
		ETEditProdQuantity.setText(quantity);
		btnProductDelete = (Button)promptView.findViewById(R.id.btnProductDelete);		
		alertDialogBuilder
		.setMessage(productItemID+","+message)
		.setCancelable(false)
		.setPositiveButton(getActivity().getResources().getString(R.string.delete),new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				if(!order.getIndexID().contentEquals("0")){
					Toast.makeText(getActivity(), "Order/Invoice synced. Cannot modify", 10).show();
					return;
				}
				controller.deleteOrderItem(productItemID);
				dialog.cancel();
				BuildOrderItemsTable();
				Toast.makeText(getActivity(), "Product deleted", 10).show();
				
			}
		  })
		.setNegativeButton(getActivity().getResources().getString(R.string.cancel),new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				// if this button is clicked, just close
				// the dialog box and do nothing	
				dialog.cancel();		
				
			}
		});

		// create alert dialog
		final AlertDialog alertDialog = alertDialogBuilder.create();
		btnProductDelete.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				alertDialog.cancel();
				if(!order.getIndexID().contentEquals("0")){
					Toast.makeText(getActivity(), "Order/Invoice synced. Cannot modify", 10).show();
					return;
				}
				if(ETEditProdQuantity.getText().toString().trim().contentEquals("")||ETEditProdQuantity.getText().toString().trim().contentEquals("0")){
					Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.quantity_required), 10).show();
					return;
				}
				
				controller.updateOrderItem(productItemID, ETEditProdQuantity.getText().toString().trim());
				Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.order_updated), 10).show();
				BuildOrderItemsTable();						
			}
		});
		
		return alertDialog;
	}

private class DownloadFile extends AsyncTask<String, Void, Void>{

    @Override
    protected Void doInBackground(String... strings) {
        String fileUrl = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf
        String fileName = strings[1];  // -> maven.pdf
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        File folder = new File(extStorageDirectory, "testthreepdf");
        folder.mkdir();

        File pdfFile = new File(folder, fileName);

        try{
            pdfFile.createNewFile();
        }catch (IOException e){
        	Log.e("UpdatePurchaseOrderFragment","downloadfile",e);
            e.printStackTrace();
        }
        FileDownloader.downloadFile(fileUrl, pdfFile);
        return null;
    }
}

}
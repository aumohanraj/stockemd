package com.he.stockem.printer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.he.stockem.R;
import com.ngx.BluetoothPrinter;

public class ConnectPrinter extends Activity {
	
	private	BluetoothPrinter mBtp= BluetoothPrinter.INSTANCE;
	public static final String title_connecting = "connecting...";
	public static final String title_connected_to = "connected: ";
	public static final String title_not_connected = "not connected";
	TextView txtPrintStatus;
	
	
	Button btnPrint;	
	Button btnBTClear,btnBTConnect,btnBTUnpair;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_connect_printer);
		txtPrintStatus = (TextView)findViewById(R.id.txtPrintStatus);
		
		btnPrint=(Button)findViewById(R.id.btnViewRecplo);
		btnBTClear=(Button)findViewById(R.id.btnBTClear);
		btnBTConnect=(Button)findViewById(R.id.btnBTConnect);
		btnBTUnpair=(Button)findViewById(R.id.btnBTUnpair);
		
		
		mBtp.setDebugService(true);

		try {
			mBtp.initService(this, mHandler);
		} catch (Exception e) {
			Log.e("Connect Printer","Initiate Printer Service",e);
		}
		
				
		btnBTConnect.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub				
				try{
					mBtp.showDeviceList();
				}catch(Exception e){
					Log.e("Connect Printer","BT Connect",e);
					Toast.makeText(v.getContext(), "Something went wrong. Try Again!!!", 10).show();
				}
			}
		});
		
		btnBTClear = (Button)findViewById(R.id.btnBTClear);		
		btnBTClear.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub				
				try{
					mBtp.clearStoredPrinter();
				}catch(Exception e){
					Log.e("Connect Printer","BT Clear",e);
					Toast.makeText(v.getContext(), "Something went wrong. Try Again!!!", 10).show();
				}
				
			}
		});
		
		btnPrint.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mBtp.printText("asdfdasfas");
			}
		});
		
		btnBTUnpair.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub				
				try{
					mBtp.unPairAll();					
				}catch(Exception e){
					Log.e("Connect Printer","BT Unpair all",e);
					Toast.makeText(v.getContext(), "Something went wrong. Try Again!!!", 10).show();
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.connect_printer, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private String mConnectedDeviceName = "";

	@SuppressLint("HandlerLeak")
	private final Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case BluetoothPrinter.MESSAGE_STATE_CHANGE:
				switch (msg.arg1) {
				case BluetoothPrinter.STATE_CONNECTED:
					txtPrintStatus.setText(title_connected_to);
					txtPrintStatus.append(mConnectedDeviceName);
					break;
				case BluetoothPrinter.STATE_CONNECTING:
					txtPrintStatus.setText(title_connecting);
					break;
				case BluetoothPrinter.STATE_LISTEN:
				case BluetoothPrinter.STATE_NONE:
					txtPrintStatus.setText(title_not_connected);
					break;
				}
				break;
			case BluetoothPrinter.MESSAGE_DEVICE_NAME:
				// save the connected device's name
				mConnectedDeviceName = msg.getData().getString(
						BluetoothPrinter.DEVICE_NAME);
				break;
			case BluetoothPrinter.MESSAGE_STATUS:
				txtPrintStatus.setText(msg.getData().getString(
						BluetoothPrinter.STATUS_TEXT));
				break;
			default:
				break;
			}
		}
	};
}

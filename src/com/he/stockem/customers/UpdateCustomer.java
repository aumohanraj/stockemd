package com.he.stockem.customers;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableRow.LayoutParams;
import android.widget.Toast;

import com.he.stockem.R;
import com.he.stockem.db.model.AreaLine;
import com.he.stockem.db.model.Customer;
import com.he.stockem.db.model.Order;
import com.he.stockem.lib.AsyncRequest;
import com.he.stockem.lib.AsyncRequest.OnAsyncRequestComplete;
import com.he.stockem.lib.AutoCompleteItem;
import com.he.stockem.lib.DatabaseHelper;
import com.he.stockem.orders.UpdateOrderFragment;
import com.he.stockem.payments.PaymentsFragment;

public class UpdateCustomer extends Fragment implements View.OnClickListener, OnAsyncRequestComplete {
	
	public UpdateCustomer(){}
	
	DatabaseHelper controller;
	Button btnNewOrder, btnNewInvoice, btnPayment, btnEdit;
	String selectedCustomerID="";
	ArrayAdapter<AutoCompleteItem> productAdapter;
	Customer customer;
	
	int scrWidth, scrWidth80by3, scrWidth10;
    Button btnMenu[];
    LinearLayout llRoot, llHsMain, llMenuGroup1, llMenuGroup2, llMenuGroup3;
    Context context;
    HorizontalScrollView hScrollView;
    Button btnCustomerEdit,btnCustomerSave;
    EditText etUpdateCustName, etUpdateCustStreetAddress1, etUpdateCustStreetAddress2, etUpdateCustPhone, etUpdateCustTIN;
    Spinner spinUpdateCustRoute;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_update_customers, container, false);
        controller = DatabaseHelper.getInstance(getActivity()); 
        selectedCustomerID = getArguments().getString("customerID");
        customer = controller.getCustomer(selectedCustomerID);
        
        etUpdateCustName = (EditText)rootView.findViewById(R.id.etUpdateCustName);
        etUpdateCustName.setEnabled(false);
        etUpdateCustStreetAddress1=(EditText)rootView.findViewById(R.id.etUpdateCustStreetAddress1); 
        etUpdateCustStreetAddress1.setEnabled(false);
        etUpdateCustStreetAddress2=(EditText)rootView.findViewById(R.id.etUpdateCustStreetAddress2);
        etUpdateCustStreetAddress2.setEnabled(false);
        etUpdateCustPhone =(EditText)rootView.findViewById(R.id.etUpdateCustPhone);
        etUpdateCustPhone.setEnabled(false);        
        etUpdateCustTIN  =(EditText)rootView.findViewById(R.id.etUpdateCustTIN);
        etUpdateCustTIN.setEnabled(false);
        spinUpdateCustRoute = (Spinner)rootView.findViewById(R.id.spinUpdateCustRoute);
        spinUpdateCustRoute.setEnabled(false);
        btnCustomerEdit=(Button)rootView.findViewById(R.id.btnCustomerEdit);
        btnCustomerSave=(Button)rootView.findViewById(R.id.btnCustomerSave);
        btnCustomerSave.setVisibility(View.GONE);
        
        btnCustomerEdit.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				btnCustomerEdit.setVisibility(View.GONE);
				btnCustomerSave.setVisibility(View.VISIBLE);
				 
				etUpdateCustName.setEnabled(true);			         
			    etUpdateCustStreetAddress1.setEnabled(true);
			    etUpdateCustStreetAddress2.setEnabled(true);
			    etUpdateCustPhone.setEnabled(true);	
			    etUpdateCustTIN.setEnabled(true);
			    spinUpdateCustRoute.setEnabled(true);
			}
		});
        
        
        List<String> categories = new ArrayList<String>();    				    				
		List<AreaLine> area_lines= controller.getAllAreaLines();
		categories.add("Select Route");
		for(AreaLine area_line : area_lines){		
			categories.add(area_line.getName());
		}
	         			      
	      // Creating adapter for spinner
	    ArrayAdapter<String> categoryAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, categories);    			    
	    categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); 
	    spinUpdateCustRoute.setAdapter(categoryAdapter);
	    
        if(!customer.getCustomField3().contentEquals("null")){
        	etUpdateCustName.setText(customer.getCustomField3());
        }
	    
        if(!customer.getStreetAddress().contentEquals("null")){
        	etUpdateCustStreetAddress1.setText(customer.getStreetAddress());
        }
        
        if(!customer.getName().contentEquals("null")){
        	etUpdateCustStreetAddress2.setText(customer.getName());
        }
        
        if(!customer.getMobilePhone().contentEquals("null")){
        	etUpdateCustPhone.setText(customer.getMobilePhone());
        }
        
        AreaLine route = controller.getRouteById(customer.getAreaLine());
        if(route != null){
        	spinUpdateCustRoute.setSelection(categories.indexOf(route.getName()));
        }
        
        
        btnCustomerSave.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				btnCustomerEdit.setVisibility(View.VISIBLE);
				btnCustomerSave.setVisibility(View.GONE);
				 
				etUpdateCustName.setEnabled(false);			         
			    etUpdateCustStreetAddress1.setEnabled(false);
			    etUpdateCustStreetAddress2.setEnabled(false);
			    etUpdateCustPhone.setEnabled(false);
			    etUpdateCustTIN.setEnabled(false);
			    spinUpdateCustRoute.setEnabled(false);
			    
			    ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair("custom_field3", etUpdateCustName.getText().toString()));
				params.add(new BasicNameValuePair("street_address", etUpdateCustStreetAddress1.getText().toString()));				
				params.add(new BasicNameValuePair("name", etUpdateCustStreetAddress2.getText().toString()));
				params.add(new BasicNameValuePair("mobile_phone", etUpdateCustPhone.getText().toString()));
				params.add(new BasicNameValuePair("custom_field2", etUpdateCustTIN.getText().toString()));
				AreaLine selectedRoute = controller.getRoute(spinUpdateCustRoute.getSelectedItem().toString());
				int routeID=0;
				if(!selectedRoute.getId().isEmpty()){
					routeID = Integer.parseInt(selectedRoute.getId());
				}
				params.add(new BasicNameValuePair("area_line", String.valueOf(routeID)));
				
				
				String url=getResources().getString(R.string.server)+"?module=api/customers&view=updateCustomer&id="+customer.getID();
				AsyncRequest getPosts = new AsyncRequest(UpdateCustomer.this, "POST", params,"update_customer","Please wait");
				getPosts.execute(url);
				
				
			    				
			}
		});
        llRoot = (LinearLayout) rootView.findViewById(R.id.TopMenuReportsRoot);
        DisplayMetrics dm = new DisplayMetrics();
        this.getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        scrWidth = dm.widthPixels;
        int scrWidth1 = dm.widthPixels;
        
        scrWidth = (int) (0.74 * scrWidth); // Calculating 80 % Width of Screen for Horizontal Scroll View
        // Calulation 1/3 of Width of  Horizontal Scroll View for 3  menu items
       if(scrWidth1 <500){
       	scrWidth80by3 = (int) (scrWidth / 1.5);
       }else if(scrWidth1 >=500 && scrWidth1 <=800){
       	scrWidth80by3 = (int) (scrWidth / 2);
       }else{
       	scrWidth80by3 = 300;
       }
       
       scrWidth10 = (int) (0.13 * dm.widthPixels);// Calulation 10% Width of Screen for Previous And Next Button

       context = this.getActivity();

       btnMenu = new Button[9];

       llMenuGroup1 = new LinearLayout(context);
       llMenuGroup2 = new LinearLayout(context);
       llMenuGroup3 = new LinearLayout(context);

       llMenuGroup1.setLayoutParams(new LayoutParams(scrWidth,
               LayoutParams.WRAP_CONTENT));
       llMenuGroup2.setLayoutParams(new LayoutParams(scrWidth,
               LayoutParams.WRAP_CONTENT));
       llMenuGroup3.setLayoutParams(new LayoutParams(scrWidth,
               LayoutParams.WRAP_CONTENT));
       llHsMain = new LinearLayout(context);
       llHsMain.setLayoutParams(new LayoutParams(scrWidth,
               LayoutParams.WRAP_CONTENT));

       btnNewOrder = (Button)this.getActivity().getLayoutInflater().inflate(R.layout.top_bar_button, null);
       btnNewOrder.setBackgroundResource(R.drawable.top_bar_button);
       btnNewOrder.setId(1);        
       btnNewOrder.setLayoutParams(new LayoutParams(scrWidth80by3,
             LayoutParams.MATCH_PARENT));
       Drawable img = this.getActivity().getResources().getDrawable( R.drawable.ic_orders );
       img.setBounds( 0, 0, 60, 60 );
       btnNewOrder.setCompoundDrawables( img, null, null, null );       
       btnNewOrder.setText("New Order");       
       llHsMain.addView(btnNewOrder);
       
       btnNewInvoice = (Button)this.getActivity().getLayoutInflater().inflate(R.layout.top_bar_button, null);
       btnNewInvoice.setBackgroundResource(R.drawable.top_bar_button);
       btnNewInvoice.setId(2);        
       btnNewInvoice.setLayoutParams(new LayoutParams(scrWidth80by3,
             LayoutParams.MATCH_PARENT));
           
       img = this.getActivity().getResources().getDrawable( R.drawable.ic_invoice );
       img.setBounds( 0, 0, 60, 60 );
       btnNewInvoice.setCompoundDrawables( img, null, null, null );
       btnNewInvoice.setText("New Invoice");        
       llHsMain.addView(btnNewInvoice);
       
       btnPayment = (Button)this.getActivity().getLayoutInflater().inflate(R.layout.top_bar_button, null);
       btnPayment.setId(3);      
       btnPayment.setBackgroundResource(R.drawable.top_bar_button);
       btnPayment.setLayoutParams(new LayoutParams(scrWidth80by3,
             LayoutParams.MATCH_PARENT));
       
       img = this.getActivity().getResources().getDrawable( R.drawable.ic_payments );
       img.setBounds( 0, 0, 60, 60 );
       btnPayment.setCompoundDrawables( img, null, null, null );
       btnPayment.setText("Make Payment");        
       llHsMain.addView(btnPayment);
       
       ImageButton btnNext = new ImageButton(context);
       btnNext.setOnClickListener(this);
       btnNext.setImageResource(R.drawable.ic_forward);
       btnNext.setId(0);
       
       ImageButton btnPre = new ImageButton(context);
       btnPre.setOnClickListener(this);
       btnPre.setImageResource(R.drawable.ic_back);
       btnPre.setPadding(3, 3, 3, 3);
       btnPre.setId(-1);
       
       btnNext.setLayoutParams(new LayoutParams(scrWidth10,
               LayoutParams.MATCH_PARENT));
       
       btnPre.setLayoutParams(new LayoutParams(scrWidth10,
               LayoutParams.MATCH_PARENT));
       
       hScrollView = new HorizontalScrollView(context);
       hScrollView.setLayoutParams(new LayoutParams(scrWidth,
               LayoutParams.WRAP_CONTENT));
       
       hScrollView.addView(llHsMain);
       
       llRoot.addView(btnPre);
       llRoot.addView(hScrollView);
       llRoot.addView(btnNext);

       btnNewOrder.setOnClickListener(new View.OnClickListener() {
		
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Order newOrder = new Order(getActivity());
				newOrder.setCustomerID(String.valueOf(selectedCustomerID));
				
				String salesman = controller.getConfigValue("salesman");
				if(salesman.contentEquals("")){
					salesman="Offline user";
				}	
				
				newOrder.setSalesman(salesman);
				newOrder.setTypeID(Order.KEY_TYPE_ORDER);
				int localOrderID=Integer.parseInt(newOrder.save());
				if(localOrderID>0){
					Bundle args = new Bundle();
					args.putString("localOrderID", String.valueOf(localOrderID));												 
					Toast.makeText(getActivity(), getResources().getString(R.string.order_created), 10).show();
					UpdateOrderFragment nextFrag= new UpdateOrderFragment();
					nextFrag.setArguments(args);
				     getActivity().getFragmentManager().beginTransaction()
				     .replace(R.id.frame_container, nextFrag)											     
				     .addToBackStack("Update Order")
				     .commit();
				}else{
					Toast.makeText(getActivity(), getResources().getString(R.string.unable_to_create_order), 10).show();												
				}
			}
		});
       btnNewInvoice.setOnClickListener(new View.OnClickListener() {
   		
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Order newOrder = new Order(getActivity());
				newOrder.setCustomerID(String.valueOf(selectedCustomerID));
				
				String salesman = controller.getConfigValue("salesman");
				if(salesman.contentEquals("")){
					salesman="Offline user";
				}	
				
				newOrder.setSalesman(salesman);
				newOrder.setTypeID(Order.KEY_TYPE_INVOICE);
				int localOrderID=Integer.parseInt(newOrder.save());
				if(localOrderID>0){
					Bundle args = new Bundle();
					args.putString("localOrderID", String.valueOf(localOrderID));												 
					Toast.makeText(getActivity(), getResources().getString(R.string.invoice_created), 10).show();
					UpdateOrderFragment nextFrag= new UpdateOrderFragment();
					nextFrag.setArguments(args);
				     getActivity().getFragmentManager().beginTransaction()
				     .replace(R.id.frame_container, nextFrag)											     
				     .addToBackStack("Update Order")
				     .commit();
				}else{
					Toast.makeText(getActivity(), getResources().getString(R.string.unable_to_create_invoice), 10).show();												
				}
				
			}
		}); 
       btnPayment.setOnClickListener(new View.OnClickListener() {
   		
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Bundle args = new Bundle();
				
				args.putString("paymentCustomerID", String.valueOf(selectedCustomerID));
				args.putString("paymentCustomerName", String.valueOf(customer.getName()));
				
				PaymentsFragment nextFrag= new PaymentsFragment();	
				UpdateCustomer.this.getActivity().getActionBar().setTitle("Payments");
				nextFrag.setArguments(args);
				UpdateCustomer.this.getActivity().getFragmentManager().beginTransaction()			    
			     .replace(R.id.frame_container, nextFrag)											     
			     .addToBackStack("Payment Fragment")
			     .commit();
								
			}
		}); 
       
        return rootView;
    }
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
	    case -1: // Previous Button Clicked
	    	
	        hScrollView.scrollBy(-scrWidth, 0); // see -ve sign to scroll back horizontally only scrWidth is 80% of Total Screen
	        break;
	    case 0: // Next Button Clicked
	        hScrollView.scrollBy(scrWidth, 0); // scrolls next horizontally only scrWidth is 80% of Total Screen
	        break;

	    case 1: // Menu Button1 Clicked
	        Toast.makeText(v.getContext(), "Menu 1 Clicked",1).show();
	        break;
	    case 2: // Menu Button1 Clicked
	        Toast.makeText(v.getContext(), "Menu 2 Clicked",1).show();
	        break;
	    case 3: // Menu Button1 Clicked
	        Toast.makeText(v.getContext(), "Menu 3 Clicked",1).show();
	        break;
	    case 4: // Menu Button1 Clicked
	        Toast.makeText(v.getContext(), "Menu 4 Clicked",1).show();
	        break;
	    case 5: // Menu Button1 Clicked
	        Toast.makeText(v.getContext(), "Menu 5 Clicked",1).show();
	        break;
	        // repeat up to 6

	    case 6:
	        Toast.makeText(v.getContext(), "Menu 6 Clicked",1).show();
	        break;
	    default:
	        break;
	    }
	}
	
		@Override
	public void asyncResponse(String response, String label) {
		// TODO Auto-generated method stub
		try {
			JSONObject obj = new JSONObject(response);
			if(obj.getBoolean("error")){
				Toast.makeText(getActivity(), "Try later", 10).show();				
			}else{
				Toast.makeText(getActivity(), "Customer updated successfully", 10).show();
				customer.setCustomField3(etUpdateCustName.getText().toString());
				customer.setStreetAddress(etUpdateCustStreetAddress1.getText().toString());
				customer.setName(etUpdateCustStreetAddress2.getText().toString());
				customer.setMobilePhone(etUpdateCustPhone.getText().toString());
				
				
				AreaLine selectedRoute = controller.getRoute(spinUpdateCustRoute.getSelectedItem().toString());
				int routeID=0;
				if(!selectedRoute.getId().isEmpty()){
					routeID = Integer.parseInt(selectedRoute.getId());
				}
				customer.setAreaLine(routeID);
				customer.save();
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Toast.makeText(getActivity(), "Try later", 10).show();
		}
	     
	}
	
	
}
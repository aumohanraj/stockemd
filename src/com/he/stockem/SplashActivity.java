package com.he.stockem;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.he.stockem.locpoller.LocationPoller;
import com.he.stockem.locpoller.LocationReceiver;

public class SplashActivity extends Activity {
	
	private static int SPLASH_TIME_OUT = 1000;
	
	private static final int PERIOD=60000;  // 1 Min
	private PendingIntent pi=null;
	private AlarmManager mgr=null;
	SharedPreferences sharedPrefs;
	String url;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		
		sharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(this);
		url = sharedPrefs.getString("prefURL", "");
		
		mgr=(AlarmManager)getSystemService(ALARM_SERVICE);	    
	    Intent i=new Intent(this, LocationPoller.class);	    
	    i.putExtra(LocationPoller.EXTRA_INTENT,
	               new Intent(this, LocationReceiver.class));
	    i.putExtra(LocationPoller.EXTRA_PROVIDER,
	               LocationManager.GPS_PROVIDER);
	    
	    pi=PendingIntent.getBroadcast(this, 0, i, 0);
	    mgr.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
	                      SystemClock.elapsedRealtime(),
	                      PERIOD,
	                      pi);
	    Log.i("SplashActivity","Location polling every 1 minutes begun");	    
		
		new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
            	if(url.isEmpty() ||  !sharedPrefs.getString("prefURL", "").startsWith("http")){
            		Intent i = new Intent(SplashActivity.this, SetURL.class);
                    startActivity(i);
                    finish(); 
            	}else{
            		Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();            		
            	}
                
            }
        }, SPLASH_TIME_OUT);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		
		return super.onOptionsItemSelected(item);
	}
}
